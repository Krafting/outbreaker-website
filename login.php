<?php
$pageTitle = "Connexion";
include('include/init.php');
if(connect() == true) {
    header('Location: index.php');
    exit();
}
include('include/header.php');
?>
    <div class="content">
        <div class="page">
            <h1>Connectez-vous pour acceder à la boutique et à toutes les fonctionnalités du site.</h1>
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <form method="post" action="include/forms/loginForm.php">         
                <div id="form">
                    <p>Votre pseudo </p>  
                    <input type="text" name="pseudo" placeholder="Pseudonyme">
                    <p>Votre mot de passe </p> 
                    <input type="password" name="password" placeholder="Mot de passe">
                    <input type="hidden" name="redir" value="<?php if(!empty($_GET['redir'])) { echo secure($_GET['redir']); } ?>">
                    <small>Pas encore inscrit ? <a href="register.php" title="Connexion">Inscrivez-vous</a></small><br>
                    <small>Mot de passe perdu ? <a href="lostPassword.php" title="Connexion">Envoyez-moi un nouveau!</a></small>
                    <br>
                    <div class="sendButton">
                        <button type="submit" name="login" class="btn">Se connecter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php
include('include/footer.php');
?>
