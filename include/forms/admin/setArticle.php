<?php
include('../../init.php');

//si l'utilisateur n'est pas admin et/ou si il n'est pas connecter , on n'affiche pas la page 
if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

/* quand on veut delete l'article , il faut verifier qu'aucun article n'appartiens à cette categorie*/ 
if (isset($_POST['delete_article'])) {
	
	/* Preparation au changement d'image comme pour addarticle*/

	$sql_exist = $connexion->prepare(" SELECT count(*) from articles where idarticle=:idarticle ;");
	$sql_exist->execute(array(
		'idarticle' => $_POST['idarticle_del']
	));
	$sql_exist=$sql_exist->fetch();

	if ($sql_exist[0]>0) {
			$sql_deleteArticle = $connexion->prepare("DELETE from articles where idarticle=:idarticle ;");
			$sql_deleteArticle->execute(array(
				'idarticle' => $_POST['idarticle_del']
			));
			header('Location: ../../../admin/gestion_article.php?succ=41'); 
			exit();
	} else {
		header('Location: ../../../admin/gestion_article.php?err=42&idarticle='.secure($_POST['idarticle_del']).''); 
		exit();
	}
}

/*upload d'image*/
if (isset($_POST['ajouterArticle'])) {
	$tabExt = array('jpg','gif','png','jpeg'); 
	define('TARGET', '../../../upload/articles/'.$_POST['idarticle'].'/');


		if  (!empty($_FILES['new_photo_article']['name'])) {	/* as-t-on selectionner un fichier ?*/		
		
		/* On regarde si le dossier de photo de l'article existe déjà sinon on lui crée un dossier */
			if (!file_exists('../../../upload/articles/'.$_POST['idarticle'].'/')) {
				mkdir('../../../upload/articles/'.$_POST['idarticle'].'/', 0775, true);
			}


			$extension  = pathinfo($_FILES['new_photo_article']['name'], PATHINFO_EXTENSION);
				/* verifie si l'extension est correcte */
				if(in_array(strtolower($extension),$tabExt)) {
					$nomImage = sha1(uniqid()) .'.'. $extension;
					
						/* si l'upload est reussi on rentre le liens de l'image dans la bdd*/

						if(move_uploaded_file(htmlspecialchars($_FILES['new_photo_article']['tmp_name']), TARGET.$nomImage)) {
							$sql_image = $connexion->prepare("UPDATE articles set imageprincipale=:nom_image where idarticle=:idarticle;");
							$sql_image->execute(array(
								'nom_image' => $nomImage,
								'idarticle' => $_POST['idarticle']
							));	
						} 
				} 
				
		} 


	//ON VERIFIE QU'UN NOM AS BIEN ÉTÉ RENTRER
	if (isset($_POST['new_nom_article']) && !empty($_POST['new_nom_article'])) {

		$sql_exist = $connexion->prepare(" SELECT count(*) from articles where idarticle=:idarticle ;");
		$sql_exist->execute(array(
			'idarticle' => $_POST['idarticle']
		));
		$sql_exist=$sql_exist->fetch();

		/* Si l'article existe alors on peut verifier d'autre condition*/
		if ($sql_exist[0]>0) {

			//on verifie qu'on as bien une quantité rentrer
			if (isset($_POST['new_qte_article']) && is_numeric($_POST['new_qte_article'])) {

				//On verifie qu'on as bien set un prix pour l'article
				if (isset($_POST['new_prix_article']) && !empty($_POST['new_prix_article']) && is_numeric($_POST['new_prix_article'])) {

					//On vérifie que le type existe et est set
					if (isset($_POST['new_type_article']) && !empty($_POST['new_type_article'])) {
						$sql_type_exist = $connexion->prepare(" SELECT count(*) from typearticle where idtype=:new_type_article;");
						$sql_type_exist->execute(array(
							'new_type_article' => $_POST['new_type_article']
						));
						$sql_type_exist=$sql_type_exist->fetch();

						/*Si le type existe alors ... */
						if ($sql_type_exist[0]>0) {
							$sql_setArticle = $connexion->prepare("UPDATE articles set 
								nomarticle=:nomarticle,
								qtedispo=:qtedispo,
								prixvente=:prixvente,
								desarticle=:desarticle,
								reftypearticle=:reftypearticle,
								keywords=:keywords 
								where idarticle=:idarticle;");
							$sql_setArticle->execute(array(
								'nomarticle' => $_POST['new_nom_article'],
								'qtedispo' => $_POST['new_qte_article'],
								'prixvente' => $_POST['new_prix_article'],
								'desarticle' => $_POST['new_des_article'],
								'reftypearticle' => $_POST['new_type_article'],
								'keywords' => $_POST['new_mot_clefs'],
								'idarticle' => $_POST['idarticle']
							));
							header('Location: ../../../admin/gestion_article.php?succ=55&idarticle='.secure($_POST['idarticle']));
							exit();
						} else {
							header('Location: ../../../admin/gestion_article.php?err=57&idarticle='.secure($_POST['idarticle']));
							exit();
						} 
					} else {
						header('Location: ../../../admin/gestion_article.php?err=37&idarticle='.secure($_POST['idarticle']));
						exit();
					}
				} else {
					header('Location: ../../../admin/gestion_article.php?err=1&idarticle='.secure($_POST['idarticle']));
					exit();
				}
			} else {
				header('Location: ../../../admin/gestion_article.php?err=1&idarticle='.secure($_POST['idarticle']));
				exit();
			}
		} else {
			header('Location: ../../../admin/gestion_article.php?err=28');
			exit();
		}
	} else {
		header('Location: ../../../admin/gestion_article.php?err=1&idarticle='.secure($_POST['idarticle']));
		exit();
	}
}


if (isset($_POST['btn_image_secondaire'])){
	/* preparation pour ajouter des images*/
	$tabExt = array('jpg','gif','png','jpeg'); 
	define('TARGET', '../../../upload/articles/'.$_POST['idarticle'].'/');

	$sql_exist_article=$connexion->prepare("SELECT count(*) FROM articles where idarticle=:idarticle;");
	$sql_exist_article->execute(array(
		'idarticle' => $_POST['idarticle']
	));
	$sql_exist_article=$sql_exist_article->fetch();

	if ($sql_exist_article[0]>0) {

		if  (!empty($_FILES['image_secondaire']['name'])) {			
		/* On regarde si le dossier de photo de l'user existe déjà sinon on lui crée un dossier */
			if (!file_exists('../../../upload/articles/'.$_POST['idarticle'].'/')) {
				mkdir('../../../upload/articles/'.$_POST['idarticle'].'/', 0775, true);
			}
			$extension  = pathinfo($_FILES['image_secondaire']['name'], PATHINFO_EXTENSION);
				/* verifie si l'extension est correcte */
				if(in_array(strtolower($extension),$tabExt)) {
					$nomImage = sha1(uniqid()) .'.'. $extension;
					
						/* si l'upload est reussi on rentre le liens de l'image dans la bdd*/

						if(move_uploaded_file(htmlspecialchars($_FILES['image_secondaire']['tmp_name']), TARGET.$nomImage)) {
							$sql_image = $connexion->prepare("INSERT INTO imagesarticle (nomfichier,refarticle) values (:nomfichier,:refarticle);");
							$sql_image->execute(array(
								'nomfichier' => $nomImage,
								'refarticle' => $_POST['idarticle']
							));
							header('Location: ../../../admin/gestion_article.php?succ=55&idarticle='.secure($_POST['idarticle']));
							exit();	
						} else {
							header('Location: ../../../admin/gestion_article.php?err=9&idarticle='.secure($_POST['idarticle']));	
							exit();
						}
				} else {
					header('Location: ../../../admin/gestion_article.php?err=7&idarticle='.secure($_POST['idarticle']));
					exit();
				}			
		} else {
			header('Location: ../../../admin/gestion_article.php?err=1&idarticle='.secure($_POST['idarticle']));
			exit();
		}
	} else {
	 	header('Location: ../../../admin/gestion_article.php?err=28');
		exit();
	}
}

?>