<?php
include('../init.php');
if(!connect()) {
    header('Location: ../../');
    exit();
}

/* SI LE POST EST ENVOYER, SI UN ARTICLE EST SELECTIONNER, SI L'ID EST NUMERIC
SI LE TEXTE D'AVIS N'EST PAS VIDE ET SI LE NOMBRE D'ÉTOILE EST PAS VIDE, NUMÉRIC ET ENTRE 1 ET 5 */
if(isset($_POST['modifAvis']) && !empty($_POST['idarticle']) && !empty($_POST['textarticle']) 
    && !empty($_POST['stars']) && is_numeric($_POST['idarticle']) && is_numeric($_POST['stars']) 
    && $_POST['stars'] < 6 && $_POST['stars'] > 0) {
    /* ON VERIFIE QUE L'ARTICLE EXISTE */
    $countArticle = $connexion->prepare('SELECT count(*) FROM articles WHERE idarticle=:idarticle');
    $countArticle->execute(array(
        'idarticle' => $_POST['idarticle']
    ));
    $countArticle = $countArticle->fetch();

    /* SI L'ARTICLE EXISTE */
    if($countArticle[0] > 0) {
        /* ON REGARDE SI L'UTILISATEUR A COMMANDER CET ARTICLE, SI OUI IL PEUT LAISSER UN AVIS */
        $sql = "'".$_POST['idarticle']."' AND refuser='".$_SESSION['id']."' ";
        $countArticles = $connexion->query($sql);
        $countBuy = $connexion->prepare('SELECT count(refarticle) FROM commander INNER JOIN commandes ON refcommande=idcommande WHERE refarticle=:idarticle AND refuser=:iduser');
        $countBuy->execute(array(
            'idarticle' => $_POST['idarticle'],
            'iduser' => $_SESSION['id']
        ));
        $countBuy = $countBuy->fetch();   

        if($countBuy[0] > 0) {
            $fetchAvis = $connexion->prepare('SELECT * FROM avisarticle WHERE idrefarticle=:idrefarticle AND idrefuser=:idrefuser');
            $fetchAvis->execute(array(
                'idrefarticle' => $_POST['idarticle'],
                'idrefuser' => $_SESSION['id']
            ));
            /* ON RECUPERE LE NOMBRE D'AVIS SUR CET ARTICLE AVEC L'ID DE L'USER CONNECTÉ */
            $countAvis = count($fetchAvis->fetchAll());
            /* SI IL A DEJA UN AVIS, ON UPDATE */
            if($countAvis > 0) {
                $updateAvis = $connexion->prepare('UPDATE avisarticle SET textavis=:textarticle, noteavis=:noteavis WHERE idrefarticle=:idrefarticle AND idrefuser=:idrefuser');
                $updateAvis->execute(array(
                    'textarticle' => $_POST['textarticle'],
                    'noteavis' => $_POST['stars'],
                    'idrefarticle' => $_POST['idarticle'],
                    'idrefuser' => $_SESSION['id']
                ));
                header('Location: ../../article.php?succ=17&id='.$_POST['idarticle']);
                exit();
            /* SINON ON LE CRÉER */
            } else { 
                $addAvis = $connexion->prepare('INSERT INTO avisarticle (textavis, noteavis, idrefarticle, idrefuser) VALUES (:textarticle, :noteavis, :idrefarticle, :idrefuser)');
                $addAvis->execute(array(
                    'textarticle' => $_POST['textarticle'],
                    'noteavis' => $_POST['stars'],
                    'idrefarticle' => $_POST['idarticle'],
                    'idrefuser' => $_SESSION['id']
                ));
                header('Location: ../../article.php?succ=16&id='.$_POST['idarticle']);
                exit();
            }
        } else {
            header('Location: ../../article.php?err=18&id='.$_POST['idarticle']);
            exit();
        }
    } else {
        header('Location: ../../shop.php');
        exit();
    }
} else {
    header('Location: ../../article.php?err=33&id='.$_POST['idarticle']);
    exit();
}



?>