<?php
include('../init.php');
/* ON CHECK SI LE POST EST ENVOYÉ ET SI LE PSEUDO ET PASSWORD N'EST PAS VIDE */
if( isset($_POST['login']) AND !empty($_POST['pseudo']) AND !empty($_POST['password']) ) {
    $pseudo = strtolower(secure($_POST['pseudo']));
    $password = hash('sha256', $_POST['password']);

    $nbUser = $connexion->prepare('SELECT COUNT(*) FROM users WHERE pseudo=:pseudo AND pass=:pass');
    $nbUser->execute(array(
        'pseudo' => $pseudo, 
        'pass' => $password
    ));
    $verif = $nbUser->fetch();
    /* SI L'UTILISATEUR EXISTE .. */
    if($verif[0] == 1) {
        $session = sha1(rand(32, 989898989898));
        $_SESSION['session'] = $session;
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['token'] = sha1(time() * rand());

        $updateUser = $connexion->prepare("UPDATE users SET session=:session WHERE pseudo=:pseudo");
        $updateUser->execute(array(
            'session' => $session, 
            'pseudo' => $pseudo
        ));
        
        if(!empty($_POST['redir'])) {
            header('Location: ../../'.secure($_POST['redir']).'.php');
            exit();
        } else {
            header('Location: ../../index.php');
            exit();
        }
        exit;
    } else {
        header('Location: ../../login.php?err=26');
        exit();
    }
} else {
    header('Location: ../../login.php?err=1');
    exit();
}

?>