<?php 
if(!isAdmin() OR !isset($adminPage) OR $adminPage = false) {
    header('Location: ../index.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://cdn.krafting.net/css/fontawesome/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administration - <?php echo $titrePage; ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">     <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link href="../include/css/components.css" rel="stylesheet">
    <link href="../include/css/admin.css" rel="stylesheet">
</head>
<body>
<div class="WholePage">
<nav>
    <p>Espace d'administration</p>
    <p><a href="../">Retour sur le site</a></p>
    <a href="index.php" <?php if($titrePage == "Dashboard") { echo "class='active'"; } ?>>Dashboard</a>
    <br/>
    <p>Gestion Membres</p>
    <a href="users.php" <?php if($titrePage == "Gestion des utilisateurs") { echo "class='active'"; } ?>>Gestion des utilisateurs </a>
    <a href="gestion_commande.php" <?php if($titrePage == "Gestion des commandes") { echo "class='active'"; } ?>>Gestion des commandes</a>
    <a href="gestion_rang.php" <?php if($titrePage == "Gestion des rangs") { echo "class='active'"; } ?>>Gestion des rangs</a>
    <br/>
    <p>Gestion Boutique</p>
    <a href="gestion_categorie.php" <?php if($titrePage == "Gestion des catégories") { echo "class='active'"; } ?>>Gestion des catégories </a>
    <a href="gestion_article.php" <?php if($titrePage == "Gestion des articles") { echo "class='active'"; } ?>>Gestion des articles</a>
    <br/>
    <p>Gestion Plays</p>
    <a href="gestion_plays.php" <?php if($titrePage == "Gestion des Plays") { echo "class='active'"; } ?>>Gestion des Plays</a>
    <a href="gestion_categorie_plays.php" <?php if($titrePage == "Gestion des catégories des Plays") { echo "class='active'"; } ?>>Gestion des catégories</a>
    <br/>


</nav>
<section>