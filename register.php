<?php
$pageTitle = "Inscription";
include("include/init.php");
include('include/header.php');

if(connect()) {
    header('Location: index.php');
    exit();
}

?>
    <div class="content">

        <div class="page">
            <h1>Inscrivez-vous pour acceder à la boutique et à toutes les fonctionnalités du site.</h1>

            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <form method="post" action="include/forms/RegisterForm.php">         
                <div id="form">
                    <p>Comment devons-nous vous appeler ? </p>  
                    <input type="text" name="pseudo" placeholder="Pseudonyme">
                    <p>Comment pouvons-nous vous contacter ? </p>  
                    <input type="text" pattern='[\w\-\.]+@[\w\-\.]+\.[A-z]{2,}$' name="mail" placeholder="Adresse e-mail">
                    <p>Comment voulez-vous vous connecter ? </p>  
                    <input type="password" name="pass1" placeholder="Mot de passe">
                    <p>Confirmer la façon de vous connecter. </p> 
                    <input type="password" name="pass2" placeholder="Confirmation de mot de passe">
                    <div class="h-captcha" data-sitekey="c1621798-8df6-4492-80f4-f32ca518a227"></div>
			        <script src="https://hcaptcha.com/1/api.js" async defer></script>
                    <br>
                    <input type="hidden" name="redir" value="<?php if(!empty($_GET['redir'])) { echo secure($_GET['redir']); } ?>">
                    <small>Déjà inscrit ? <a href="login.php" title="Connexion">Connectez-vous</a></small>
                    <br>
                    <div class="sendButton">
                        <button type="submit" name="register" class="btn">Rejoindre la bande!</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php
include('include/footer.php');
?>
