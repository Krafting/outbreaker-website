<?php
$adminPage = true; $titrePage = "Gestion des articles";
include('../include/init.php');
include('header.php');

/*  Pour supprimer un article  */
if (isset($_GET['idarticle']) && !empty($_GET['idarticle']) && is_numeric($_GET['idarticle'])
    && isset($_GET['del']) && !empty($_GET['del']) && is_numeric($_GET['del'])) {

    $sql_idimagearticle_exist = $connexion->prepare("SELECT count(*) FROM imagesarticle where idimagearticle=:idimagearticle ;");
    $sql_idimagearticle_exist->execute(array(
        'idimagearticle'=> $_GET['del']
    ));
    $sql_idimagearticle_exist=$sql_idimagearticle_exist->fetch();


    if ($sql_idimagearticle_exist[0]>0) {

        /* requete pour recuperer le nom de l'image puis on l'unlick avec le liens qui permet de la retrouvé dans upload/........)*/
        $sql_requete=$connexion->prepare("SELECT * FROM imagesarticle where idimagearticle=:idimagearticle ;");
        $sql_requete->execute(array(
            'idimagearticle'=> $_GET['del']
        ));
        $sql_requete=$sql_requete->fetch();
        unlink('../upload/articles/'.$_GET['idarticle'].'/'.$sql_requete['nomfichier']);
        echo "succes";

        /* suppression de la bdd*/
        $sql_delete_image=$connexion->prepare("DELETE FROM imagesarticle where idimagearticle=:idimagearticle ;");
        $sql_delete_image->execute(array(
            'idimagearticle'=> $_GET['del']
        ));
       header('Location: ./gestion_article.php?succ=81&idarticle='.$_GET['idarticle']);
       exit();
    } else {
        header('Location: ./gestion_article.php?err=82&idarticle='.$_GET['idarticle']);
        exit();
    }
} 

if (isset($_GET['idarticle']) && !empty($_GET['idarticle']) && is_numeric($_GET['idarticle'])) {

	$sql_id_exist = $connexion->prepare("SELECT count(*) FROM articles where idarticle=:idarticle ;");
	$sql_id_exist->execute(array(
		'idarticle'=> $_GET['idarticle']
	));
	$sql_id_exist=$sql_id_exist->fetch();


	if ($sql_id_exist[0]>0) {

       
      	if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }             
            }
	   

	   

	    ?><h1>Modifier l'article</h1><?php 
        /* requête pour selectionner les données utilisé plus tard dans les values*/
        $sql_nomarticle = $connexion->prepare("SELECT * from articles where idarticle=:idarticle;");
        $sql_nomarticle->execute(array(
        	'idarticle'=> $_GET['idarticle']
        ));

     	$sql_nomarticle=$sql_nomarticle->fetch();
     	$nomarticle=$sql_nomarticle['nomarticle'];
     	$idarticle=$sql_nomarticle['idarticle'];
        $qte_dispo=$sql_nomarticle['qtedispo'];
        $mot_clefs=$sql_nomarticle['keywords'];
        $prix_vente=$sql_nomarticle['prixvente'];
        $desc=$sql_nomarticle['desarticle'];
        $nomimage=$sql_nomarticle['imageprincipale'];

     
        ?><div class="flexBlock">
            <div class="flexContent border-first">
                <!-- Enctype nécessaire pour l'upload de l'image-->
                <form method="post" action="../include/forms/admin/setArticle.php" enctype="multipart/form-data">    
                    <?php /*input hidden sert a recuperer l'id de l'article qu'on veut modifier dans le script setArticle.php*/ ?> 
                    <input type="hidden" name="idarticle" value="<?php echo $idarticle; ?>" >    


                    <p>Entrer le nouveau nom de l'article  : </p>  
                    <input type="text" name="new_nom_article" placeholder="Nom de l'article" value="<?php echo $nomarticle; ?>"> 

                    <p>Changer la quantité disponible</p>
                    <input type="text" name="new_qte_article" placeholder="Quantité de l'article"
                     value="<?php echo $qte_dispo; ?>">


                    <p>Changer les mot-clefs</p>
                    <input type="text" name="new_mot_clefs" placeholder="Mots-clefs"  value="<?php echo $mot_clefs; ?>">  


                    <p>Changer le prix de vente</p>
                    <input type="text" name="new_prix_article" placeholder="Prix de vente" value="<?php echo $prix_vente; ?>">    
            </div>
            <div class="flexContent">
                    <p>Entrer une nouvelle description</p>
                    <textarea name="new_des_article" placeholder="Description"><?php echo $desc;?></textarea> 

                    <!--reftype menu déroulant--> 
                    <p>Changer le type de l'article</p>
                    <select name="new_type_article"> 
                            <?php $sql_type = $connexion->prepare("SELECT * from typearticle");
                                        $sql_type->execute();
                                        $sql_type=$sql_type->fetchall();
                                            foreach ($sql_type as $ligne) { ?>
                                            <option <?php if ($sql_nomarticle['reftypearticle']==$ligne['idtype']) {echo 'selected=""';} ?> value="<?php echo $ligne['idtype'];?>"> <?php echo $ligne['nomtype'];?></option> <?php } ?>


                        </select>             

                    <p>Photo(s) de l'article(s)</p>  
                    <input type="file" name="new_photo_article"> 

                    <div class="sendButton">
                        <button type="submit" name="ajouterArticle" class="btn">Modifier l'article</button>        
                    </div>
                </form>                                
            </div>
        </div>


        <div class="img" style="margin-top: 20px;">
            <h2>Ajouter une image secondaire</h2>
            <form method="post" action="../include/forms/admin/setArticle.php" enctype="multipart/form-data">
                <input type="file" name="image_secondaire">
                <input type="hidden" name="idarticle" value="<?php echo $idarticle; ?>">
                <div class="sendButton" style="text-align: center;">
                    <button class="btn" type="submit" name="btn_image_secondaire">Ajouter l'image</button>
                </div>
            </form>
            <h2>Images de l'article</h2>
            <div class="flexBlock">
                <div class="flexContent" style="padding:0px;">
                    <img style="border-right: 5px solid var(--light-grey); padding-right: 20px; margin-right: 2" src="../../upload/articles/<?php echo $idarticle.'/'.$nomimage; ?>" height="400px">
                </div>
                <div class="flexContent" style="padding:0px;">
             <?php 
                        $sql_img_secondaire = $connexion->prepare('SELECT * FROM imagesarticle WHERE refarticle=:refarticle');
                        $sql_img_secondaire->execute(array(
                            'refarticle' => $_GET['idarticle']
                        ));
                        $sql_img_secondaire = $sql_img_secondaire->fetchAll();
                        foreach($sql_img_secondaire as $image) {
                            $nomimage=$image['nomfichier'];
                            $idimage=$image['idimagearticle'];
                            ?> 
                             <button class="nocss" onclick="openModal('picArticle<?php echo $image['idimagearticle'];?>')"><img height="197px" src="../../upload/articles/<?php echo $_GET['idarticle'].'/'.$nomimage;?>"></img></button>
                                <div id="picArticle<?php echo $image['idimagearticle'];?>" class="modal">

                                    <div class="modal-content" style="max-width: 40%;">
                                        <div class="modal-header"><h2>Voulez-vous supprimez cette image</h2>
                                        </div>
                                        <p style="max-height: 600px;overflow: auto;">
                                            <div class="sendButton">
                                                <a class="btn" href="?del=<?php echo $idimage;?>&idarticle=<?php echo $_GET['idarticle'];?>">Supprimer l'image</a></div>
                                        <img style="width: 100%; height: auto" src="../../upload/articles/<?php echo $_GET['idarticle'].'/'.$nomimage; ?>"></img>
                                        </p>
                                    </div>
                                </div>
                        <?php } ?>
                </div>
            </div>
                    
        </div>

            





        <form method="post" action="../include/forms/admin/setArticle.php">
                <br><br><br>
                <input type="hidden" name='idarticle_del' value="<?php echo $idarticle; ?>">
                <button type="submit" name="delete_article" class="btn">Supprimer l'article</button>  <br><br><br><br>   
        </form>  
        <script type="text/javascript" src="../include/js/js.js"></script>   


<?php
 } else {  
 	header('Location: ./gestion_article.php?err=28');
     exit();
 }
} else {  

   if(isset($_GET['err']) OR isset($_GET['succ'])) {
        if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
            $idMsg = $_GET['err'];
            echo getMessage($idMsg);
        }
        if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
            $idMsg = $_GET['succ'];
            echo getMessage($idMsg);
        }
    }
 	
?>
 
<h1>Gestion des articles</h1>
<hr style="margin-bottom: 30px;">
<br>
<table>
        <tr>
            <a  class="btn" href="./ajouterArticle.php">Ajouter un article</a>
    	</tr>
        <tr style="font-weight: bold;">
    		<td>Nom des articles</td>
    		<td style="width: 30%;">Action</td>
            <td style="width: 30%;">Voir les avis</td>
    	</tr>
     
<?php
		$sql_article = $connexion->prepare("SELECT * from articles  order by idarticle;");
    	$sql_article->execute();
        $sql_article=$sql_article->fetchall();
        foreach ($sql_article as $ligne) {
        	$nomarticle=$ligne['nomarticle'];
        	echo "<tr><td> ".$nomarticle."</td>"; 
        	echo "<td><a class=' ' href='?idarticle=".$ligne['idarticle']."' name='modifier'>Modifier</a>"; 
            echo "<td><a class=' ' href='./voir_avis.php?idarticle=".$ligne['idarticle']."' name='avis'>Voir les avis</a>";
        }
?>

</table>

<?php } ?>