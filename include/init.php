<?php
# Init file
# Where the magic happens
session_start();
ob_start();
// setlocale(LC_ALL, 'French');
# CONNEXION A LA BDD
$user='';
$pass='';
$dbname='';
$host='localhost';
$url = "https://ob.krafting.net/";
// $port='';
// $infos='pgsql:host='.$host.';dbname='.$dbname.';port='.$port;  
try {
    // $connexion = new PDO($infos, $user, $pass);
    $connexion = new PDO('mysql:host='.$host.';dbname='.$dbname.'; charset=utf8', $user, $pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOException $e) {
    $connexion = "";
    print "Erreur de connexion a la base de donnees ! : " . $e->getMessage();
    die();
}

// DES VARIABLES 
$maxImageSize = 2000000; // 2MB
$maxVideoSize = 20000000; // 20MB

$regexpseudo="/[\w-]{2,25}$/";
$regexmail="/[\w\-\.]+@[\w\-\.]+\.[A-z]{2,}$/";
// $date = date();

function secure($texte) {
    return htmlspecialchars($texte);
}

function connect() {
	global $connexion;
	if(!empty($_SESSION['session'])) {
		// $nbrUser = $connexion->query("SELECT COUNT(*) FROM users WHERE session='".$_SESSION['session']."' AND pseudo='".$_SESSION['pseudo']."'");
		// $verif = $nbrUser->fetch();
        $nbrUser = $connexion->prepare('SELECT COUNT(*) FROM users WHERE session=:session AND pseudo=:pseudo');
        $nbrUser->execute(array(
            'session' => $_SESSION['session'],
            'pseudo' => $_SESSION['pseudo']
        ));
        $verif = $nbrUser->fetch();

        if ($verif[0] == 1) {
			return true;
		}
		session_unset();
		session_destroy();
		return null;
	}
	return false;
}

function getMessage($id) {
    global $connexion;
    /* ON REGARDE SI LE MESSAGE EXISTE */ 
    $nbrMessage = $connexion->prepare('SELECT COUNT(*) FROM messages WHERE idmessage=:idmessage');
    $nbrMessage->execute(array('idmessage' => $id));
    $verif = $nbrMessage->fetch();
    if($verif[0] > 0) {
        /* ON FETCH LE MESSAGE ET ON CHECK LE TYPE */
        $message = $connexion->prepare('SELECT * FROM messages WHERE idmessage=:idmessage');
        $message->execute(array('idmessage' => $id));
        $message = $message->fetch();
        /* TYPE 1 = SUCCESS & TYPE 0 = ERREUR */
        /* ON PEUT AJOUTER DES TYPE AS WE GO  */
        if($message['typemessage'] == 1) {
            $content = '<div class="successMessage"> '.$message['textmessage'].' </div>';
            return $content;
        } elseif($message['typemessage'] == 0) {
            $content = '<div class="errorMessage"> '.$message['textmessage'].' </div>';
            return $content;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function findProfilePic($iduser) {
	global $connexion;
    $result = $connexion->prepare('SELECT profilepicture FROM users WHERE iduser=:iduser');
    $result->execute(array(
        'iduser' => $iduser
    ));
    $value = $result->fetch();

    $profilePicture = $iduser.'/'.$value['profilepicture'];
    $default = "default.png";
    // return $value;
    if(!empty($value['profilepicture'])) {
        return $profilePicture;
    } else {
        return $default;
    }
}

function isAdmin() {
    if(connect()) {
        if(!empty($_SESSION['rang']) AND $_SESSION['rang'] == 4) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return false;
}


function canUploadPlay() {
    if(connect()) {
        if(!empty($_SESSION['rang']) AND ($_SESSION['rang'] == 4 OR $_SESSION['rang'] == 2)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
    return false;
}

function dateFormat($date, $format) {
    $newDate = date($format, strtotime($date));
    return $newDate; 
}

function truncage($start, $text, $nbChar) {
    $textReturn = substr($text, $start, $nbChar);
    if(strlen($text) > $nbChar) {
        return $textReturn."..";
    } else {
        return $textReturn;
    }
}

function findNumPlayComment($id) {
    global $connexion; 
    $selectAll = $connexion->prepare('SELECT COUNT(*) FROM commentairesplay WHERE refplay=:refplay');
    $selectAll->execute(array(
        'refplay' => $id
    ));
    $nbview = $selectAll->fetch();
    return $nbview[0];
}

function findCurrentURIArgs($uri) {
    $currentURI = explode('?', $uri);
    if(count($currentURI) > 1) {
        $currentURIArgs = $currentURI[1];
    } else {
        $currentURIArgs = "none";
    }
    return $currentURIArgs;
}

function findNumViews($id) {
    global $connexion; 
    $selectAll = $connexion->prepare('SELECT COUNT(*) FROM viewplay WHERE refplay=:refplay');
    $selectAll->execute(array(
        'refplay' => $id
    ));
    $nbview = $selectAll->fetch();
    return $nbview[0];
}

if(strstr($_SERVER['REQUEST_URI'], 'logout')) {
    session_destroy();
    session_unset();
	$_SESSION = array();
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}	
	header('Location: index.php?disconnected');
    exit();
}

if(connect() == true) {
    $pseudo = $_SESSION['pseudo'];
    if(empty($_SESSION['pseudo'])) {
        $pseudo = 'Inconnu';
    }
    if($pseudo != 'Inconnu' AND !empty($pseudo)) {
        $req2 = $connexion->prepare('SELECT * FROM users INNER JOIN rangs ON refrang=idrang WHERE pseudo=:pseudo');
        $req2->execute(array(
            'pseudo' => $pseudo
        ));
        $result = $req2->fetch();

        if($result['session'] == $_SESSION['session'] AND !empty($result['session'])) {
            $_SESSION['pseudo'] = $result['pseudo'];
            $_SESSION['mail'] = $result['mail'];
            $_SESSION['id'] = $result['iduser'];
            $_SESSION['rang'] = $result['refrang'];
            $_SESSION['nomrang'] = $result['nomrang'];
            $_SESSION['about'] = $result['about'];
            $_SESSION['dateinscri'] = $result['dateinscri'];
            $_SESSION['verifaccount'] = $result['verifaccount'];
            $_SESSION['allowcommentaire'] = $result['allowcommentaire'];
            if(empty($result['profilepicture']) OR $result['profilepicture'] == "") {
                $_SESSION['profilepicture'] = "default.png";
            } else {
                $_SESSION['profilepicture'] = $result['profilepicture'];
            }
        } else {
            session_unset();
            session_destroy();
        }
    }
}

if(isset($needConnect) AND $needConnect == true AND !connect()) {
    header('Location: login.php');
    exit();
}

function dateInscri($id) {
    global $connexion;
    $reqSelectUser = $connexion->prepare('SELECT dateinscri FROM users WHERE iduser=:id');
    $reqSelectUser->execute(array(
        'id' => $id
    ));
    $user = $reqSelectUser->fetch(); 

    $newDate = date("d M Y", strtotime($user['dateinscri']));
    return $newDate;
}

function formatTexte($texte) {
    $texteFormat = str_replace("\n", "<br>", $texte);
    return $texteFormat;
}
?>
