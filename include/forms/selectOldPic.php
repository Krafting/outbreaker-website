<?php

include("../init.php"); 

if(!connect()) {
    header('Location: ../../');
    exit();
}

/* SI ON SELECTIONNE UNE ANCIENNE PHOTO */
if (isset($_POST['selectPicturePost'])){
	/* SI LE NOM DE L'IMAGE N'EST PAS VIDE */
	if (!empty ($_POST['selectImage'])) {
		/* SI LE FICHIER EXISTE ON UPDATE L'UTILISATEUR AVEC CE NOM DE FICHIER */
		if (file_exists ("../../upload/profiles/".$_SESSION['id']."/".$_POST['selectImage'])){
			$nbUser = $connexion->prepare('UPDATE users SET profilepicture=:profilepic WHERE iduser=:id;');
			$nbUser->execute(array(
				'profilepic' => $_POST['selectImage'], 
				'id' => $_SESSION['id']
			));
			header('Location: ../../membre.php?succ=13'); 
			exit();
		} else {
			header('Location: ../../membre.php?err=6');
			exit();
		}
	} else {
		header('Location: ../../membre.php?err=1');	
		exit();
	}
}

/* SI ON DELETE UNE ANCIENNE PHOTO */
if (isset($_POST['deletePicturePost'])) {
	/* SI LE NOM DE L'IMAGE N'EST PAS VIDE */
	if (!empty($_POST['deleteImage'])) {
		/* SI LE FICHIER EXISTE */
		if(file_exists ("../../upload/profiles/".$_SESSION['id']."/".$_POST['selectImage'])){
			/* SI LA PHOTO DE PROFILE ACTUELLE DE L'UTILISATEUR EST CELLE QUE L'ON VEUX SUPPRIMER, ALORS ON UPDATE LA BDD AUSSI */
			if ($_POST['deleteImage']==$_SESSION['profilepicture']) {
				$nbUser = $connexion->prepare('UPDATE users SET profilepicture=:profilepic WHERE iduser=:id;');
				$nbUser->execute(array(
					'profilepic' => '', 
					'id' => $_SESSION['id']
				));
				/* ON DELETE LE FICHIER AUSSI */
				unlink("../../upload/profiles/".$_SESSION['id']."/".$_POST['deleteImage']);
				header('Location: ../../membre.php?succ=14'); 
				exit();
			} else {
				unlink("../../upload/profiles/".$_SESSION['id']."/".$_POST['deleteImage']);
				header('Location: ../../membre.php?succ=14');
				exit();
			}
		} else {
			header('Location: ../../membre.php?err=6');
			exit();
		}
	} else {
		header('Location: ../../membre.php?err=1');
		exit();
	}
}
?>