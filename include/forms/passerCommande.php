<?php 
include('../init.php');
/* SI ON EST CONNECTÉ, UN UTILISATEUR NON CONNECTÉ NE PEUX PAS COMMANDER */
if(connect()) {
    /* SI LE FORMULAIRE EST ENVOYÉ */
    if(isset($_POST['passerCmd'])) {

        /* ON CHECK SI LE PANIER DE L'UTILISATEUR EST VIDE */
        $paniervide = $connexion->prepare('SELECT COUNT(*) FROM panier2 WHERE idrefuser=:id');
        $paniervide->execute(array(
            'id' => $_SESSION['id']
        ));
        $paniervide = $paniervide->fetch();
        
        /* SI LE PANIER DE L'UTILISATEUR EST PAS VIDE */
        if($paniervide[0] > 0) {
            /* SI LE FORMULAIRE EST BIEN REMPLIS */
            if(!empty($_POST['nomCommande']) AND !empty($_POST['prenomCommande']) 
            AND !empty($_POST['addrLivraison']) AND !empty($_POST['villeLivraison']) 
            AND !empty($_POST['codePosLivraison']) AND !empty($_POST['depLivraison'])) {
                
                if(!empty($_POST['codebancaire']) AND !empty($_POST['moisCarte']) 
                AND !empty($_POST['AnneeCarte']) AND !empty($_POST['cryptoCarte'])) {
                    /* ON CHECK LA REGEX DE LA CARTE DE CREDIT */
                    if(preg_match_all("/[0-9]{3}$/", $_POST['cryptoCarte']) && 
                    preg_match_all("/[0-9]{2}$/", $_POST['AnneeCarte']) &&
                    preg_match_all("/[0-9]{5,}$/", $_POST['codebancaire']) &&
                    preg_match_all("/[0-9]{2}$/", $_POST['moisCarte'])) {
                        /* ON CHECK LE FORMAT DU CODE POSTAL */
                        if(preg_match_all("/[0-9- ]+$/", $_POST['codePosLivraison'])) {

                            /* On check l'adresse pour voir si elle existe ou non, pour des raisons évidentes */
                            $urlCheck = "https://api-adresse.data.gouv.fr/search/?q=".urlencode($_POST['addrLivraison']).urlencode(", ").urlencode($_POST['villeLivraison'])."&type=housenumber&autocomplete=1&postcode=".urlencode($_POST['codePosLivraison'])."";
                            $json_content = json_decode(file_get_contents($urlCheck), true);

                            /* ON RÉCUPÈRE LE SCORE DE FIABILITÉ DE L'ADRESSE RENTRÉE */
                            if(isset($json_content["features"][0]["properties"]["score"])) {
                                $score = $json_content["features"][0]["properties"]["score"];
                            } else {
                                $score = 0;
                            }

                            /* SI LE SCORE EST SUPPÉRIEUR À UN CERTAIN NOMBRE, ALORS L'ADRESSE EXISTE (PROBABLEMENT) */
                            if($score > 0.25) {
                                /* ON CRÉE LA COMMANDE, AVEC ETAT PAR DEFAULT: 1 (EN ATTENTE DE VALIDATION) */
                                $result = $connexion->prepare('INSERT INTO commandes (refetat, refuser, addresscommande, nomcommande, prenomcommande, villecommande, codepostal, telcommande, depcommande, adressescore) VALUES (:defaut, :id, :addrLivraison, :nomCommande, :prenomCommande, :villeLivraison, :codePosLivraison, :telCommande, :depLivraison, :adressescore)');
                                $result->execute(array(
                                    'defaut' => '1',
                                    'id' => $_SESSION['id'],
                                    'addrLivraison' => $_POST['addrLivraison'],
                                    'nomCommande' => $_POST['nomCommande'],
                                    'prenomCommande' => $_POST['prenomCommande'],
                                    'villeLivraison' => $_POST['villeLivraison'],
                                    'codePosLivraison' => $_POST['codePosLivraison'],
                                    'telCommande' => $_POST['telCommande'],
                                    'depLivraison' => $_POST['depLivraison'],
                                    'adressescore' => $score
                                ));
                                
                                /* ON SELECTIONNE L'ID DE LA DERNIERE COMMANDE DE L'UTILISATEUR */
                                $latestCommande = $connexion->prepare('SELECT * FROM commandes WHERE refuser=:id ORDER BY idcommande DESC LIMIT 1;');
                                $latestCommande->execute(array(
                                    'id' => $_SESSION['id']
                                ));
                                $latestCommande = $latestCommande->fetchAll();
                                
                                /* ON LIE LES OBJETS DANS LE PANIER DE L'UTILISATEUR A LA COMMMANDE */
                                $result = $connexion->prepare('SELECT * FROM panier2 INNER JOIN articles ON idarticle=idrefarticle WHERE idrefuser=:id');
                                $result->execute(array(
                                    'id' => $_SESSION['id']
                                ));
                                $results = $result->fetchAll();
                                foreach($results as $item) {
                                    $sendPanierToCommande = $connexion->prepare('INSERT INTO commander (refcommande, qtecommande, refarticle) VALUES (:refcommande, :qtecommande, :refarticle)');
                                    $sendPanierToCommande->execute(array(
                                        'refcommande' => $latestCommande[0]['idcommande'],
                                        'refarticle' => $item['idrefarticle'],
                                        'qtecommande' => $item['qte']
                                    ));

                                    /* ON DECREMENTE LE NOMBRE D'ARTICLE DISPONIBLE A LA VENTE */
                                    $setNewQte = $connexion->prepare('UPDATE articles SET qtedispo=qtedispo-:qte WHERE idarticle=:idarticle');
                                    $setNewQte->execute(array(
                                        'idarticle' => $item['refarticle'],
                                        'qte' => $item['qte']
                                    ));

                                    /* ON INCREMENTE LE PRIX TOTAL DES ARTICLES DE LA COMMANDE (Pour pouvoir y mettre un % de réduction plus tard) */
                                    $setNewQte = $connexion->prepare('UPDATE commandes SET prixtotal=prixtotal+:prix WHERE idcommande=:idcommande');
                                    $setNewQte->execute(array(
                                        'idcommande' => $latestCommande[0]['idcommande'],
                                        'prix' => $item['prixvente']*$item['qte']
                                    ));
                                }

                                /* ON VIDE LE PANIER DE L'UTILISATEUR */
                                $sqlEmptyPanier = $connexion->prepare('DELETE FROM panier2 WHERE idrefuser=:id');
                                $sqlEmptyPanier->execute(array(
                                    'id' => $_SESSION['id']
                                ));
                                header('Location: ../../membre.php?succ=21');
                                exit();
                            } else {
                                header('Location: ../../buy.php?err=90');
                                exit();
                            }
                        } else {
                            /* ADRESSE MAL FORMATTER */
                            header('Location: ../../buy.php?err=89');
                            exit();
                        }
                    } else {
                        /* CARTE BANCAIRE MAL FORMATTER */
                        header('Location: ../../buy.php?err=85');
                        exit();
                    }
                } else {
                    /* CARTE BANCAIRE VIDE */
                    header('Location: ../../buy.php?err=20');
                    exit();
                }
            } else {
                /* SI UN CHAMP EST VIDE */
                header('Location: ../../buy.php?err=1');
                exit();
            }
        } else {
            /* SI LA PANIER EST VIDE */
            header('Location: ../../buy.php?err=19');
            exit();
        }
    } else {
        /* SI ON EST PAS PASSÉ PAR LE FORMULAIRE */
        header('Location: ../../');
        exit();
    }
} else {
    /* SI ON EST PAS CONNECTÉ */
    header('Location: ../../');
    exit();
}

?>