<?php
$pageTitle = "Inscription";
include('include/init.php');
include('include/header.php');

// rmdir('../upload/profiles');

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}
// deleteDirectory('../upload/articles');
// deleteDirectory('../upload/thumbs');
// deleteDirectory('../upload/plays');
// deleteDirectory('../upload/profiles');
// deleteDirectory('../upload/plays/7');
// unlink('../upload/plays/7');
// mkdir('../upload/plays/7', 0775);
// mkdir("../upload/articles", 0775);
// mkdir("../upload/thumbs", 0775);
// mkdir("../upload/plays", 0775);
// mkdir("../upload/profiles", 0775);
phpinfo();
?>
    <div class="content">
        <!-- CHANGER LA CLASSE DE CETTE DIV : -->
        <div class="page">
            <h1>Inscrivez-vous pour acceder à la boutique et à toutes les fonctionnalités du site.</h1>

            <!-- LE CONTENU DE LA PAGE VA APRÈS CECI : -->
        </div>
    </div>

<?php
include('include/footer.php');
?>