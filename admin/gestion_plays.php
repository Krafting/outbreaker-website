<?php
$adminPage = true; $titrePage = "Gestion des Plays";
include('../include/init.php');
include('header.php');

if (isset($_GET['idplay']) && !empty($_GET['idplay']) && is_numeric($_GET['idplay'])) {

    /* Pour delete un commentaire*/
    if (isset($_POST['del_comment'])) {
        
        $sql_comment_exist=$connexion->prepare("SELECT count(*) FROM commentairesplay where idcommentaireplay=:idcommentaireplay and refplay=:idplay ;");
        $sql_comment_exist->execute(array(
            'idcommentaireplay' => $_POST['idcommentaireplay'],
            'idplay' => $_GET['idplay']
        ));

        $sql_comment_exist=$sql_comment_exist->fetch();

        if ($sql_comment_exist[0]>0) {

            $sql_del_comment=$connexion->prepare("DELETE FROM commentairesplay where idcommentaireplay=:idcommentaireplay and refplay=:idplay;");
            $sql_del_comment->execute(array(
                'idcommentaireplay' => $_POST['idcommentaireplay'],
                'idplay' => $_GET['idplay']
            )); 
            header('Location: ./gestion_plays.php?idplay='.$_GET['idplay'].'&succ=79');
            exit();
        } else {
         header('Location: ./gestion_plays.php?idplay='.$_GET['idplay'].'&err=78');   
         exit();
        }
    } 
    
    /* ON CHECK SI LE PLAY EXISTE */
    $findPlay = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE idvideo=:idvideo');
    $findPlay->execute(array(
        'idvideo' => $_GET['idplay']
    ));
    $check = $findPlay->fetch();

    if($check[0] > 0) {
        /* ON SELECTIONNE LE PLAYS DEmANDER */
        $selectPlay = $connexion->prepare('SELECT * FROM plays WHERE idvideo=:idvideo');
        $selectPlay->execute(array(
            'idvideo' => $_GET['idplay']
        ));
        $playEdit = $selectPlay->fetch();
?>
            <?php if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
    <h1>Edition du Play</h1>
    <hr style="margin-bottom: 30px;">
    <form action="../include/forms/admin/editPlay.php" method="POST" enctype="multipart/form-data">
        <div class="flexBlock">
            <div class="flexContent">
                <input type="hidden" value="<?php echo $_GET['idplay']; ?>" name="idvideo">
                <input type="hidden" value="<?php echo $playEdit['refuser']; ?>" name="iduser">
                <img style="width: 100%" height="355px" src="../../upload/thumbs/<?php echo $playEdit['refuser'].'/'.$playEdit['miniature']; ?>"></img>
                <p>Miniature du Play</p>
                <input type="file" name="playThumb">            </div>
            <div class="flexContent">
                <p>Titre du Play</p>
                <input type="text" required name="titrePlay" placeholder="Titre de votre Play" value="<?php echo $playEdit['titrevideo']; ?>">

                <p>Description du Play</p>
                <textarea style="height: 145px;" name="descPlay" placeholder="Description de votre Play"><?php echo $playEdit['descriptionvideo']; ?></textarea>
                <p>Catégorie du Play</p>
                <select name="catPlay">
                    <?php 
                    /* ON SELECTIONNE TOUTE LES CATÉGORIES POSSIBLE DES VIDÉOS */
                    $selectAllCategorie = $connexion->prepare('SELECT * FROM typevideo');
                    $selectAllCategorie->execute();
                    $categories = $selectAllCategorie->fetchAll();
                    foreach($categories as $categorie) {
                        echo "<option value='".$categorie['idtypevideo']."'"; 
                        if($categorie['idtypevideo'] == $playEdit['reftypevideo']) { echo "selected"; }
                        echo ">".$categorie['nomtypevideo']."</option>";
                    }
                    ?>
                </select>
            </div>
            </div>
            <div class="sendButton">
                <button name="sendPlay" class="btn" type="submit">Envoyer ce Play !</button> 
                <a class='btn' style="margin-left: 25px;" onclick='openModal("delPlay")' href='#'>Supprimer ce Play</a>
            </div>
        </form>
            <div class="flexBlock">
                <div class="flexContent">
                    <h1>Commentaires</h1>
                    <hr style="margin-bottom: 30px;">
                        <!-- Pagination de l'affichage des commentaires -->

<style>
.avisArticle .profileBlock a { 
    vertical-align: top;
    margin: 40px;
    display: inline-block
}
.avisArticle .profileBlock img {
    height: 100px !important; width: 100px;
}
.avisArticle .flexBlock.oneChild {    
    background: var(--light-grey);
    border-radius: 10px;
}
</style>
                       

                        <?php 
                            $sql_nb_comment_plays=$connexion->prepare("SELECT COUNT(*) FROM commentairesplay where refplay=:idplay ;");
                            $sql_nb_comment_plays->execute(array(
                                'idplay' => $_GET['idplay']
                            ));

                            $count=$sql_nb_comment_plays->fetch();

                            /* retourne un entier superieur */
                            $nb_comment_page=2; /*modifier valeur de comm/page */
                            $nb_page= ceil ($count[0] / $nb_comment_page);
                            /* arrondie au nb superieur*/

                            if (!empty($_GET['p']) && is_numeric($_GET['p'])) {
                                $page=$_GET['p'];
                                if ($page>$nb_page) {
                                    $page = $nb_page;
                                }
                            } else {
                                $page=1; 
                            }
                            $first_comment=($page-1)*$nb_comment_page;
                            $sql= $connexion->prepare("SELECT * from commentairesplay inner join users on refusersent=iduser where refplay=:idplay  ORDER BY idcommentaireplay DESC LIMIT ".$nb_comment_page." OFFSET ".$first_comment); 
                            $sql->execute(array(
                                'idplay' => $_GET['idplay']
                            ));
                            $sql=$sql->fetchall();
                            
                            foreach ($sql as $ligne) {
                                $commentaire=$ligne['textecommentaireplay'];
                                $iduser=$ligne['iduser'];
                                $pseudo=$ligne['pseudo'];
                                $idcommentaireplay=$ligne['idcommentaireplay'];
                                ?>
                            <div class="avisArticle" style="margin-top: 60px;">
                                <div style="flex-flow:column; margin-bottom: 30px">
                                    <div class="flexBlock oneChild">
                                        <div class="flexContent">
                                            <div class="profileBlock" style="max-height: 100px;margin-top: 7px">
                                                <img style="border-radius: 10px" src="../../upload/profiles/<?php echo findProfilePic($iduser); ?>"> 
                                            </div>
                                        </div>
                                            <div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                                                 <?php echo formatTexte($commentaire); ?>
                                            </div>
                    
                                            <form method="post">
                                            <input type="hidden" name="idcommentaireplay" value="<?php echo $idcommentaireplay; ?>">  
                                            <div class="flexContent">
                                                <div class="sendButton">
                                                    <button type="submit" name="del_comment" class="btn">Supprimer</button>        
                                                </div>
                                            </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    <!-- LA PAGINATION DES ARTICLES -->
                    <div class="pagination">
                        <?php if($nb_page > 0) { ?>
                        <span class="pageB infos">PAGES</span>
                        <?php  } ?>
                        <?php for($i = 1; $i <= $nb_page; $i++) {
                                if($i == $page) {
                                    echo '<span class="pageB active">'.$i.'</span>';
                                } else {
                                    echo '<a href="?'.findCurrentURIArgs($_SERVER['REQUEST_URI']).'&p='.$i.'" class="pageB">'.$i.'</a>';
                                }
                            } ?>
                        </div>
                    </div>
    <!-- MODAL DE SUPPRESSION -->
    <div id="delPlay" class='modal'>
        <div class='modal-content'>
            <div class='modal-header'>
                <span class='close'>&times;</span>
                <h2>Êtes-vous sûr de vous ?</h2>
            </div>
                <h1>Êtes vous sur de vouloir supprimer ce Play ?</h1>
                
                <form style="padding-bottom: 75px;" action="../include/forms/admin/editPlay.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="<?php echo $_GET['idplay']; ?>" name="idvideo">
                    <input type="hidden" value="<?php echo $playEdit['refuser']; ?>" name="iduser">
                    <button name="delPlay" class="btn" type="submit">Supprimer mon Play !</button>
                </form>
            </p>
        </div>
    </div>
    <script src="../include/js/js.js"></script>
<?php  } else { 
        header('Location: gestion_plays.php'); 
        exit();
    }
} else {  

   if(isset($_GET['err']) OR isset($_GET['succ'])) {
        if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
            $idMsg = $_GET['err'];
            echo getMessage($idMsg);
        }
        if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
            $idMsg = $_GET['succ'];
            echo getMessage($idMsg);
        }
    }
 	
?>
 
<h1>Gestion des Plays</h1>
    <hr style="margin-bottom: 30px;">
<table>
    	<tr>
    		<td>#</td>
    		<td>Nom</td>
    		<td>Utilisateur</td>
    		<td>Nombre de vues</td>
    		<td>Action</td>
    	</tr>
<?php
		$sql_idplay = $connexion->prepare("SELECT * from plays inner join users on refuser=iduser ORDER BY idvideo DESC;");
    	$sql_idplay->execute();
        $sql_idplay=$sql_idplay->fetchAll();
        foreach ($sql_idplay as $play) {
        	$idvideo=$play['idvideo'];
        	$nomvideo=$play['titrevideo'];
        	$user=$play['pseudo'];
        	$iduser=$play['iduser'];
            
        	echo "<tr><td>".$idvideo."</td>";
        	echo "<td style='word-break: break-all;'> ".$nomvideo."</td>";
        	echo "<td style='text-align: left;'><a href='users.php?id=".$iduser."'>".$user."</a></td>";
        	echo "<td> ".findNumViews($idvideo)." Vues</td>";
        	echo"<td><a class=' ' href='?idplay=".$idvideo."' name='modifier'>Modifier</a></td>";
        }

?>

</table>
<?php } ?>
