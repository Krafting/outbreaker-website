<?php
$needConnect = true;
$isBoutique = true;
$pageTitle = "Votre Panier";
include('include/init.php');
/* AJOUT D'ARTICLE DANS LE PANIER */
if(isset($_GET['addArticle']) AND is_numeric($_GET['addArticle'])) {

    /* ON CHECK SI L'ARTICLE EXISTE ET LA QUANTITÉ DISPONIBLE */
    $findArticle = $connexion->prepare('SELECT * FROM articles WHERE idarticle=:idarticle');
    $findArticle->execute(array(
        'idarticle' => $_GET['addArticle']
    ));
    $qtedispor = $findArticle->fetch();

    $findArticle = $connexion->prepare('SELECT COUNT(*) FROM articles WHERE idarticle=:idarticle');
    $findArticle->execute(array(
        'idarticle' => $_GET['addArticle']
    ));
    $check = $findArticle->fetch();

    /* SI L'ARTICLE EXISTE, ON L'AJOUTE AU PANIER DE L'UTILISATEUR */
    if($check[0] == 1) {
        if($qtedispor['qtedispo'] > 0) {
            /* ON REGARDE SI L'UTILISATEUR A DEJA LARTICLE DANS SON PANIER */
            $findArticlePanier = $connexion->prepare('SELECT * FROM panier2 WHERE idrefarticle=:idarticle AND idrefuser=:iduser');
            $findArticlePanier->execute(array(
                'idarticle' => $_GET['addArticle'],
                'iduser' => $_SESSION['id']
            ));
            $check = count($findArticlePanier->fetchAll());
        
            /* SI L'ARTICLE EST DANS LE PANIER DE L'UTILISATEUR ALORS ON EDIT LA QTE */
            if($check > 0) {
                $sqlQuantite = $connexion->prepare("UPDATE panier2 SET qte=qte+1 WHERE idrefuser=:iduser AND idrefarticle=:idarticle");
                $sqlQuantite->execute(array(
                    'idarticle' => $_GET['addArticle'],
                    'iduser' => $_SESSION['id']
                ));

            } else {
                $sentToBdd = $connexion->prepare('INSERT INTO panier2 (idrefuser, idrefarticle, qte) VALUES (:refuser, :refarticle, :panier)');
                $sentToBdd->execute(array(
                    'refuser' => $_SESSION['id'],
                    'refarticle' => $_GET['addArticle'],
                    'panier' => 1
                ));
            }
            
            header('Location: ?succ=30');
            exit();
        } else {
            header('Location: ?err=32');
            exit();
        }
    } else {
        header('Location: ?err=28');
        exit();
    }
}
/* DELETION D'ARTICLE DANS LE PANIER */
if(isset($_GET['remArticle']) AND is_numeric($_GET['remArticle'])) {

    /* ON CHECK SI L'ARTICLE EST DANS LE PANIER DE L'UTILISATEUR */
    $findArticlePanier = $connexion->prepare('SELECT * FROM panier2 WHERE idrefarticle=:idarticle AND idrefuser=:iduser');
    $findArticlePanier->execute(array(
        'idarticle' => $_GET['remArticle'],
        'iduser' => $_SESSION['id']
    ));
    $check = count($findArticlePanier->fetchAll());

    /* SI L'ARTICLE EST DANS LE PANIER DE L'UTILISATEUR ALORS ON LE SUPPRIME */
    if($check == 1) {
        
        /* ON CHECK LA QUANTITÉ DE L'ARTICLE*/
        $findArticlePaniers = $connexion->prepare('SELECT * FROM panier2 WHERE idrefarticle=:idarticle AND idrefuser=:iduser');
        $findArticlePaniers->execute(array(
            'idarticle' => $_GET['remArticle'],
            'iduser' => $_SESSION['id']
        ));
        $qte = $findArticlePaniers->fetch();

        /* SI ELLE EST SUPERIEUR A 1 ALORS ON NE RETIRE L'ARTICLE QU'UNE SEULE FOIS */
        if($qte['qte'] > 1) {
            $sqlQuantite = $connexion->prepare("UPDATE panier2 SET qte=qte-1 WHERE idrefuser=:iduser AND idrefarticle=:idarticle");
            $sqlQuantite->execute(array(
                'idarticle' => $_GET['remArticle'],
                'iduser' => $_SESSION['id']
            ));
        } else {
            $deletePanierArt = $connexion->prepare('DELETE FROM panier2 WHERE idrefarticle=:idarticle AND idrefuser=:iduser');
            $deletePanierArt->execute(array(
                'idarticle' => $_GET['remArticle'],
                'iduser' => $_SESSION['id']
            ));
        }
        header('Location: ?succ=31');
        exit();
    } else {
        header('Location: ?err=29');
        exit();
    }
}

include('include/header.php');
?>

    <div class="content">
        <!-- CHANGER LA CLASSE DE CETTE DIV : -->
        <div class="page">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1>Votre Panier!</h1>
		    <table>
                <tr>
                    <td style='width: 5%'>Quantité</td>
                    <td>Article(s)</td>
                    <td>Prix (En €)</td>
                    <td style="width: 10%;">Retirer</td>
                </tr>
                <?php 
                /* ON AFFICHE LES ARTICLES DU PANIER DE L'UTILISATEUR */ 
                $selectPanier = $connexion->prepare('SELECT * FROM panier2 INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                $selectPanier->execute(array(
                    'id' => $_SESSION['id']
                ));
                $articles = $selectPanier->fetchAll();

                foreach($articles as $article) {
                    echo "<tr>
                            <td>".$article['qte']." x</td>
                            <td>".$article['nomarticle']." <a href='article.php?id=".$article['idarticle']."' target='blank'>Lien</a></td>
                            <td>".$article['prixvente']." €</td>
                            <td><a href='?remArticle=".$article['idrefarticle']."'>Retirer</a></td>
                        </tr>";
                }
                /* SI ON A PAS D'ARTICLES DANS LE PANIER */
                if(count($articles) == 0) {
                    echo "  <tr>
                                <td colspan='4'>
                                    Vous n'avez pas d'articles dans votre panier.
                                </td>
                            </tr>";
                /* SINON : ON A DES ARTICLES ET ON AFFICHE LE MONTANT TOTAL */ 
                } else {
                    $result = $connexion->prepare('SELECT sum(prixvente*qte) FROM panier2 INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                    $result->execute(array(
                        'id' => $_SESSION['id']
                    ));
                    $result = $result->fetch();
                    echo "<tr>
                                <td colspan=2 style='text-align: right;'> 
                                    Prix d'achat total de vos articles.
                                </td>
                                <td>".round($result[0], 2)." €</td>
                            </tr>";
                }
                ?>
            </table>
           <?php if(count($articles) > 0) { ?><a href="buy.php" class="btn" style="float: right">Passer commande !</a><?php } ?>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <!-- LE CONTENU DE LA PAGE VA APRÈS CECI : -->
        </div>
    </div>

<?php
include('include/footer.php');
?>
