--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.articles (
    idarticle integer NOT NULL,
    nomarticle text NOT NULL,
    qtedispo integer NOT NULL,
    prixvente real NOT NULL,
    desarticle text,
    reftypearticle integer NOT NULL,
    keywords text,
    imageprincipale text
);


ALTER TABLE public.articles OWNER TO rt1projet10;

--
-- Name: articles_idarticle_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.articles_idarticle_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_idarticle_seq OWNER TO rt1projet10;

--
-- Name: articles_idarticle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.articles_idarticle_seq OWNED BY public.articles.idarticle;


--
-- Name: avisarticle; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.avisarticle (
    idrefarticle integer NOT NULL,
    idrefuser integer NOT NULL,
    textavis text NOT NULL,
    noteavis integer NOT NULL
);


ALTER TABLE public.avisarticle OWNER TO rt1projet10;

--
-- Name: commander; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.commander (
    idcommander integer NOT NULL,
    refcommande integer NOT NULL,
    qtecommande integer NOT NULL,
    refarticle integer NOT NULL
);


ALTER TABLE public.commander OWNER TO rt1projet10;

--
-- Name: commander_idcommander_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.commander_idcommander_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commander_idcommander_seq OWNER TO rt1projet10;

--
-- Name: commander_idcommander_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.commander_idcommander_seq OWNED BY public.commander.idcommander;


--
-- Name: commandes; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.commandes (
    idcommande integer NOT NULL,
    refetat integer NOT NULL,
    addresscommande text NOT NULL,
    extras text,
    nomcommande text NOT NULL,
    prenomcommande text NOT NULL,
    telcommande text,
    depcommande text NOT NULL,
    codepostal text NOT NULL,
    villecommande text NOT NULL,
    refuser integer NOT NULL,
    prixtotal numeric DEFAULT 0 NOT NULL,
    adressescore numeric DEFAULT 0
);


ALTER TABLE public.commandes OWNER TO rt1projet10;

--
-- Name: commandes_idcommande_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.commandes_idcommande_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commandes_idcommande_seq OWNER TO rt1projet10;

--
-- Name: commandes_idcommande_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.commandes_idcommande_seq OWNED BY public.commandes.idcommande;


--
-- Name: commentaires; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.commentaires (
    idcommentaire integer NOT NULL,
    textecommentaire text NOT NULL,
    refusersent integer NOT NULL,
    refuserreceived integer NOT NULL,
    datecommentaire date NOT NULL,
    timecommentaire bigint DEFAULT 0
);


ALTER TABLE public.commentaires OWNER TO rt1projet10;

--
-- Name: commentaires_idcommentaire_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.commentaires_idcommentaire_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commentaires_idcommentaire_seq OWNER TO rt1projet10;

--
-- Name: commentaires_idcommentaire_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.commentaires_idcommentaire_seq OWNED BY public.commentaires.idcommentaire;


--
-- Name: commentairesplay; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.commentairesplay (
    idcommentaireplay integer NOT NULL,
    textecommentaireplay text NOT NULL,
    refusersent integer NOT NULL,
    datecommentaireplay date NOT NULL,
    timecommentaireplay bigint DEFAULT 0,
    refplay integer NOT NULL
);


ALTER TABLE public.commentairesplay OWNER TO rt1projet10;

--
-- Name: commentairesplay_idcommentaireplay_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.commentairesplay_idcommentaireplay_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commentairesplay_idcommentaireplay_seq OWNER TO rt1projet10;

--
-- Name: commentairesplay_idcommentaireplay_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.commentairesplay_idcommentaireplay_seq OWNED BY public.commentairesplay.idcommentaireplay;


--
-- Name: etats; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.etats (
    idetat integer NOT NULL,
    nometat character varying(25) NOT NULL
);


ALTER TABLE public.etats OWNER TO rt1projet10;

--
-- Name: etats_idetat_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.etats_idetat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etats_idetat_seq OWNER TO rt1projet10;

--
-- Name: etats_idetat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.etats_idetat_seq OWNED BY public.etats.idetat;


--
-- Name: imagesarticle; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.imagesarticle (
    idimagearticle integer NOT NULL,
    nomfichier text NOT NULL,
    refarticle integer NOT NULL
);


ALTER TABLE public.imagesarticle OWNER TO rt1projet10;

--
-- Name: imagesarticle_idimagearticle_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.imagesarticle_idimagearticle_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.imagesarticle_idimagearticle_seq OWNER TO rt1projet10;

--
-- Name: imagesarticle_idimagearticle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.imagesarticle_idimagearticle_seq OWNED BY public.imagesarticle.idimagearticle;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.messages (
    idmessage integer NOT NULL,
    textmessage text NOT NULL,
    typemessage integer NOT NULL
);


ALTER TABLE public.messages OWNER TO rt1projet10;

--
-- Name: messages_idmessage_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.messages_idmessage_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_idmessage_seq OWNER TO rt1projet10;

--
-- Name: messages_idmessage_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.messages_idmessage_seq OWNED BY public.messages.idmessage;


--
-- Name: panier; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.panier (
    idpanier integer NOT NULL,
    refuser integer NOT NULL,
    refarticle integer NOT NULL
);


ALTER TABLE public.panier OWNER TO rt1projet10;

--
-- Name: panier2; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.panier2 (
    idrefuser integer NOT NULL,
    idrefarticle integer NOT NULL,
    qte integer NOT NULL
);


ALTER TABLE public.panier2 OWNER TO rt1projet10;

--
-- Name: panier_idpanier_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.panier_idpanier_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.panier_idpanier_seq OWNER TO rt1projet10;

--
-- Name: panier_idpanier_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.panier_idpanier_seq OWNED BY public.panier.idpanier;


--
-- Name: plays; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.plays (
    idvideo integer NOT NULL,
    titrevideo text NOT NULL,
    nomfichiervideo text NOT NULL,
    nomlienvideo text,
    reftypevideo integer NOT NULL,
    descriptionvideo text,
    miniature text,
    refuser integer NOT NULL,
    nbviews integer DEFAULT 0,
    homepage integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.plays OWNER TO rt1projet10;

--
-- Name: rangs; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.rangs (
    idrang integer NOT NULL,
    nomrang character varying(30) NOT NULL
);


ALTER TABLE public.rangs OWNER TO rt1projet10;

--
-- Name: rangs_idRang_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public."rangs_idRang_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."rangs_idRang_seq" OWNER TO rt1projet10;

--
-- Name: rangs_idRang_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public."rangs_idRang_seq" OWNED BY public.rangs.idrang;


--
-- Name: typearticle; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.typearticle (
    idtype integer NOT NULL,
    nomtype text NOT NULL
);


ALTER TABLE public.typearticle OWNER TO rt1projet10;

--
-- Name: typearticle_idtype_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.typearticle_idtype_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.typearticle_idtype_seq OWNER TO rt1projet10;

--
-- Name: typearticle_idtype_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.typearticle_idtype_seq OWNED BY public.typearticle.idtype;


--
-- Name: typevideo; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.typevideo (
    idtypevideo integer NOT NULL,
    nomtypevideo character varying(40)
);


ALTER TABLE public.typevideo OWNER TO rt1projet10;

--
-- Name: typevideo_idTypeVideo_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public."typevideo_idTypeVideo_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."typevideo_idTypeVideo_seq" OWNER TO rt1projet10;

--
-- Name: typevideo_idTypeVideo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public."typevideo_idTypeVideo_seq" OWNED BY public.typevideo.idtypevideo;


--
-- Name: users; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.users (
    iduser integer NOT NULL,
    pseudo text NOT NULL,
    pass text NOT NULL,
    mail text NOT NULL,
    salt text,
    refrang integer DEFAULT 1 NOT NULL,
    session text,
    verifaccount text,
    profilepicture text,
    about character varying(5000),
    dateinscri date,
    allowcommentaire integer DEFAULT 1,
    resetlink text,
    resetdate bigint DEFAULT 0
);


ALTER TABLE public.users OWNER TO rt1projet10;

--
-- Name: users_iduser_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.users_iduser_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_iduser_seq OWNER TO rt1projet10;

--
-- Name: users_iduser_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.users_iduser_seq OWNED BY public.users.iduser;


--
-- Name: videos_idvideo_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.videos_idvideo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.videos_idvideo_seq OWNER TO rt1projet10;

--
-- Name: videos_idvideo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.videos_idvideo_seq OWNED BY public.plays.idvideo;


--
-- Name: viewplay; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.viewplay (
    idview integer NOT NULL,
    refplay integer NOT NULL,
    refuser integer,
    ip character varying(28) NOT NULL,
    dateview date NOT NULL
);


ALTER TABLE public.viewplay OWNER TO rt1projet10;

--
-- Name: viewplay_idview_seq; Type: SEQUENCE; Schema: public; Owner: rt1projet10
--

CREATE SEQUENCE public.viewplay_idview_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.viewplay_idview_seq OWNER TO rt1projet10;

--
-- Name: viewplay_idview_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rt1projet10
--

ALTER SEQUENCE public.viewplay_idview_seq OWNED BY public.viewplay.idview;


--
-- Name: wishlist; Type: TABLE; Schema: public; Owner: rt1projet10
--

CREATE TABLE public.wishlist (
    idrefuser integer NOT NULL,
    idrefarticle integer NOT NULL
);


ALTER TABLE public.wishlist OWNER TO rt1projet10;

--
-- Name: articles idarticle; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.articles ALTER COLUMN idarticle SET DEFAULT nextval('public.articles_idarticle_seq'::regclass);


--
-- Name: commander idcommander; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commander ALTER COLUMN idcommander SET DEFAULT nextval('public.commander_idcommander_seq'::regclass);


--
-- Name: commandes idcommande; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commandes ALTER COLUMN idcommande SET DEFAULT nextval('public.commandes_idcommande_seq'::regclass);


--
-- Name: commentaires idcommentaire; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentaires ALTER COLUMN idcommentaire SET DEFAULT nextval('public.commentaires_idcommentaire_seq'::regclass);


--
-- Name: commentairesplay idcommentaireplay; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentairesplay ALTER COLUMN idcommentaireplay SET DEFAULT nextval('public.commentairesplay_idcommentaireplay_seq'::regclass);


--
-- Name: etats idetat; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.etats ALTER COLUMN idetat SET DEFAULT nextval('public.etats_idetat_seq'::regclass);


--
-- Name: imagesarticle idimagearticle; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.imagesarticle ALTER COLUMN idimagearticle SET DEFAULT nextval('public.imagesarticle_idimagearticle_seq'::regclass);


--
-- Name: messages idmessage; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.messages ALTER COLUMN idmessage SET DEFAULT nextval('public.messages_idmessage_seq'::regclass);


--
-- Name: panier idpanier; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier ALTER COLUMN idpanier SET DEFAULT nextval('public.panier_idpanier_seq'::regclass);


--
-- Name: plays idvideo; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.plays ALTER COLUMN idvideo SET DEFAULT nextval('public.videos_idvideo_seq'::regclass);


--
-- Name: rangs idrang; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.rangs ALTER COLUMN idrang SET DEFAULT nextval('public."rangs_idRang_seq"'::regclass);


--
-- Name: typearticle idtype; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.typearticle ALTER COLUMN idtype SET DEFAULT nextval('public.typearticle_idtype_seq'::regclass);


--
-- Name: typevideo idtypevideo; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.typevideo ALTER COLUMN idtypevideo SET DEFAULT nextval('public."typevideo_idTypeVideo_seq"'::regclass);


--
-- Name: users iduser; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.users ALTER COLUMN iduser SET DEFAULT nextval('public.users_iduser_seq'::regclass);


--
-- Name: viewplay idview; Type: DEFAULT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.viewplay ALTER COLUMN idview SET DEFAULT nextval('public.viewplay_idview_seq'::regclass);


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.articles (idarticle, nomarticle, qtedispo, prixvente, desarticle, reftypearticle, keywords, imageprincipale) FROM stdin;
33	T-Shirt Cisco Pro Team	50	49.9500008	Un superbe t-shirt style e-sport pour booster vos connaissances lors de vos passages de certifications cisco.\r\n\r\nDonne un bonus de connaissances de +10 sur Cisco IOS, ainsi qu'un bonus d'xp pour chaque VLAN configuré.	51	cisco t-shirt pro	78a7014c0b877aa60e46e7aa92323db4eb444dfb.jpg
2	Tarte	0	19.9500008	Cet article est un chapeau de type béret. Pour avoir un maximum de charisme.	1	tarte 	a88d769ee215ad8d20081e036818b2da77427587.jpeg
27	Talon Knife (CS:GO)	250	39.9900017	Réplique réel du couteau Talon dans le jeu Counter-Strike Global Offensive.\r\n\r\nPlus de détail\r\n\r\n    Blade finish: Gloss Coating\r\n    Blade material: 420 Stainless Steel\r\n    Handle material: ABS - Acrylonitrile Butadiene Styrene w/ Fiberglass\r\n    Weight: 3.83 grams\r\n\r\n	47	csgo, knife, talon, replica	ec3498e24bbd71526816b96b212842d67c212f42.jpg
3	Casquette Orange Box	11	29.9899998	Une Casquette Valve avec comme caracteristique principale ces couleurs et motif ressamblant a l'original Orange Box de valve	2	snapback, casquette, orange, half life, valve	345b31940cc81826b071c37a18c1cd5e7880ad42.png
\.


--
-- Data for Name: avisarticle; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.avisarticle (idrefarticle, idrefuser, textavis, noteavis) FROM stdin;
33	1	Super T-Shirt, il m'a bien aidé lors de ma CCNA1!	5
27	1	Tranchant !	4
27	7	Le couteau est de bonne manufacture, il est equilibré, résistant et il est bien aiguisé. \r\n\r\nJe met donc une note de 5 étoiles.\r\n\r\nDe plus la livraison est rapide. 	5
\.


--
-- Data for Name: commander; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.commander (idcommander, refcommande, qtecommande, refarticle) FROM stdin;
54	44	1	27
55	44	3	33
56	45	1	3
57	46	1	27
\.


--
-- Data for Name: commandes; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.commandes (idcommande, refetat, addresscommande, extras, nomcommande, prenomcommande, telcommande, depcommande, codepostal, villecommande, refuser, prixtotal, adressescore) FROM stdin;
44	1	1 Rue du parc	\N	ALDERSON	Elliot		Allier	03200	Vichy	1	189.84	0.96315272727273
45	1	5224 Le Bourg	\N	Waters	Berniece		Cantal	15110	Saint-Urcize	1	29.99	0.40494597402597
46	1	3 place de la mairie	\N	6sko	enable	06-02-03-06-06	Côte-d'or	21220	Collonges-lès-Bévy	7	39.99	0.93119272727273
\.


--
-- Data for Name: commentaires; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.commentaires (idcommentaire, textecommentaire, refusersent, refuserreceived, datecommentaire, timecommentaire) FROM stdin;
30	NOTRE BOUTIQUE\r\nFaites vous plaisir et commandez vos vetements aux couleurs de OutBreaker eSport! 	1	1	2021-06-07	1623082648
\.


--
-- Data for Name: commentairesplay; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.commentairesplay (idcommentaireplay, textecommentaireplay, refusersent, datecommentaireplay, timecommentaireplay, refplay) FROM stdin;
23	Trop bien!	1	2021-06-13	1623610354	35
\.


--
-- Data for Name: etats; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.etats (idetat, nometat) FROM stdin;
1	En attente
2	Validée
3	En Transit
4	Livrée
5	Annulée
\.


--
-- Data for Name: imagesarticle; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.imagesarticle (idimagearticle, nomfichier, refarticle) FROM stdin;
7	67fb9a8b54e787b6f0e2736ab4ca06ace255f0a0.jpg	27
8	e8c15470579f98a26b0f2addf2fd5b1b104c22df.jpg	27
9	ee59d24db378ebe8bd17157a14a1d2b9b105b31c.jpg	2
13	f66bc80248eee2ea47e5bb6fdb9699ef4a4e166b.png	3
14	ad14914baffece1a947c8fbb87f2b540f8d4e0d0.png	3
16	a0f5f7cdf183a5b2b3ebca88a33c75d37b162d5c.jpg	33
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.messages (idmessage, textmessage, typemessage) FROM stdin;
2	Erreur : Vérifiez que votre ancien mot de passe est correct.	0
4	Erreur : L'email entrée n'est pas dans le bon format.	0
5	Erreur : Vérifiez que les mots de passes correspondent.	0
6	Erreur : Essaie pas de casser notre site (stp).	0
7	Erreur : L'image rentrée n'est pas dans le bon format (formats acceptés: gif, jpg, png, jpeg).	0
9	Erreur : Une erreur est survenue pendant le téléchargement de l'image.	0
10	Votre mot de passe a été modifié avec succès.	1
11	Votre compte a été modifié avec succès.	1
12	Votre profil a été modifié avec succès.	1
13	Votre photo de profil a été modifié avec succès.	1
14	Cette image a été supprimé avec succès de nos serveurs.	1
15	Votre commande a bien été placé, vous pouvez y suivre l'activité depuis votre espace membre (en bas de cette page).	1
16	Votre avis a été ajouté avec succès.	1
17	Votre avis a été modifié avec succès.	1
18	Erreur : Une erreur est survenue.	0
1	Erreur : Un des champs requis est vide.	0
21	Votre commande a bien été placé, vous pouvez y suivre l'activité depuis votre espace membre.	1
22	Erreur : Cette adresse e-mail est déjà utilisée. </br> Il n'est pas possible d'utiliser deux fois la même adresse e-mail	0
23	Erreur : Ce pseudo existe déjà.	0
3	Erreur : Vérifiez que les mots de passes correspondent.	0
25	Erreur : Votre description ne respecte pas la taille limite (1250 caractères)	0
27	Vous êtes inscrit! Vous pouvez désormais vous connecter.	1
31	L'article a bien été retiré de votre panier.	1
30	L'article a bien été ajouté dans votre panier.	1
58	Votre Play a été modifié avec succès.	1
34	La catégorie à été modifié avec succès	1
28	Erreur : L'article n'existe pas.	0
75	Un lien de réinitialisation de mot de passe vous a été envoyé par mail. Ce lien expirera dans 2 heures.	1
20	Erreur : Vos coordonnées bancaires ne sont pas remplisent correctement.	0
33	Erreur : Votre avis ne peux pas être vide!	0
29	Erreur : Cet article n'est pas dans votre panier.	0
19	Erreur : Votre panier est vide, vous ne pouvez pas passer commande.	0
32	Erreur : L'article est en rupture de stock.	0
36	La catégorie à été supprimé avec succès 	1
35	Erreur : Vous ne pouvez pas supprimer une catégorie si elle est lié à un article.	0
37	Erreur : La catégorie n'existe pas	0
38	La catégorie as bien été créé	1
24	Le pseudonyme n'est pas accepté. Le pseudonyme doit comprendre de 2 à 25 caractères.</br>Les caractères autorisés sont: <br><ul><li>Lettres de A à Z et chiffres de 0 à 9</li><li> Les traits d'union et les sous-tirets</li>	0
39	Erreur : Vous n'avez pas rentré de nom de catéogrie	0
40	Le nom de l'article a bien été modifié 	1
41	L'article a bien été supprimé	1
43	L'article a bien été ajouté a votre wishlist.	1
45	Erreur : L'article n'est pas dans votre wishlist.	0
46	L'article a bien été créé	1
47	Erreur : L'utilisateur n'existe pas	0
48	Erreur : Cet utilisateur n'accepte pas les commentaires sur son profil.	0
49	Votre commentaire a été envoyé avec succès!	1
53	Vous ne pouvez pas annuler cette commande.	0
54	Votre commande a été annulée avec succès.	1
44	L'article a été retiré avec succès de votre wishlist.	1
55	L'article as bien été modifié	1
57	Erreur : l'article n'as pas été modifié 	0
59	Votre Play a été mis en ligne avec succès.	1
60	Erreur : Une erreur est survenue lors de l'envoie d'un fichier.	0
42	Erreur : L'article n'existe pas.	0
56	Erreur : L'article n'existe pas.	0
26	Erreur : Vérifiez que le pseudo et le mot de passe correspondent.	0
61	Erreur : Ce Play n'existe pas.	0
50	Erreur : Vous devez être connecté afin de pouvoir commenté.	0
62	Votre play a été supprimé avec succès.	1
63	Erreur : Ce play n'existe pas, ou ne vous appartient pas.	0
64	Erreur : Vous ne pouvez pas supprimer un utilisateur qui n'existe pas.	0
65	Erreur : Vous ne pouvez pas supprimer un administrateur ou modérateur.	0
66	L'utilisateur a bien été supprimé.	1
67	Ce compte a bien été modifié	1
68	Erreur : Ce rang n'existe pas.	0
69	Erreur : Le pseudo ne peut pas être vide.	0
71	Le mot de passe de cet utilisateur a été modifié avec succès.	1
72	Erreur : Cette image n'existe pas.	0
70	Ce profil a été modifié avec succès	1
73	La commande a été modifié avec succès	1
74	Erreur : Le delai de ce lien a expiré, veuillez en générer un nouveau.	0
76	Erreur : Vous ne pouvez pas supprimer une catégorie si elle est lié à un Play. 	0
77	Erreur : Un article possède déjà ce nom	0
78	Erreur : Ce commentaire n'existe pas.	0
79	Votre commentaire a été supprimé avec succès.	1
80	Ce commentaire a été supprimé avec succès.	1
81	Vous avez supprimé la photo avec succès	1
82	Erreur : Cette image n'existe pas.	0
8	Erreur : L'image est trop volumineuse.	0
83	Erreur : La miniature séléctionnée est trop volumineuse.	0
84	Erreur : La vidéo séléctionnée est trop volumineuse.	0
85	Erreur : Votre carte bancaire a mal été renseignée.	0
86	Erreur : Une erreur est survenue lors de l'envoie du mail. Veuillez réessayer plus tard.	0
87	Un mail vous a été envoyé avec un lien pour réinitialiser votre mot de passe dans votre boite mail.	1
88	Erreur : Aucun utilisateur n'est enregistré avec cette adresse mail.	0
89	Erreur : Votre adresse est mal formatté, veuillez réessayer	0
90	Erreur : Votre adresse semble être inexistante, veuillez réessayer ou utiliser une autre adresse.	0
91	Erreur : Ce rang n'existe pas.	0
93	Ce rang a été supprimé avec succès.	1
92	Ce rang a été modifié avec succès.	1
95	Ce rang a été ajouté avec succès.	1
94	Erreur : Vous ne pouvez pas supprimer un rang appartenant à au moins un utilisateurs.	0
\.


--
-- Data for Name: panier; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.panier (idpanier, refuser, refarticle) FROM stdin;
\.


--
-- Data for Name: panier2; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.panier2 (idrefuser, idrefarticle, qte) FROM stdin;
1	2	1
\.


--
-- Data for Name: plays; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.plays (idvideo, titrevideo, nomfichiervideo, nomlienvideo, reftypevideo, descriptionvideo, miniature, refuser, nbviews, homepage) FROM stdin;
32	S1mple AWP noscope from heaven on Cache	142d869a8c3ae4ecc4d117345772258e9a44c8aa.mp4	\N	3	Insanely skilled.\nHe killed them.\nWin the round.\nBut not the final.	dc329215f00235a43c44633850472533818f5436.jpg	1	0	1
33	Snax 1v4 on Cbble	c9b8d980bde6675e09dbd49524c8cb23e289d5b5.mp4	\N	3	iouyh	2daa663d11faa4d552fcde05c57afac4c43b3d1f.jpg	1	0	1
35	Most Expensive fake Flash from S1mple on Dust2!!	ef187f1e17a3a5fd728cffe8af30923bbe6a6adf.mp4	\N	3	That cost around $10k today	4fb77348b59e5233265511267add6cd99035474c.jpg	1	0	1
34	coldzera anhilated Team Liquid in B Apps on Mirage	dc01c109ed7110c9570980e29b6c5e7d1b2920e0.mp4	\N	3	AND A JUMPING DOUBLE FOR COLD!!	21596aaaeb276af0f55bbae2cb02b630d4ccdced.jpg	1	0	1
31	ENVYUS HappY insane ace on Inferno B !	827e9829f867efc26c58f13c8eb46301fa005d5c.mp4	\N	3	Happened a while back!	2c367388cd5f4247572cb6999db69d082f66be01.jpg	1	0	1
\.


--
-- Data for Name: rangs; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.rangs (idrang, nomrang) FROM stdin;
2	Joueur
3	Modérateur
4	Administrateur
1	Utilisateur
\.


--
-- Data for Name: typearticle; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.typearticle (idtype, nomtype) FROM stdin;
1	Tartes
46	Équipements
47	CS:GO
51	T-Shirt
2	Casquettes
\.


--
-- Data for Name: typevideo; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.typevideo (idtypevideo, nomtypevideo) FROM stdin;
3	CS:GO
4	League of Legends
5	Rainbow Six Siege
8	Autres
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.users (iduser, pseudo, pass, mail, salt, refrang, session, verifaccount, profilepicture, about, dateinscri, allowcommentaire, resetlink, resetdate) FROM stdin;
7	admin2	d7706354167bb01cf46d5cc16170fe05b9f54b9bda010ce71626c9c46194c49b	example@example.com	\N	4	1d47933d74307410dd2ce9e8fb97dab1cd5527f9	\N		League of legends / Assassin's creed unity	\N	0	\N	\N
1	admin	65c3f75641b22925c737ca657b126cd68c39e423349d43031cf9a3b9a18cee1f	example@example.com	\N	4	39007ecc2614b9f0d420424fada97ab61aee76d6	\N	ef3cbd0040c0f7ccea732c677a344901bc2682a1.png	Desc	2020-05-26	1		0
18	user	d7706354167bb01cf46d5cc16170fe05b9f54b9bda010ce71626c9c46194c49b	example@example.com	\N	1	9428a17a5310fbc93fa30ac4b8f68cbbbc1537f4	\N	\N	je suis user	2021-06-03	1	\N	0
3	user1	699ed840dab219ba10c6e176f984a18a551f662ab571704cf8a8f04eef0c0363	example@example.com	\N	3	9e3337ca3a2a2805f3d9a26506c502c474c7f089	\N		Je joue à league of legends des fois	\N	0	\N	\N
\.


--
-- Data for Name: viewplay; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.viewplay (idview, refplay, refuser, ip, dateview) FROM stdin;
33	31	\N	10.7.253.60	2021-06-13
34	34	\N	10.7.253.60	2021-06-13
35	33	\N	10.7.253.60	2021-06-13
36	35	\N	10.7.253.60	2021-06-13
37	32	\N	10.7.253.60	2021-06-13
38	31	\N	10.7.253.157	2021-06-15
39	33	\N	10.7.253.157	2021-06-15
40	32	\N	10.7.253.157	2021-06-15
41	35	\N	10.7.253.157	2021-06-15
42	34	\N	10.7.253.157	2021-06-15
43	34	\N	10.7.253.152	2021-06-15
44	32	\N	10.7.253.152	2021-06-15
45	33	\N	10.7.253.152	2021-06-15
46	35	\N	10.7.253.152	2021-06-15
47	31	\N	10.7.253.152	2021-06-15
\.


--
-- Data for Name: wishlist; Type: TABLE DATA; Schema: public; Owner: rt1projet10
--

COPY public.wishlist (idrefuser, idrefarticle) FROM stdin;
1	2
1	27
\.


--
-- Name: articles_idarticle_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.articles_idarticle_seq', 33, true);


--
-- Name: commander_idcommander_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.commander_idcommander_seq', 57, true);


--
-- Name: commandes_idcommande_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.commandes_idcommande_seq', 46, true);


--
-- Name: commentaires_idcommentaire_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.commentaires_idcommentaire_seq', 30, true);


--
-- Name: commentairesplay_idcommentaireplay_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.commentairesplay_idcommentaireplay_seq', 23, true);


--
-- Name: etats_idetat_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.etats_idetat_seq', 5, true);


--
-- Name: imagesarticle_idimagearticle_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.imagesarticle_idimagearticle_seq', 16, true);


--
-- Name: messages_idmessage_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.messages_idmessage_seq', 95, true);


--
-- Name: panier_idpanier_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.panier_idpanier_seq', 19, true);


--
-- Name: rangs_idRang_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public."rangs_idRang_seq"', 8, true);


--
-- Name: typearticle_idtype_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.typearticle_idtype_seq', 51, true);


--
-- Name: typevideo_idTypeVideo_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public."typevideo_idTypeVideo_seq"', 8, true);


--
-- Name: users_iduser_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.users_iduser_seq', 18, true);


--
-- Name: videos_idvideo_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.videos_idvideo_seq', 35, true);


--
-- Name: viewplay_idview_seq; Type: SEQUENCE SET; Schema: public; Owner: rt1projet10
--

SELECT pg_catalog.setval('public.viewplay_idview_seq', 47, true);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (idarticle);


--
-- Name: avisarticle avisarticle_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.avisarticle
    ADD CONSTRAINT avisarticle_pkey PRIMARY KEY (idrefarticle, idrefuser);


--
-- Name: commander commander_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commander
    ADD CONSTRAINT commander_pkey PRIMARY KEY (idcommander);


--
-- Name: commandes commandes_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commandes
    ADD CONSTRAINT commandes_pkey PRIMARY KEY (idcommande);


--
-- Name: commentaires commentaires_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentaires
    ADD CONSTRAINT commentaires_pkey PRIMARY KEY (idcommentaire);


--
-- Name: commentairesplay commentairesplay_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentairesplay
    ADD CONSTRAINT commentairesplay_pkey PRIMARY KEY (idcommentaireplay);


--
-- Name: etats etats_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.etats
    ADD CONSTRAINT etats_pkey PRIMARY KEY (idetat);


--
-- Name: imagesarticle imagesarticle_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.imagesarticle
    ADD CONSTRAINT imagesarticle_pkey PRIMARY KEY (idimagearticle);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (idmessage);


--
-- Name: panier2 panier2_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier2
    ADD CONSTRAINT panier2_pkey PRIMARY KEY (idrefuser, idrefarticle);


--
-- Name: panier panier_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier
    ADD CONSTRAINT panier_pkey PRIMARY KEY (idpanier);


--
-- Name: rangs rangs_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.rangs
    ADD CONSTRAINT rangs_pkey PRIMARY KEY (idrang);


--
-- Name: typearticle typearticle_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.typearticle
    ADD CONSTRAINT typearticle_pkey PRIMARY KEY (idtype);


--
-- Name: typevideo typevideo_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.typevideo
    ADD CONSTRAINT typevideo_pkey PRIMARY KEY (idtypevideo);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (iduser);


--
-- Name: plays videos_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.plays
    ADD CONSTRAINT videos_pkey PRIMARY KEY (idvideo);


--
-- Name: viewplay viewplay_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.viewplay
    ADD CONSTRAINT viewplay_pkey PRIMARY KEY (idview);


--
-- Name: wishlist wishlist_pkey; Type: CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.wishlist
    ADD CONSTRAINT wishlist_pkey PRIMARY KEY (idrefuser, idrefarticle);


--
-- Name: articles articles_reftypearticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_reftypearticle_fkey FOREIGN KEY (reftypearticle) REFERENCES public.typearticle(idtype) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: avisarticle avisarticle_idrefarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.avisarticle
    ADD CONSTRAINT avisarticle_idrefarticle_fkey FOREIGN KEY (idrefarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: avisarticle avisarticle_idrefuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.avisarticle
    ADD CONSTRAINT avisarticle_idrefuser_fkey FOREIGN KEY (idrefuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commander commander_refarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commander
    ADD CONSTRAINT commander_refarticle_fkey FOREIGN KEY (refarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commander commander_refcommande_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commander
    ADD CONSTRAINT commander_refcommande_fkey FOREIGN KEY (refcommande) REFERENCES public.commandes(idcommande) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commandes commandes_refetat_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commandes
    ADD CONSTRAINT commandes_refetat_fkey FOREIGN KEY (refetat) REFERENCES public.etats(idetat) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: commandes commandes_refuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commandes
    ADD CONSTRAINT commandes_refuser_fkey FOREIGN KEY (refuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commentaires commentaires_refuserreceived_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentaires
    ADD CONSTRAINT commentaires_refuserreceived_fkey FOREIGN KEY (refuserreceived) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commentaires commentaires_refusersent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentaires
    ADD CONSTRAINT commentaires_refusersent_fkey FOREIGN KEY (refusersent) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commentairesplay commentairesplay_refplay_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentairesplay
    ADD CONSTRAINT commentairesplay_refplay_fkey FOREIGN KEY (refplay) REFERENCES public.plays(idvideo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: commentairesplay commentairesplay_refusersent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.commentairesplay
    ADD CONSTRAINT commentairesplay_refusersent_fkey FOREIGN KEY (refusersent) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: imagesarticle imagesarticle_refarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.imagesarticle
    ADD CONSTRAINT imagesarticle_refarticle_fkey FOREIGN KEY (refarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: panier2 panier2_idrefarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier2
    ADD CONSTRAINT panier2_idrefarticle_fkey FOREIGN KEY (idrefarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: panier2 panier2_idrefuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier2
    ADD CONSTRAINT panier2_idrefuser_fkey FOREIGN KEY (idrefuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: panier panier_refarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier
    ADD CONSTRAINT panier_refarticle_fkey FOREIGN KEY (refarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: panier panier_refuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.panier
    ADD CONSTRAINT panier_refuser_fkey FOREIGN KEY (refuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_refrang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_refrang_fkey FOREIGN KEY (refrang) REFERENCES public.rangs(idrang) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: plays videos_reftypevideo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.plays
    ADD CONSTRAINT videos_reftypevideo_fkey FOREIGN KEY (reftypevideo) REFERENCES public.typevideo(idtypevideo) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: viewplay viewplay_refplay_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.viewplay
    ADD CONSTRAINT viewplay_refplay_fkey FOREIGN KEY (refplay) REFERENCES public.plays(idvideo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: viewplay viewplay_refuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.viewplay
    ADD CONSTRAINT viewplay_refuser_fkey FOREIGN KEY (refuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wishlist wishlist_idrefarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.wishlist
    ADD CONSTRAINT wishlist_idrefarticle_fkey FOREIGN KEY (idrefarticle) REFERENCES public.articles(idarticle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wishlist wishlist_idrefuser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rt1projet10
--

ALTER TABLE ONLY public.wishlist
    ADD CONSTRAINT wishlist_idrefuser_fkey FOREIGN KEY (idrefuser) REFERENCES public.users(iduser) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

