<?php
$isBoutique = true;
$pageTitle = "Boutique";
include('include/init.php');
include('include/header.php');

/* ON FORME LA REQUETTE POUR CHERCHER LES ARTICLES
(Probablement a security hell??) */
$sql = 'SELECT * FROM articles WHERE 1=1';
if(isset($_GET['search'])) {
    if(!empty($_GET['recherche'])) {
        $sql = $sql." AND (nomarticle LIKE '%".secure($_GET['recherche'])."%' 
        OR desarticle LIKE '%".secure($_GET['recherche'])."%'
        OR keywords LIKE '%".secure($_GET['recherche'])."%') ";
    }
    if(!empty($_GET['cat']) AND is_numeric($_GET['cat'])) {
        $sql = $sql." AND reftypearticle='".$_GET['cat']."'";
    }
    if(!empty($_GET['prix']) AND is_numeric($_GET['prix'])) {
        $sql = $sql." AND prixvente<='".$_GET['prix']."'";
    }
}

$articlesParPage = 15;
/* ON COMPTE LE NOMBRE TOTAL D'ARTICLES */
$nbArticleTotal = $connexion->prepare($sql);
$nbArticleTotal->execute();
$count = count($nbArticleTotal->fetchAll());
$nbrPageVid = ceil($count / $articlesParPage);

/* ON SELECTIONNE LA PAGE, SI Y'A LE GET OU PAS */
if(!empty($_GET['p']) and is_numeric($_GET['p'])) {
    $page = secure($_GET['p']);
    if($page > $nbrPageVid) {
        $page = $nbrPageVid;
    }
} else {
    $page = 1;
}
$firstVids = ($page-1)*$articlesParPage;

/* ON FINI LA REQUETTE SQL */
$sql = $sql.' ORDER BY idarticle DESC LIMIT '.$articlesParPage.' OFFSET '.$firstVids;
$result = $connexion->prepare($sql);
$result->execute();
$result2 = $result->fetchAll();
?>

    <div class="content">
        <div class="page shop">
            <h1>NOTRE BOUTIQUE</h1>
            <h2>Faites vous plaisir et commandez vos vetements aux couleurs de OutBreaker eSport! </h2>
            <div class="shopContent">
            <div class="sidebar">
                <form method="GET" style="width: 200px;">
                    <input type="text" placeholder="Rechercher.." value="<?php if(isset($_GET['recherche'])) { echo $_GET['recherche']; } ?>" name="recherche">
                    <h3>CATEGORIES</h3>

                    <input type="radio" checked id="cat0" name="cat" value="0">
                    <label for="cat0">Toute les catégories</label><br><br>
                   
                    <div style="text-align: left;">
                        <?php 
                        $resultCategorie = $connexion->prepare('SELECT * from typearticle');
                        $resultCategorie->execute();
                        $resultCategorie = $resultCategorie->fetchAll();

                        foreach($resultCategorie as $categorie) {
                        ?>  
                            <input type="radio" <?php if(isset($_GET['cat']) AND $_GET['cat'] == $categorie['idtype']) { echo "checked"; } ?> id="cat<?php echo $categorie['idtype']; ?>" name="cat" value="<?php echo $categorie['idtype']; ?>">
                            <label for="cat<?php echo $categorie['idtype']; ?>"><?php echo $categorie['nomtype']; ?></label><br>
                        <?php } ?>
                    </div>
                    <h3>Prix</h3>
                        <input type="radio" checked id="allPrix" name="prix" value="0">
                        <label for="allPrix">Tout les prix</label><br><br>
                    <div style="text-align: left;">
                        <input type="radio" <?php if(isset($_GET['prix']) AND $_GET['prix'] == 30) { echo "checked"; } ?> id="prix1" name="prix" value="30">
                        <label for="prix1">Moins de 30 €</label><br>
                        <input type="radio" <?php if(isset($_GET['prix']) AND $_GET['prix'] == 60) { echo "checked"; } ?> id="prix2" name="prix" value="60">
                        <label for="prix2">Moins de 60 €</label><br>  
                        <input type="radio" <?php if(isset($_GET['prix']) AND $_GET['prix'] == 100) { echo "checked"; } ?> id="prix3" name="prix" value="100">
                        <label for="prix3">Moins de 100 €</label><br>
                    </div>
                    <button type="submit" class="btn" placeholder="Rechercher.." value="sent" name="search">Rechercher!</button>
                </form>
            </div>
            <div class="articles">
                <div class="listeArticles">
                    <?php 
                    foreach($result2 as $article) {
                    ?>
                    <div class="article">
                        <img src="upload/articles/<?php echo $article['idarticle'].'/'.$article['imageprincipale'];?>"></img>
                        <h3><?php echo $article['nomarticle']; ?></h3>
                        <p class="descArti"><?php echo truncage(0, $article['desarticle'], 99); ?> [...]</p>
                        <p><b><?php echo $article['prixvente']; ?> €</b>
                            <?php if($article['qtedispo'] > 0) { ?>
                            <label class="stock yes">En stock</label>
                            <?php } else { ?>
                            <label class="stock no">Rupture</label>
                            <?php } ?>
                        </p>
                        <div class="lienArticle" style="float: right; margin-right: 10px;">
                            <a href="article.php?id=<?php echo $article['idarticle']; ?>" class="btn">Voir Plus</a>
                        </div>
                    </div>
                    <?php } ?>

                </div>
                    <!-- LA PAGINATION DES ARTICLES -->
                    <div class="pagination">
                        <?php if($nbrPageVid > 0) { ?>
                        <span class="pageB infos">PAGES</span>
                        <?php  } ?>
                        <?php for($i = 1; $i <= $nbrPageVid; $i++) {
                                if($i == $page) {
                                    echo '<span class="pageB active">'.$i.'</span>';
                                } else {
                                    echo '<a href="?'.findCurrentURIArgs($_SERVER['REQUEST_URI']).'&p='.$i.'" class="pageB">'.$i.'</a>';
                                }
                            } ?>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

<?php
include('include/footer.php');
?>