<?php
$pageTitle = "Page de profile";
include('include/init.php');
if(!isset($_GET['id']) OR empty($_GET['id']) OR !is_numeric($_GET['id'])) {
    header('Location: index.php');
    exit();
}

/* SI ON DELETE QUELQUE UN COMMENTAIRES */
if(isset($_GET['del']) AND !empty($_GET['del']) AND is_numeric($_GET['del'])) {
    $findIfUser = $connexion->prepare('SELECT COUNT(*) FROM commentaires WHERE (refuserreceived=:iduser OR refusersent=:iduser) AND idcommentaire=:idcomm');
    $findIfUser->execute(array(
        'iduser' => $_SESSION['id'],
        'idcomm' => $_GET['del']
    ));
    $count = $findIfUser->fetch();
    /* ON CHECK SI L'UTILISATEUR A CRÉE LE COMMENTAIRE, OU SI L'UTILISATEUR EST SUR SON PROFILE, LES ADMINS PEUVENT TOUT DELETE */
    if($count[0] > 0 OR isAdmin()) {
        $deleteComment = $connexion->prepare('DELETE FROM commentaires WHERE idcommentaire=:idcomment');
        $deleteComment->execute(array(
            'idcomment' => $_GET['del']
        ));
        header('Location: profiles.php?id='.$_GET['id'].'&succ=80');
        exit();
    } else {
        header('Location: profiles.php?id='.$_GET['id'].'&err=78');
        exit();
    }
}

include('include/header.php');

/* ON VERIFIE L'EXISTANCE DE L'UTILISATEUR */
$findUser = $connexion->prepare('SELECT count(*) FROM users WHERE iduser=:id');
$findUser->execute(array(
    'id' => $_GET['id']
));
$findUser = $findUser->fetch();
if($findUser[0] > 0) {

/* ON SELECTIONNE LES INFOS DE L'UTILISATEUR QU'ON AFFICHE */
$reqSelectUser = $connexion->prepare('SELECT * FROM users INNER JOIN rangs ON refrang=idrang WHERE iduser=:id');
$reqSelectUser->execute(array(
    'id' => $_GET['id']
));
$user = $reqSelectUser->fetch();    ?>
    <div class="content">
        <div class="page profil" style="margin-top: 20px;">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1 class="profile"></h1>
            <div class="block">
                <div class="flexBlock" style="padding-top: 50px;">
                <div class="flexContent" style="flex:1; ">
                    <img src="upload/profiles/<?php echo findProfilePic($user['iduser']); ?>" class="image-profile">
                    </div>
                <div class="flexContent" style="flex:3; text-align: left;margin:auto">    
                    <h1 class="profile"><?php echo $user['pseudo']; ?></h1>
                    <h2 class="profile" style="font-style: italic;"><?php echo $user['nomrang']; ?></h2>
                    <p style="font-style: italic; text-align: left;">Inscrit le <?php echo dateInscri($user['iduser']); ?></p>
                </div>
                </div>
            </div>
            <h1 class="profile">Informations</h1><br><br>
            <div class="tabs">
                <button class="tabBtn active" id="Commentaires" onclick="openTab('CommentairesBlock', 'Commentaires')">Commentaires</button>
                <button class="tabBtn" id="Plays" onclick="openTab('PlaysBlock', 'Plays')">Plays</button>
                <button class="tabBtn" id="Avis" onclick="openTab('AvisBlock', 'Avis')">Avis</button>
                <button class="tabBtn" id="APropos" onclick="openTab('AProposBlock', 'APropos')">À Propos</button>
            </div>
            <div id="CommentairesBlock" class="tabContent">
                <?php if(connect() AND ($user['allowcommentaire'] == 1 OR isAdmin())) { ?>
                <div class="sendComment">
                    <form action="include/forms/addComment.php" method="post">
                        <textarea style="margin: 0; width: 96%;" name="contentComment" placeholder="Vous pouvez laisser un commentaire sur le profil de cet utilisateur"></textarea>
                        <input type="hidden" name="idreceived" value="<?php echo $_GET['id']; ?>">
                        <button type='submit' style="float:right;" name="sendComment" class="btn">Envoyer mon commentaire!</button>
                    </form>
                </div>
                <?php }
                /* ON RECUPERE LES COMMENTAIRES QUE L'UTILISATEUR A RECU*/
                $reqSelectComment = $connexion->prepare('SELECT * FROM commentaires INNER JOIN users ON refusersent=iduser WHERE refuserreceived=:id ORDER BY idcommentaire DESC');
                $reqSelectComment->execute(array(
                    'id' => $user['iduser']
                ));
                $comments = $reqSelectComment->fetchAll();
                
                $count = count($comments);
                if($count > 0) {
                foreach($comments as $comment) {

                ?>
                <div class="avisArticles">
                    <div class="avisArticle" style="margin-bottom: 25px;">
                            <div style="flex-flow:column;">

                            <div class="flexBlock oneChild">
                                <div class="flexContent" style="flex: 1; padding: 10px;max-width: 100px;">
                                    <div class="profileBlock" style="max-height: 100px;">
                                        <img style="border-radius: 10px" src="upload/profiles/<?php echo findProfilePic($comment['iduser']); ?>"></img>
                                    </div>
                                </div>
                                <div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                                    <a style="margin: 0;" href="profiles.php?id=<?php echo $comment['iduser']; ?>">
                                        <h2 style="margin:0;"><?php echo $comment['pseudo']; ?>
                                        <?php if((connect() AND $_SESSION['id'] == $user['iduser']) OR isAdmin() OR (connect() AND $_SESSION['id'] == $comment['iduser'])) { ?>
                                            <a href="<?php echo '?'.findCurrentURIArgs($_SERVER['REQUEST_URI']).'&del='.$comment['idcommentaire']; ?>" style="float:right"><i class="fas fa-trash-alt"></i></a>
                                        <?php } ?></h2>
                                    </a>
                                    <p style="margin:0; padding-bottom: 20px;"><?php echo date("d M Y à h:i:s", $comment['timecommentaire']); ?></p>
                                    <?php echo formatTexte($comment['textecommentaire']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } } else { echo '<div class="infoMessage" style="margin-bottom: 150px;">L\'utilisateur n\'a pas encore reçus de commentaire sur son profil.</div>'; } ?>
            </div>

            <div id="PlaysBlock" class="tabContent plays" style="display:none">
                <div class="flexPlay">
                    <?php 
                    $searchAllPlays = $connexion->prepare('SELECT * FROM plays INNER JOIN users ON refuser=iduser WHERE refuser=:id');
                    $searchAllPlays->execute(array('id' => $user['iduser']));
                    
                    foreach($searchAllPlays as $play) {
                    ?>
                    <div class="previewPlay">
                        <a href="watch.php?uuid=<?php echo $play['idvideo']; ?>">
                            <img src="upload/thumbs/<?php echo $play['refuser'].'/'.$play['miniature']; ?>"></img>
                        </a>
                        <div class="userBlock">
                            <a href="profiles.php?id=<?php echo $play['iduser']; ?>">
                                <img src="upload/profiles/<?php echo findProfilePic($play['iduser']); ?>"></img>
                            </a>
                            <p>
                                <a href="watch.php?uuid=<?php echo $play['idvideo']; ?>"><b><?php echo $play['titrevideo']; ?></b></a>
                                <br><a href="profiles.php?id=<?php echo $play['iduser']; ?>"><?php echo $play['pseudo']; ?></a> - <?php echo findNumViews($play['idvideo']); ?> vues
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div id="AvisBlock" class="tabContent" style="display:none">
                <?php 
                    /* ON RECUPERE LES AVIS DE L'UTILISATEUR */
                    $reqSelectAvis = $connexion->prepare('SELECT * FROM avisarticle INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                    $reqSelectAvis->execute(array(
                        'id' => $user['iduser']
                    ));
                    $avis = $reqSelectAvis->fetchAll();
                    
                    $count = count($avis);
                    if($count > 0) {
                    foreach($avis as $avis) {
                    ?>
                    <div class="avisArticles">
                        <div class="avisArticle" style="margin: 60px 0;">
                            <div style="flex-flow:column; margin-bottom: 30px">
                                <div class="flexBlock oneChild">
                                    <div class="flexContent" style="max-width: 100px;flex: 1; padding: 10px;" style="">
                                        <div class="profileBlock" style="    text-align: left;">
                                            <img style="border-radius: 10px" src="upload/profiles/<?php echo findProfilePic($avis['idrefuser']); ?>"></img>
                                       </div>
                                    </div>
                                    <div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                                        <h2 style="margin:0;display: inline; text-decoration: underline;"><?php echo $avis['nomarticle']; ?></h2> : 
                                        <a style="color: var(--violet)" style="margin: 0;" href="article.php?id=<?php echo $avis['idarticle']; ?>"> (Voir la page du Magasin)</a>
                                        <p style="margin:0;">Note : <b style="font-weight: bold"><?php echo $avis['noteavis']; ?> / 5</b> étoiles</p>
                                        <?php echo formatTexte($avis['textavis']); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?php } } else { echo '<div class="infoMessage" style="margin-bottom: 150px;">Cet utilisateur n\'a pas posté d\'avis sur un produit</div>'; } ?>
            </div>

            <div id="AProposBlock" class="tabContent" style="display:none">
                <h2 style="text-align: justify">
                    <?php   if(empty($user['about'])) { 
                                echo $user['pseudo']." n'a pas encore de description.";
                            } else { 
                                echo formatTexte($user['about']); 
                            } ?>
                </h2>
            </div>
                    
        </div>
    </div>

<?php } else {
    header('Location: index.php');
    exit();
}
include('include/footer.php');
?>