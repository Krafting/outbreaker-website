<?php
include('../init.php');
define('TARGETTHUMB', '../../upload/thumbs/'.$_SESSION['id'].'/');    
define('MAX_SIZE', 8000000);
$tabExt = array('jpg','png','gif','jpeg'); 

/* SI ON EST PAS CONNECTÉ ET SI NOTRE RANG N'EST PAS SUFFISANT */
if(!connect() OR $_SESSION['rang'] < 1) {
    header('Location: ../../index.php');
    exit();
}

if(connect()) {
    /* SI LE POST A ÉTÉ ENVOYÉ ET QUE TOUT LES CHAMPS REQUIS SONT REMPLIS */
    if(isset($_POST['sendPlay'])) {
        if(isset($_POST['catPlay']) && !empty($_POST['catPlay']) 
        && isset($_POST['descPlay']) && isset($_POST['titrePlay']) && !empty($_POST['titrePlay'])) {

            /* SI LE DOSSIER D'UTILISATEUR N'EXISTE PAS, ON LE CRÉE */
            if (!file_exists('../../upload/thumbs/'.$_SESSION['id'].'/')) {
                mkdir('../../upload/thumbs/'.$_SESSION['id'].'/', 0755, true);
            }
            /* ON CHECK SI LE PLAY APPARTIENT A L'USER */
            $selectPlayUser = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE refuser=:iduser AND idvideo=:idplay');
            $selectPlayUser->execute(array(
                'iduser' => $_SESSION['id'],
                'idplay' => $_POST['idvideo']
            ));
            $count = $selectPlayUser->fetch();
            if($count[0] > 0) {

                $updateVideoBDD = $connexion->prepare('UPDATE plays SET titrevideo=:titrevideo, reftypevideo=:reftypevideo, descriptionvideo=:descriptionvideo WHERE idvideo=:idvideo');
                $updateVideoBDD->execute(array(
                    'titrevideo' => $_POST['titrePlay'],
                    'reftypevideo' => $_POST['catPlay'],
                    'descriptionvideo' => $_POST['descPlay'],
                    'idvideo' => $_POST['idvideo']
                ));
                header('Location: ../../plays.php?action=gerer&succ=58&edit='.$_POST['idvideo']);
                exit();

                /* SI L'UTILISATEUR A ENVOYÉ UNE NOUVELLE MINIATURE */
                if(!empty($_FILES['playThumb']['name'])) {
                    /* EXTENTION DES FICHERS UPLOADER */
                    $extension  = pathinfo($_FILES['playThumb']['name'], PATHINFO_EXTENSION);
                    /* SI L'EXTENTION EST AUTORISÉE */
                    if(in_array(strtolower($extension),$tabExt)) {
                        /* ON CHECK LA TAILLE DE L'IMAGE */
                        if (filesize($_FILES["playThumb"]["tmp_name"]) < $maxImageSize) {
                            $nomImage = sha1(uniqid()) .'.'. $extension;
                            /* SI L'UPLOAD A ÉTÉ FAIS AVEC SUCCÈS */
                            if(move_uploaded_file($_FILES['playThumb']['tmp_name'], TARGETTHUMB.$nomImage)) {
                                $updateVideoBDD = $connexion->prepare('UPDATE plays SET miniature=:miniature WHERE refuser=:iduser AND idvideo=:idplay');
                                $updateVideoBDD->execute(array(
                                    'miniature' => $nomImage,
                                    'iduser' => $_SESSION['id'],
                                    'idplay' => $_POST['idvideo']
                                ));
                            } else {
                                header('Location: ../../plays.php?action=gerer&err=60&edit='.$_POST['idvideo']);
                                exit();
                            }
                        } else {
                            header('Location: ../../plays.php?action=gerer&err=83&edit='.$_POST['idvideo']);
                            exit();
                        }
                    } else {
                        header('Location: ../../plays.php?action=gerer&err=7&edit='.$_POST['idvideo']);
                        exit();
                    }
                } 
            } else {
                header('Location: ../../plays.php?action=gerer&err=63');
                exit();
            }
        } else {
            header('Location: ../../plays.php?action=gerer&err=1');
            exit();
        }
    }

    /* SI ON DELETE, ON CHECK SI LE POST N'EST PAS VIDE ET L'ID EST NUMERIC */
    if(isset($_POST['delPlay'])) {
        if(isset($_POST['idvideo']) && is_numeric($_POST['idvideo'])) {
            /* ON CHECK SI LE PLAY APPARTIENT A L'USER */
            $selectPlayUser = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE refuser=:iduser AND idvideo=:idplay');
            $selectPlayUser->execute(array(
                'iduser' => $_SESSION['id'],
                'idplay' => $_POST['idvideo']
            ));
            $count = $selectPlayUser->fetch();
            if($count[0] > 0) {

                $selectPlayUserDelete = $connexion->prepare('SELECT * FROM plays WHERE refuser=:iduser AND idvideo=:idplay');
                $selectPlayUserDelete->execute(array(
                    'iduser' => $_SESSION['id'],
                    'idplay' => $_POST['idvideo']
                ));
                $unlinkData = $selectPlayUserDelete->fetch();
                /* ON DELETE LES FICHIERS */
                unlink('../../upload/thumbs/'.$_SESSION['id'].'/'.$unlinkData['miniature']);
                unlink('../../upload/plays/'.$_SESSION['id'].'/'.$unlinkData['nomfichiervideo']);

                /* ON DELETE DE LA BDD LA VIDEO ET LES COMMENTAIRES */
                $delPlay = $connexion->prepare('DELETE FROM plays WHERE idvideo=:idplay');
                $delPlay->execute(array(
                    'idplay' => $_POST['idvideo']
                ));
                /* LA SUPPRESSION DES COMMENTAIRE SE FAIS PAR 'ON DELETE CASCADE' DANS LA BDD*/

                header('Location: ../../plays.php?action=gerer&succ=62');
                exit();
            } else {
                header('Location: ../../plays.php?action=gerer&err=63');
                exit();
            }
        } else {
            header('Location: ../../plays.php?action=gerer&err=1');
            exit();
        }
    }
    
}
?>