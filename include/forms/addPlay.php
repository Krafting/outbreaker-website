<?php
include('../init.php');
define('TARGET', '../../upload/plays/'.$_SESSION['id'].'/');    
define('TARGETTHUMB', '../../upload/thumbs/'.$_SESSION['id'].'/');    
define('MAX_SIZE', 8000000);
$tabExtVideo = array('mp4','avi','gif','mkv'); 
$tabExt = array('jpg','png','gif','jpeg');

/* SI ON EST PAS CONNECTÉ ET SI NOTRE RANG N'EST PAS SUFFISANT */
if(!connect() OR $_SESSION['rang'] < 2) {
    header('Location: ../../index.php');
    exit();
}

if(connect()) {
    /* SI LE POST A ÉTÉ ENVOYÉ ET QUE TOUT LES CHAMPS REQUIS SONT REMPLIS */
    if(isset($_POST['sendPlay']) && isset($_POST['catPlay']) && !empty($_POST['catPlay']) 
    && isset($_POST['descPlay']) && isset($_POST['titrePlay']) && !empty($_POST['titrePlay'])) {
        /* SI L'UTILISATEUR A ENVOYÉ UNE VIDÉO ET UNE MINIATURE */
        if(!empty($_FILES['play']['name']) && !empty($_FILES['playThumb']['name'])) {

            /* ON CHECK LA TAILLE DE LA VIDÉO ET DE L'IMAGE */
			if (filesize($_FILES["play"]["tmp_name"]) < $maxVideoSize) {
                if (filesize($_FILES["playThumb"]["tmp_name"]) < $maxImageSize) {
                    /* SI LE DOSSIER D'UTILISATEUR N'EXISTE PAS, ON LE CRÉE */
                    if (!file_exists('../../upload/plays/'.$_SESSION['id'].'/')) {
                        mkdir('../../upload/plays/'.$_SESSION['id'].'/', 0775, true);
                    }
                    if (!file_exists('../../upload/thumbs/'.$_SESSION['id'].'/')) {
                        mkdir('../../upload/thumbs/'.$_SESSION['id'].'/', 0775, true);
                    }

                    /* EXTENTION DES FICHERS UPLOADER */
                    $extensionVideo  = pathinfo($_FILES['play']['name'], PATHINFO_EXTENSION);
                    $extension  = pathinfo($_FILES['playThumb']['name'], PATHINFO_EXTENSION);
                    /* SI L'EXTENTION EST AUTORISÉE */
                    if(in_array(strtolower($extensionVideo),$tabExtVideo) && in_array(strtolower($extension),$tabExt)) {
                        $nomVideo = sha1(uniqid()) .'.'. $extensionVideo;
                        $nomImage = sha1(uniqid()) .'.'. $extension;
                        /* SI L'UPLOAD A ÉTÉ FAIS AVEC SUCCÈS */
                        if(move_uploaded_file($_FILES['play']['tmp_name'], TARGET.$nomVideo) 
                        && move_uploaded_file($_FILES['playThumb']['tmp_name'], TARGETTHUMB.$nomImage)) {
                            $insertVideoBDD = $connexion->prepare('INSERT INTO plays (titrevideo, nomfichiervideo, reftypevideo, descriptionvideo, miniature, refuser) VALUES (:titrevideo, :nomfichiervideo, :reftypevideo, :descriptionvideo, :miniature, :iduser);');
                            $insertVideoBDD->execute(array(
                                'titrevideo' => $_POST['titrePlay'],
                                'nomfichiervideo' => $nomVideo,
                                'reftypevideo' => $_POST['catPlay'],
                                'descriptionvideo' => $_POST['descPlay'],
                                'miniature' => $nomImage,
                                'iduser' => $_SESSION['id']
                            ));
                            header('Location: ../../plays.php?action=gerer&succ=59');
                            exit();
                        } else {
                            header('Location: ../../plays.php?action=gerer&err=60');
                            exit();
                        }
                    } else {
                        header('Location: ../../plays.php?action=add&err=7');
                        exit();
                    }
                } else {
                    header('Location: ../../plays.php?action=add&err=83');
                    exit();
                }
            } else {
                header('Location: ../../plays.php?action=add&err=84');
                exit();
            }
        } else {
            header('Location: ../../plays.php?action=add&err=1');
            exit();
        }
    } else {
        header('Location: ../../plays.php?action=add&err=1');
        exit();
    }
}
?>