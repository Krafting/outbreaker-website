<?php
$adminPage = true; $titrePage = "Dashboard";
include('../include/init.php');
include('header.php');

/* STATS REQUEST */
// users
$countUsers = $connexion->prepare('SELECT count(*) FROM users');
$countUsers->execute();
$countUsers = $countUsers->fetch();
// users
$countUsersToday = $connexion->prepare('SELECT count(*) FROM users WHERE dateinscri=:dateinscri');
$countUsersToday->execute(array('dateinscri' => date('d-m-Y')));
$countUsersToday = $countUsersToday->fetch();
// articles commandée
$countArticlesCommandee = $connexion->prepare('SELECT count(*) FROM commander');
$countArticlesCommandee->execute();
$countArticlesCommandee = $countArticlesCommandee->fetch();
// commandes
$countCommandes = $connexion->prepare('SELECT count(*) FROM commandes');
$countCommandes->execute();
$countCommandes = $countCommandes->fetch();
// plays 
$countPlays = $connexion->prepare('SELECT count(*) FROM plays');
$countPlays->execute();
$countPlays = $countPlays->fetch();
// plays 
$countPlaysView = $connexion->prepare('SELECT count(*) FROM viewplay');
$countPlaysView->execute();
$countPlaysView = $countPlaysView->fetch();
// articles
$countArticles = $connexion->prepare('SELECT count(*) FROM articles');
$countArticles->execute();
$countArticles = $countArticles->fetch();
// categories
$countCategorie = $connexion->prepare('SELECT count(*) FROM typearticle');
$countCategorie->execute();
$countCategorie = $countCategorie->fetch();

?>
<div class="index">
    <h1>Dashboard d'administration</h1>
    <hr style="margin-bottom: 30px;">
    <h3>Statistiques Générales</h3>
    <div class="contantStats">
        <a href="users.php" class="stats">
            <div class="blockStats blue">
                <i class="fas fa-users" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countUsers[0]; ?></p>
                    Utilisateurs inscits
                </div>
            </div>
        </a>
        <a href="users.php" class="stats">
            <div class="blockStats black">
                <i class="fas fa-users" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countUsersToday[0]; ?></p>
                    Inscriptions aujourd'hui
                </div>
            </div>
        </a>
    </div>
    <h3>Statistiques Boutiques</h3>

    <div class="contantStats">
        <a href="gestion_commande.php" class="stats">
            <div class="blockStats red">
                <i class="fas fa-cubes" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countArticlesCommandee[0]; ?></p>
                    Articles commandés
                </div>
            </div>
        </a>
        <a href="gestion_article.php" class="stats">
            <div class="blockStats pink">
                <i class="fas fa-cubes" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countArticles[0]; ?></p>
                    Articles
                </div>
            </div>
        </a>
        <a href="gestion_categorie.php" class="stats">
            <div class="blockStats orange">
                <i class="fas fa-cubes" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countCategorie[0]; ?></p>
                    Catégories
                </div>
            </div>
        </a>
        <a href="gestion_commande.php" class="stats">
            <div class="blockStats yellow">
                <i class="fas fa-box" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countCommandes[0]; ?></p>
                    Commandes
                </div>
            </div>
        </a>
    </div>

    <h3>Statistiques Plays</h3>
    <div class="contantStats">
        <a href="gestion_plays.php" class="stats">
            <div class="blockStats green">
                <i class="fas fa-play" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countPlays[0]; ?></p>
                    Plays mis en ligne
                </div>
            </div>
        </a>
        <a href="gestion_plays.php" class="stats">
            <div class="blockStats purple">
                <i class="far fa-eye" style="margin-left: 10px; font-size: 70px;float: left;"></i>
                <div class="textStats">
                    <p style="margin: 0; padding:0"><?php echo $countPlaysView[0]; ?></p>
                    Vues sur les Plays
                </div>
            </div>
        </a>
    </div>
</div>