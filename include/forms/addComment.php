<?php
include('../init.php');
/* ON CHECK QU'ON EST CONNECTÉ, QUE LE POSTE N'EST PAS VIDE ET ENVOYÉ */
if(connect()) {
    if(isset($_POST['sendComment']) AND isset($_POST['idreceived'])) {
        if(!empty($_POST['contentComment'])) {
            /* A FAIRE: verifier si l'utilisateur existe, si il accept les commentaire */
            $findUser = $connexion->prepare('SELECT * FROM users WHERE iduser=:id');
            $findUser->execute(array(
                'id' => $_POST['idreceived']
            ));
            $findUser = $findUser->fetch();
            if(isset($findUser['pseudo'])) {
                /* ON VERIFIE QUE L'UTILISATEUR ACCEPTE LES COMMENTAIRES, OU SI ON EST ADMIN */
                if($findUser['allowcommentaire'] == 1 OR isAdmin()) {
                    /* ON AJOUTE LE COMMENTAIRE */
                    $addComment = $connexion->prepare('INSERT INTO commentaires (textecommentaire, refusersent, refuserreceived, datecommentaire, timecommentaire) VALUES (:textecommentaire, :refusersent, :refuserreceived, :datecommentaire, :timecommentaire);');
                    $addComment->execute(array(
                        'textecommentaire' => $_POST['contentComment'], 
                        'refusersent' => $_SESSION['id'], 
                        'refuserreceived' => $_POST['idreceived'], 
                        'datecommentaire' => date('d-m-Y'),
                        'timecommentaire' => time()
                    ));
                    header('Location: ../../profiles.php?id='.$_POST['idreceived'].'&succ=49');
                    exit();
                } else {
                    /* L'USER N'ACCEPTE PAS LES COMMENTAIRES */
                    header('Location: ../../profiles.php?id='.$_POST['idreceived'].'&err=48');
                    exit();
                }
            } else {
                /* L'USER N'EXISTE PAS */
                header('Location: ../../profiles.php?err=47');
                exit();
            }
        } else {
            /* MESSAGE D'ERREUR SI LE POST  EST VIDE */
            header('Location: ../../profiles.php?id='.$_POST['idreceived'].'&err=1');
            exit();
        }
    } else {
        /* SI ON NE PASSE PAS PAR LE POST */
        header('Location: ../../index.php');
        exit();
    }
} else {
    /* SI ON EST PAS CONNECTÉ */
    header('Location: ../../profiles.php?id='.$_POST['idreceived'].'&err=50');
    exit();
}
?>