-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 11, 2021 at 02:36 AM
-- Server version: 10.2.40-MariaDB-log-cll-lve
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krafting_outbreaker`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `idarticle` int(11) NOT NULL,
  `nomarticle` longtext NOT NULL,
  `qtedispo` int(11) NOT NULL,
  `prixvente` float NOT NULL,
  `desarticle` longtext DEFAULT NULL,
  `reftypearticle` int(11) NOT NULL,
  `keywords` longtext DEFAULT NULL,
  `imageprincipale` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`idarticle`, `nomarticle`, `qtedispo`, `prixvente`, `desarticle`, `reftypearticle`, `keywords`, `imageprincipale`) VALUES
(2, 'Tarte', 0, 19.95, 'Cet article est un chapeau de type béret. Pour avoir un maximum de charisme.', 1, 'tarte ', 'a88d769ee215ad8d20081e036818b2da77427587.jpeg'),
(3, 'Casquette Orange Box', 11, 29.99, 'Une Casquette Valve avec comme caracteristique principale ces couleurs et motif ressamblant a l\'original Orange Box de valve', 2, 'snapback, casquette, orange, half life, valve', '345b31940cc81826b071c37a18c1cd5e7880ad42.png'),
(27, 'Talon Knife (CS:GO)', 250, 39.99, 'Réplique réel du couteau Talon dans le jeu Counter-Strike Global Offensive.\r\n\r\nPlus de détail\r\n\r\n    Blade finish: Gloss Coating\r\n    Blade material: 420 Stainless Steel\r\n    Handle material: ABS - Acrylonitrile Butadiene Styrene w/ Fiberglass\r\n    Weight: 3.83 grams\r\n\r\n', 47, 'csgo, knife, talon, replica', 'ec3498e24bbd71526816b96b212842d67c212f42.jpg'),
(33, 'T-Shirt Cisco Pro Team', 50, 49.95, 'Un superbe t-shirt style e-sport pour booster vos connaissances lors de vos passages de certifications cisco.\r\n\r\nDonne un bonus de connaissances de +10 sur Cisco IOS, ainsi qu\'un bonus d\'xp pour chaque VLAN configuré.', 51, 'cisco t-shirt pro', '78a7014c0b877aa60e46e7aa92323db4eb444dfb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `avisarticle`
--

CREATE TABLE `avisarticle` (
  `idrefarticle` int(11) NOT NULL,
  `idrefuser` int(11) NOT NULL,
  `textavis` longtext NOT NULL,
  `noteavis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `avisarticle`
--

INSERT INTO `avisarticle` (`idrefarticle`, `idrefuser`, `textavis`, `noteavis`) VALUES
(27, 1, 'Tranchant !', 4),
(27, 7, 'Le couteau est de bonne manufacture, il est equilibré, résistant et il est bien aiguisé. \r\n\r\nJe met donc une note de 5 étoiles.\r\n\r\nDe plus la livraison est rapide. ', 5),
(33, 1, 'Super T-Shirt, il m\'a bien aidé lors de ma CCNA1!', 5);

-- --------------------------------------------------------

--
-- Table structure for table `commander`
--

CREATE TABLE `commander` (
  `idcommander` int(11) NOT NULL,
  `refcommande` int(11) NOT NULL,
  `qtecommande` int(11) NOT NULL,
  `refarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commander`
--

INSERT INTO `commander` (`idcommander`, `refcommande`, `qtecommande`, `refarticle`) VALUES
(54, 44, 1, 27),
(55, 44, 3, 33),
(56, 45, 1, 3),
(57, 46, 1, 27);

-- --------------------------------------------------------

--
-- Table structure for table `commandes`
--

CREATE TABLE `commandes` (
  `idcommande` int(11) NOT NULL,
  `refetat` int(11) NOT NULL,
  `addresscommande` longtext NOT NULL,
  `extras` longtext DEFAULT NULL,
  `nomcommande` longtext NOT NULL,
  `prenomcommande` longtext NOT NULL,
  `telcommande` longtext DEFAULT NULL,
  `depcommande` longtext NOT NULL,
  `codepostal` longtext NOT NULL,
  `villecommande` longtext NOT NULL,
  `refuser` int(11) NOT NULL,
  `prixtotal` double NOT NULL DEFAULT 0,
  `adressescore` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commandes`
--

INSERT INTO `commandes` (`idcommande`, `refetat`, `addresscommande`, `extras`, `nomcommande`, `prenomcommande`, `telcommande`, `depcommande`, `codepostal`, `villecommande`, `refuser`, `prixtotal`, `adressescore`) VALUES
(44, 1, '1 Rue du parc', NULL, 'ALDERSON', 'Elliot', '', 'Allier', '03200', 'Vichy', 1, 189.84, 0.96315272727273),
(45, 1, '5224 Le Bourg', NULL, 'Waters', 'Berniece', '', 'Cantal', '15110', 'Saint-Urcize', 1, 29.99, 0.40494597402597),
(46, 1, '3 place de la mairie', NULL, '6sko', 'enable', '06-02-03-06-06', 'Côte-d\'or', '21220', 'Collonges-lès-Bévy', 7, 39.99, 0.93119272727273);

-- --------------------------------------------------------

--
-- Table structure for table `commentaires`
--

CREATE TABLE `commentaires` (
  `idcommentaire` int(11) NOT NULL,
  `textecommentaire` longtext NOT NULL,
  `refusersent` int(11) NOT NULL,
  `refuserreceived` int(11) NOT NULL,
  `datecommentaire` date NOT NULL,
  `timecommentaire` bigint(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commentaires`
--

INSERT INTO `commentaires` (`idcommentaire`, `textecommentaire`, `refusersent`, `refuserreceived`, `datecommentaire`, `timecommentaire`) VALUES
(30, 'NOTRE BOUTIQUE\r\nFaites vous plaisir et commandez vos vetements aux couleurs de OutBreaker eSport! ', 1, 1, '2021-06-07', 1623082648);

-- --------------------------------------------------------

--
-- Table structure for table `commentairesplay`
--

CREATE TABLE `commentairesplay` (
  `idcommentaireplay` int(11) NOT NULL,
  `textecommentaireplay` longtext NOT NULL,
  `refusersent` int(11) NOT NULL,
  `datecommentaireplay` date NOT NULL,
  `timecommentaireplay` bigint(20) DEFAULT 0,
  `refplay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commentairesplay`
--

INSERT INTO `commentairesplay` (`idcommentaireplay`, `textecommentaireplay`, `refusersent`, `datecommentaireplay`, `timecommentaireplay`, `refplay`) VALUES
(23, 'Trop bien!', 1, '2021-06-13', 1623610354, 35);

-- --------------------------------------------------------

--
-- Table structure for table `etats`
--

CREATE TABLE `etats` (
  `idetat` int(11) NOT NULL,
  `nometat` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `etats`
--

INSERT INTO `etats` (`idetat`, `nometat`) VALUES
(1, 'En attente'),
(2, 'Validée'),
(3, 'En Transit'),
(4, 'Livrée'),
(5, 'Annulée');

-- --------------------------------------------------------

--
-- Table structure for table `imagesarticle`
--

CREATE TABLE `imagesarticle` (
  `idimagearticle` int(11) NOT NULL,
  `nomfichier` longtext NOT NULL,
  `refarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagesarticle`
--

INSERT INTO `imagesarticle` (`idimagearticle`, `nomfichier`, `refarticle`) VALUES
(7, '67fb9a8b54e787b6f0e2736ab4ca06ace255f0a0.jpg', 27),
(8, 'e8c15470579f98a26b0f2addf2fd5b1b104c22df.jpg', 27),
(9, 'ee59d24db378ebe8bd17157a14a1d2b9b105b31c.jpg', 2),
(13, 'f66bc80248eee2ea47e5bb6fdb9699ef4a4e166b.png', 3),
(14, 'ad14914baffece1a947c8fbb87f2b540f8d4e0d0.png', 3),
(16, 'a0f5f7cdf183a5b2b3ebca88a33c75d37b162d5c.jpg', 33);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `idmessage` int(11) NOT NULL,
  `textmessage` longtext NOT NULL,
  `typemessage` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`idmessage`, `textmessage`, `typemessage`) VALUES
(1, 'Erreur : Un des champs requis est vide.', 0),
(2, 'Erreur : Vérifiez que votre ancien mot de passe est correct.', 0),
(3, 'Erreur : Vérifiez que les mots de passes correspondent.', 0),
(4, 'Erreur : L\'email entrée n\'est pas dans le bon format.', 0),
(5, 'Erreur : Vérifiez que les mots de passes correspondent.', 0),
(6, 'Erreur : Essaie pas de casser notre site (stp).', 0),
(7, 'Erreur : L\'image rentrée n\'est pas dans le bon format (formats acceptés: gif, jpg, png, jpeg).', 0),
(8, 'Erreur : L\'image est trop volumineuse.', 0),
(9, 'Erreur : Une erreur est survenue pendant le téléchargement de l\'image.', 0),
(10, 'Votre mot de passe a été modifié avec succès.', 1),
(11, 'Votre compte a été modifié avec succès.', 1),
(12, 'Votre profil a été modifié avec succès.', 1),
(13, 'Votre photo de profil a été modifié avec succès.', 1),
(14, 'Cette image a été supprimé avec succès de nos serveurs.', 1),
(15, 'Votre commande a bien été placé, vous pouvez y suivre l\'activité depuis votre espace membre (en bas de cette page).', 1),
(16, 'Votre avis a été ajouté avec succès.', 1),
(17, 'Votre avis a été modifié avec succès.', 1),
(18, 'Erreur : Une erreur est survenue.', 0),
(19, 'Erreur : Votre panier est vide, vous ne pouvez pas passer commande.', 0),
(20, 'Erreur : Vos coordonnées bancaires ne sont pas remplisent correctement.', 0),
(21, 'Votre commande a bien été placé, vous pouvez y suivre l\'activité depuis votre espace membre.', 1),
(22, 'Erreur : Cette adresse e-mail est déjà utilisée. </br> Il n\'est pas possible d\'utiliser deux fois la même adresse e-mail', 0),
(23, 'Erreur : Ce pseudo existe déjà.', 0),
(24, 'Le pseudonyme n\'est pas accepté. Le pseudonyme doit comprendre de 2 à 25 caractères.</br>Les caractères autorisés sont: <br><ul><li>Lettres de A à Z et chiffres de 0 à 9</li><li> Les traits d\'union et les sous-tirets</li>', 0),
(25, 'Erreur : Votre description ne respecte pas la taille limite (1250 caractères)', 0),
(26, 'Erreur : Vérifiez que le pseudo et le mot de passe correspondent.', 0),
(27, 'Vous êtes inscrit! Vous pouvez désormais vous connecter.', 1),
(28, 'Erreur : L\'article n\'existe pas.', 0),
(29, 'Erreur : Cet article n\'est pas dans votre panier.', 0),
(30, 'L\'article a bien été ajouté dans votre panier.', 1),
(31, 'L\'article a bien été retiré de votre panier.', 1),
(32, 'Erreur : L\'article est en rupture de stock.', 0),
(33, 'Erreur : Votre avis ne peux pas être vide!', 0),
(34, 'La catégorie à été modifié avec succès', 1),
(35, 'Erreur : Vous ne pouvez pas supprimer une catégorie si elle est lié à un article.', 0),
(36, 'La catégorie à été supprimé avec succès ', 1),
(37, 'Erreur : La catégorie n\'existe pas', 0),
(38, 'La catégorie as bien été créé', 1),
(39, 'Erreur : Vous n\'avez pas rentré de nom de catéogrie', 0),
(40, 'Le nom de l\'article a bien été modifié ', 1),
(41, 'L\'article a bien été supprimé', 1),
(42, 'Erreur : L\'article n\'existe pas.', 0),
(43, 'L\'article a bien été ajouté a votre wishlist.', 1),
(44, 'L\'article a été retiré avec succès de votre wishlist.', 1),
(45, 'Erreur : L\'article n\'est pas dans votre wishlist.', 0),
(46, 'L\'article a bien été créé', 1),
(47, 'Erreur : L\'utilisateur n\'existe pas', 0),
(48, 'Erreur : Cet utilisateur n\'accepte pas les commentaires sur son profil.', 0),
(49, 'Votre commentaire a été envoyé avec succès!', 1),
(50, 'Erreur : Vous devez être connecté afin de pouvoir commenté.', 0),
(53, 'Vous ne pouvez pas annuler cette commande.', 0),
(54, 'Votre commande a été annulée avec succès.', 1),
(55, 'L\'article as bien été modifié', 1),
(56, 'Erreur : L\'article n\'existe pas.', 0),
(57, 'Erreur : l\'article n\'as pas été modifié ', 0),
(58, 'Votre Play a été modifié avec succès.', 1),
(59, 'Votre Play a été mis en ligne avec succès.', 1),
(60, 'Erreur : Une erreur est survenue lors de l\'envoie d\'un fichier.', 0),
(61, 'Erreur : Ce Play n\'existe pas.', 0),
(62, 'Votre play a été supprimé avec succès.', 1),
(63, 'Erreur : Ce play n\'existe pas, ou ne vous appartient pas.', 0),
(64, 'Erreur : Vous ne pouvez pas supprimer un utilisateur qui n\'existe pas.', 0),
(65, 'Erreur : Vous ne pouvez pas supprimer un administrateur ou modérateur.', 0),
(66, 'L\'utilisateur a bien été supprimé.', 1),
(67, 'Ce compte a bien été modifié', 1),
(68, 'Erreur : Ce rang n\'existe pas.', 0),
(69, 'Erreur : Le pseudo ne peut pas être vide.', 0),
(70, 'Ce profil a été modifié avec succès', 1),
(71, 'Le mot de passe de cet utilisateur a été modifié avec succès.', 1),
(72, 'Erreur : Cette image n\'existe pas.', 0),
(73, 'La commande a été modifié avec succès', 1),
(74, 'Erreur : Le delai de ce lien a expiré, veuillez en générer un nouveau.', 0),
(75, 'Un lien de réinitialisation de mot de passe vous a été envoyé par mail. Ce lien expirera dans 2 heures.', 1),
(76, 'Erreur : Vous ne pouvez pas supprimer une catégorie si elle est lié à un Play. ', 0),
(77, 'Erreur : Un article possède déjà ce nom', 0),
(78, 'Erreur : Ce commentaire n\'existe pas.', 0),
(79, 'Votre commentaire a été supprimé avec succès.', 1),
(80, 'Ce commentaire a été supprimé avec succès.', 1),
(81, 'Vous avez supprimé la photo avec succès', 1),
(82, 'Erreur : Cette image n\'existe pas.', 0),
(83, 'Erreur : La miniature séléctionnée est trop volumineuse.', 0),
(84, 'Erreur : La vidéo séléctionnée est trop volumineuse.', 0),
(85, 'Erreur : Votre carte bancaire a mal été renseignée.', 0),
(86, 'Erreur : Une erreur est survenue lors de l\'envoie du mail. Veuillez réessayer plus tard.', 0),
(87, 'Un mail vous a été envoyé avec un lien pour réinitialiser votre mot de passe dans votre boite mail.', 1),
(88, 'Erreur : Aucun utilisateur n\'est enregistré avec cette adresse mail.', 0),
(89, 'Erreur : Votre adresse est mal formatté, veuillez réessayer', 0),
(90, 'Erreur : Votre adresse semble être inexistante, veuillez réessayer ou utiliser une autre adresse.', 0),
(91, 'Erreur : Ce rang n\'existe pas.', 0),
(92, 'Ce rang a été modifié avec succès.', 1),
(93, 'Ce rang a été supprimé avec succès.', 1),
(94, 'Erreur : Vous ne pouvez pas supprimer un rang appartenant à au moins un utilisateurs.', 0),
(95, 'Ce rang a été ajouté avec succès.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `panier`
--

CREATE TABLE `panier` (
  `idpanier` int(11) NOT NULL,
  `refuser` int(11) NOT NULL,
  `refarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `panier2`
--

CREATE TABLE `panier2` (
  `idrefuser` int(11) NOT NULL,
  `idrefarticle` int(11) NOT NULL,
  `qte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panier2`
--

INSERT INTO `panier2` (`idrefuser`, `idrefarticle`, `qte`) VALUES
(1, 2, 1),
(1, 27, 2),
(20, 27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plays`
--

CREATE TABLE `plays` (
  `idvideo` int(11) NOT NULL,
  `titrevideo` longtext NOT NULL,
  `nomfichiervideo` longtext NOT NULL,
  `nomlienvideo` longtext DEFAULT NULL,
  `reftypevideo` int(11) NOT NULL,
  `descriptionvideo` longtext DEFAULT NULL,
  `miniature` longtext DEFAULT NULL,
  `refuser` int(11) NOT NULL,
  `nbviews` int(11) DEFAULT 0,
  `homepage` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plays`
--

INSERT INTO `plays` (`idvideo`, `titrevideo`, `nomfichiervideo`, `nomlienvideo`, `reftypevideo`, `descriptionvideo`, `miniature`, `refuser`, `nbviews`, `homepage`) VALUES
(31, 'ENVYUS HappY insane ace on Inferno B !', '827e9829f867efc26c58f13c8eb46301fa005d5c.mp4', NULL, 3, 'Happened a while back!!!', '2c367388cd5f4247572cb6999db69d082f66be01.jpg', 1, 0, 1),
(32, 'S1mple AWP noscope from heaven on Cache', '142d869a8c3ae4ecc4d117345772258e9a44c8aa.mp4', NULL, 3, 'Insanely skilled.\nHe killed them.\nWin the round.\nBut not the final.', 'dc329215f00235a43c44633850472533818f5436.jpg', 1, 0, 1),
(33, 'Snax 1v4 on Cbble', 'c9b8d980bde6675e09dbd49524c8cb23e289d5b5.mp4', NULL, 3, 'iouyh', '2daa663d11faa4d552fcde05c57afac4c43b3d1f.jpg', 1, 0, 1),
(34, 'coldzera anhilated Team Liquid in B Apps on Mirage', 'dc01c109ed7110c9570980e29b6c5e7d1b2920e0.mp4', NULL, 3, 'AND A JUMPING DOUBLE FOR COLD!!', '21596aaaeb276af0f55bbae2cb02b630d4ccdced.jpg', 1, 0, 1),
(35, 'Most Expensive fake Flash from S1mple on Dust2!!', 'ef187f1e17a3a5fd728cffe8af30923bbe6a6adf.mp4', NULL, 3, 'That cost around $10k today', '4fb77348b59e5233265511267add6cd99035474c.jpg', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rangs`
--

CREATE TABLE `rangs` (
  `idrang` int(11) NOT NULL,
  `nomrang` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rangs`
--

INSERT INTO `rangs` (`idrang`, `nomrang`) VALUES
(1, 'Utilisateur'),
(2, 'Joueur'),
(3, 'Modérateur'),
(4, 'Administrateur');

-- --------------------------------------------------------

--
-- Table structure for table `typearticle`
--

CREATE TABLE `typearticle` (
  `idtype` int(11) NOT NULL,
  `nomtype` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typearticle`
--

INSERT INTO `typearticle` (`idtype`, `nomtype`) VALUES
(1, 'Tartes'),
(2, 'Casquettes'),
(46, 'Équipements'),
(47, 'CS:GO'),
(51, 'T-Shirt');

-- --------------------------------------------------------

--
-- Table structure for table `typevideo`
--

CREATE TABLE `typevideo` (
  `idtypevideo` int(11) NOT NULL,
  `nomtypevideo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `typevideo`
--

INSERT INTO `typevideo` (`idtypevideo`, `nomtypevideo`) VALUES
(3, 'CS:GO'),
(4, 'League of Legends'),
(5, 'Rainbow Six Siege'),
(8, 'Autres');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `pseudo` longtext NOT NULL,
  `pass` longtext NOT NULL,
  `mail` longtext NOT NULL,
  `salt` longtext DEFAULT NULL,
  `refrang` int(11) NOT NULL DEFAULT 1,
  `session` longtext DEFAULT NULL,
  `verifaccount` longtext DEFAULT NULL,
  `profilepicture` longtext DEFAULT NULL,
  `about` varchar(5000) DEFAULT NULL,
  `dateinscri` date DEFAULT NULL,
  `allowcommentaire` int(11) DEFAULT 1,
  `resetlink` longtext DEFAULT NULL,
  `resetdate` bigint(200) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`iduser`, `pseudo`, `pass`, `mail`, `salt`, `refrang`, `session`, `verifaccount`, `profilepicture`, `about`, `dateinscri`, `allowcommentaire`, `resetlink`, `resetdate`) VALUES
(1, 'admin', '65c3f75641b22925c737ca657b126cd68c39e423349d43031cf9a3b9a18cee1f', 'example@example.com', NULL, 4, '52f6cffd42b0ee92d6dcd5262f92ab979f817510', NULL, '32709c55eeaed9fbae515fa2f1e3d90a08480159.png', 'Desc', '2021-04-11', 1, '', 0),
(7, 'admin2', 'd7706354167bb01cf46d5cc16170fe05b9f54b9bda010ce71626c9c46194c49b', 'example@example.com', NULL, 4, '1d47933d74307410dd2ce9e8fb97dab1cd5527f9', NULL, '', 'League of legends / Assassin\'s creed unity', '2021-09-05', 0, NULL, NULL),
(20, 'user', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'example@example.com', NULL, 1, '8da61c0317675e302bc3253889baee430f0deeaa', NULL, NULL, NULL, '2021-09-10', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `viewplay`
--

CREATE TABLE `viewplay` (
  `idview` int(11) NOT NULL,
  `refplay` int(11) NOT NULL,
  `refuser` int(11) DEFAULT NULL,
  `ip` varchar(28) NOT NULL,
  `dateview` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `viewplay`
--

INSERT INTO `viewplay` (`idview`, `refplay`, `refuser`, `ip`, `dateview`) VALUES
(33, 31, NULL, '10.7.253.60', '2021-06-13'),
(34, 34, NULL, '10.7.253.60', '2021-06-13'),
(35, 33, NULL, '10.7.253.60', '2021-06-13'),
(36, 35, NULL, '10.7.253.60', '2021-06-13'),
(37, 32, NULL, '10.7.253.60', '2021-06-13'),
(38, 31, NULL, '10.7.253.157', '2021-06-15'),
(39, 33, NULL, '10.7.253.157', '2021-06-15'),
(40, 32, NULL, '10.7.253.157', '2021-06-15'),
(41, 35, NULL, '10.7.253.157', '2021-06-15'),
(42, 34, NULL, '10.7.253.157', '2021-06-15'),
(43, 34, NULL, '10.7.253.152', '2021-06-15'),
(44, 32, NULL, '10.7.253.152', '2021-06-15'),
(45, 33, NULL, '10.7.253.152', '2021-06-15'),
(46, 35, NULL, '10.7.253.152', '2021-06-15'),
(47, 31, NULL, '10.7.253.152', '2021-06-15'),
(48, 31, NULL, '91.68.223.232', '0000-00-00'),
(49, 32, NULL, '91.68.223.232', '0000-00-00'),
(50, 33, NULL, '91.68.223.232', '0000-00-00'),
(51, 35, NULL, '91.68.223.232', '0000-00-00'),
(52, 34, NULL, '91.68.223.232', '0000-00-00'),
(53, 31, NULL, '91.68.223.232', '2021-09-11'),
(54, 32, NULL, '91.68.223.232', '2021-09-11'),
(55, 33, NULL, '91.68.223.232', '2021-09-11'),
(56, 34, NULL, '91.68.223.232', '2021-09-11'),
(57, 35, NULL, '91.68.223.232', '2021-09-11');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `idrefuser` int(11) NOT NULL,
  `idrefarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`idrefuser`, `idrefarticle`) VALUES
(1, 2),
(1, 27);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`idarticle`),
  ADD KEY `articles_reftypearticle_fkey` (`reftypearticle`);

--
-- Indexes for table `avisarticle`
--
ALTER TABLE `avisarticle`
  ADD PRIMARY KEY (`idrefarticle`,`idrefuser`),
  ADD KEY `avisarticle_idrefuser_fkey` (`idrefuser`);

--
-- Indexes for table `commander`
--
ALTER TABLE `commander`
  ADD PRIMARY KEY (`idcommander`),
  ADD KEY `commander_refarticle_fkey` (`refarticle`),
  ADD KEY `commander_refcommande_fkey` (`refcommande`);

--
-- Indexes for table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`idcommande`),
  ADD KEY `commandes_refetat_fkey` (`refetat`),
  ADD KEY `commandes_refuser_fkey` (`refuser`);

--
-- Indexes for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD PRIMARY KEY (`idcommentaire`),
  ADD KEY `commentaires_refuserreceived_fkey` (`refuserreceived`),
  ADD KEY `commentaires_refusersent_fkey` (`refusersent`);

--
-- Indexes for table `commentairesplay`
--
ALTER TABLE `commentairesplay`
  ADD PRIMARY KEY (`idcommentaireplay`),
  ADD KEY `commentairesplay_refplay_fkey` (`refplay`),
  ADD KEY `commentairesplay_refusersent_fkey` (`refusersent`);

--
-- Indexes for table `etats`
--
ALTER TABLE `etats`
  ADD PRIMARY KEY (`idetat`);

--
-- Indexes for table `imagesarticle`
--
ALTER TABLE `imagesarticle`
  ADD PRIMARY KEY (`idimagearticle`),
  ADD KEY `imagesarticle_refarticle_fkey` (`refarticle`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`idmessage`);

--
-- Indexes for table `panier`
--
ALTER TABLE `panier`
  ADD PRIMARY KEY (`idpanier`),
  ADD KEY `panier_refarticle_fkey` (`refarticle`),
  ADD KEY `panier_refuser_fkey` (`refuser`);

--
-- Indexes for table `panier2`
--
ALTER TABLE `panier2`
  ADD PRIMARY KEY (`idrefuser`,`idrefarticle`),
  ADD KEY `panier2_idrefarticle_fkey` (`idrefarticle`);

--
-- Indexes for table `plays`
--
ALTER TABLE `plays`
  ADD PRIMARY KEY (`idvideo`),
  ADD KEY `videos_reftypevideo_fkey` (`reftypevideo`);

--
-- Indexes for table `rangs`
--
ALTER TABLE `rangs`
  ADD PRIMARY KEY (`idrang`);

--
-- Indexes for table `typearticle`
--
ALTER TABLE `typearticle`
  ADD PRIMARY KEY (`idtype`);

--
-- Indexes for table `typevideo`
--
ALTER TABLE `typevideo`
  ADD PRIMARY KEY (`idtypevideo`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `users_refrang_fkey` (`refrang`);

--
-- Indexes for table `viewplay`
--
ALTER TABLE `viewplay`
  ADD PRIMARY KEY (`idview`),
  ADD KEY `viewplay_refplay_fkey` (`refplay`),
  ADD KEY `viewplay_refuser_fkey` (`refuser`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`idrefuser`,`idrefarticle`),
  ADD KEY `wishlist_idrefarticle_fkey` (`idrefarticle`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `idarticle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `commander`
--
ALTER TABLE `commander`
  MODIFY `idcommander` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `commandes`
--
ALTER TABLE `commandes`
  MODIFY `idcommande` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `commentaires`
--
ALTER TABLE `commentaires`
  MODIFY `idcommentaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `commentairesplay`
--
ALTER TABLE `commentairesplay`
  MODIFY `idcommentaireplay` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `etats`
--
ALTER TABLE `etats`
  MODIFY `idetat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `imagesarticle`
--
ALTER TABLE `imagesarticle`
  MODIFY `idimagearticle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `idmessage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `panier`
--
ALTER TABLE `panier`
  MODIFY `idpanier` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plays`
--
ALTER TABLE `plays`
  MODIFY `idvideo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `rangs`
--
ALTER TABLE `rangs`
  MODIFY `idrang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `typearticle`
--
ALTER TABLE `typearticle`
  MODIFY `idtype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `typevideo`
--
ALTER TABLE `typevideo`
  MODIFY `idtypevideo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `viewplay`
--
ALTER TABLE `viewplay`
  MODIFY `idview` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_reftypearticle_fkey` FOREIGN KEY (`reftypearticle`) REFERENCES `typearticle` (`idtype`) ON UPDATE CASCADE;

--
-- Constraints for table `avisarticle`
--
ALTER TABLE `avisarticle`
  ADD CONSTRAINT `avisarticle_idrefarticle_fkey` FOREIGN KEY (`idrefarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `avisarticle_idrefuser_fkey` FOREIGN KEY (`idrefuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commander`
--
ALTER TABLE `commander`
  ADD CONSTRAINT `commander_refarticle_fkey` FOREIGN KEY (`refarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commander_refcommande_fkey` FOREIGN KEY (`refcommande`) REFERENCES `commandes` (`idcommande`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_refetat_fkey` FOREIGN KEY (`refetat`) REFERENCES `etats` (`idetat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `commandes_refuser_fkey` FOREIGN KEY (`refuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commentaires`
--
ALTER TABLE `commentaires`
  ADD CONSTRAINT `commentaires_refuserreceived_fkey` FOREIGN KEY (`refuserreceived`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commentaires_refusersent_fkey` FOREIGN KEY (`refusersent`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `commentairesplay`
--
ALTER TABLE `commentairesplay`
  ADD CONSTRAINT `commentairesplay_refplay_fkey` FOREIGN KEY (`refplay`) REFERENCES `plays` (`idvideo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `commentairesplay_refusersent_fkey` FOREIGN KEY (`refusersent`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `imagesarticle`
--
ALTER TABLE `imagesarticle`
  ADD CONSTRAINT `imagesarticle_refarticle_fkey` FOREIGN KEY (`refarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `panier_refarticle_fkey` FOREIGN KEY (`refarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `panier_refuser_fkey` FOREIGN KEY (`refuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `panier2`
--
ALTER TABLE `panier2`
  ADD CONSTRAINT `panier2_idrefarticle_fkey` FOREIGN KEY (`idrefarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `panier2_idrefuser_fkey` FOREIGN KEY (`idrefuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `plays`
--
ALTER TABLE `plays`
  ADD CONSTRAINT `videos_reftypevideo_fkey` FOREIGN KEY (`reftypevideo`) REFERENCES `typevideo` (`idtypevideo`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_refrang_fkey` FOREIGN KEY (`refrang`) REFERENCES `rangs` (`idrang`) ON UPDATE CASCADE;

--
-- Constraints for table `viewplay`
--
ALTER TABLE `viewplay`
  ADD CONSTRAINT `viewplay_refplay_fkey` FOREIGN KEY (`refplay`) REFERENCES `plays` (`idvideo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `viewplay_refuser_fkey` FOREIGN KEY (`refuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_idrefarticle_fkey` FOREIGN KEY (`idrefarticle`) REFERENCES `articles` (`idarticle`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_idrefuser_fkey` FOREIGN KEY (`idrefuser`) REFERENCES `users` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
