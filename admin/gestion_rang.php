<?php
$adminPage = true; $titrePage = "Gestion des rangs";
include('../include/init.php');
include('header.php');
?>
<?php

/* Si on à l'id d'un article dans le liens (en method get), alors on affiche la page de ce type */
if (isset($_GET['idrang']) && !empty($_GET['idrang']) && is_numeric($_GET['idrang'])) {

	$sql_id_exist = $connexion->prepare("SELECT count(*) FROM rangs where idrang=:idrang ;");
	$sql_id_exist->execute(array(
		'idrang'=> $_GET['idrang']
	));
	$sql_id_exist=$sql_id_exist->fetch();

	if ($sql_id_exist[0]>0) {

      	if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }             
            }

	    ?><h1>Changer le rang <?php 

        $sql_nomrang = $connexion->prepare("SELECT * from rangs where idrang=:idrang;");
        $sql_nomrang->execute(array(
        	'idrang'=> $_GET['idrang']
        ));

     	$sql_nomrang=$sql_nomrang->fetch();
     	$nomrang=$sql_nomrang['nomrang'];
     	$idrang=$sql_nomrang['idrang'];

        ?></h1>
        <hr style="margin-bottom: 30px;">

        <div class="flexBlock">
            <div class="flexContent border-first">
                <h2>Changer le nom du rang</h2>
                <form method="post" action="../include/forms/admin/setRang.php">         
                    <div id="form">
                        <p>Entrée un nouveau nom pour le rang : </p>  
                        <input type="text" name="nom_rang" value="<?php echo $nomrang; ?>" placeholder="Nouveau nom de la rang">
                        <input type="hidden" name="idrang" value="<?php echo $idrang; ?>">
                        <div class="sendButton">
                            <button type="submit" name="changeNomrang" class="btn">Changer le nom</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="flexContent">
                <h2>Supprimer le rang</h2>
                <form method="post"  action="../include/forms/admin/setRang.php">         
                    <div id="form">          
                        <div class="sendButton">
                            <button type="submit" name="delete_rang" class="btn">Supprimer</button>
                   			<input type="hidden" name="idrang_del" value="<?php echo $idrang; ?>">
                        </div>
                    </div>

<?php
    } else {  
        header('Location: ./gestion_rang.php?err=37');
        exit();
    }
} else {  

    if(isset($_GET['err']) OR isset($_GET['succ'])) {
        if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
            $idMsg = $_GET['err'];
            echo getMessage($idMsg);
        }
        if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
            $idMsg = $_GET['succ'];
            echo getMessage($idMsg);
        }
    }
 	
?>
<h1>Rangs des utilisateurs</h1>
<hr style="margin-bottom: 30px;">
    <table>
        <tr>
            <td colspan="2">
                <form method="post"  action="../include/forms/admin/setRang.php">
                    <div id="form">
                        <input type="text" placeholder="Nom du rang" style="width: 50%;display: inline-block;margin: 25px;" name="new_rang" >         
                            <div class="sendButton" style="vertical-align: top;">
                                <button type="submit" name="add_rang" class="btn">ajouter un rang</button>
                            </div>
                    </div>	
                </form>
            </td>
        </tr>
    	<tr style="font-weight: bold;">
    		<td>Nom du rang</td>
    		<td style="width: 30%;">Action</td>
    	</tr>
<?php
		$sql_type = $connexion->prepare("SELECT * from rangs order by idrang;");
    	$sql_type->execute();
        $sql_type=$sql_type->fetchall();
        foreach ($sql_type as $ligne) {
        	$nomrang=$ligne['nomrang'];
        	echo "<tr><td> ".$nomrang."</td>"; 
        	echo"<td><a class=' ' href='?idrang=".$ligne['idrang']."' name='modifier'>Modifier</a>"; 
        }
?>
</table>
<?php } ?>