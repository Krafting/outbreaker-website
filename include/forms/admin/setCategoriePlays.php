<?php
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

/* Apres avoir appuyer sur le bouton , on verifie que l'entrée ne sois pas vide sinon redirection page d'erreur*/
if( isset($_POST['changeNomCategorie'])) {

	/*Verif champs non vide*/
	if(!empty($_POST['nom_categorie'])) {
		$sql_exist = $connexion->prepare(" SELECT count(*) from typevideo where idtypevideo=:idtype ;");
		$sql_exist->execute(array(
			'idtype' => $_POST['idtype']
		));
		$sql_exist=$sql_exist->fetch();

		if ($sql_exist[0]>0) {
			$sql_setCategorie = $connexion->prepare("UPDATE typevideo set nomtypevideo=:nomtype where idtypevideo=:idtype ;");
			$sql_setCategorie->execute(array(
				'nomtype' => $_POST['nom_categorie'],
				'idtype' => $_POST['idtype']
			));
			header('Location: ../../../admin/gestion_categorie_plays.php?succ=34&idtype='.secure($_POST['idtype']).''); 
			exit();
		} else {
			header('Location: ../../../admin/gestion_categorie_plays.php?succ=37'); 
			exit();
		} 
	} else {
		header('Location: ../../../admin/gestion_categorie_plays.php?succ=39&idtype='.secure($_POST['idtype']).''); 
		exit();
	} 
}

/* quand on veut delete la categorie , il faut verifier qu'aucun play n'appartiens à cette categorie */
if (isset($_POST['delete_categorie'])) {

	$sql_link_play = $connexion->prepare(" SELECT count(*) from plays where reftypevideo=:idtype ;");
	$sql_link_play->execute(array(
		'idtype' => $_POST['idtype_del']
	));
	$sql_link_play=$sql_link_play->fetch();

	if ($sql_link_play[0]==0) {
		$sql_deleteCategorie = $connexion->prepare("DELETE from typevideo where idtypevideo=:idtype ;");
		$sql_deleteCategorie->execute(array(
			'idtype' => $_POST['idtype_del']
		));
		header('Location: ../../../admin/gestion_categorie_plays.php?succ=36'); 
		exit();
	} else {
		header('Location: ../../../admin/gestion_categorie_plays.php?err=76&idtype='.secure($_POST['idtype_del']).'');  
		exit();
	}
}



/* On verifie qu'on n'as bien donner un nom à la catégorie avant de la creer */
if (isset($_POST['add_categorie'])) {
	if(!empty($_POST['new_categorie'])) {
		$sql_addCategorie = $connexion->prepare("INSERT into typevideo (nomtypevideo) VALUES (:nouvelle_categorie);");
		$sql_addCategorie->execute(array(
			'nouvelle_categorie' => $_POST['new_categorie']
		));
		header('Location: ../../../admin/gestion_categorie_plays.php?succ=38&idtype='.secure($_POST['idtype']).'');
		exit();
	} else {
		header('Location: ../../../admin/gestion_categorie_plays.php?err=39');
		exit();
	}
}
?>