<?php

include("../../init.php"); 

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}


/* quand on click sur le bouton pour selectionner la photo */
if (isset($_POST['selectPicturePost'])){
	/* si on as selectionné une image */
	if (!empty ($_POST['selectImage'])) {
		/* on verifie si le fichier existe dans le dossier de l'utilisateur si oui alors on l'as change */
		if (file_exists ("../../../upload/profiles/".$_POST['id_user']."/".$_POST['selectImage'])){
			$sql_set_picture = $connexion->prepare("UPDATE users set profilepicture=:select_image where iduser=:iduser;");
			$sql_set_picture->execute(array(
				'select_image' => secure($_POST['selectImage']),
				'iduser' => $_POST['id_user']
			));

			header('Location: ../../../admin/users.php?succ=70&id='.$_POST['id_user']); 
			exit();
		} else {
			header('Location: ../../../admin/users.php?err=72&id='.$_POST['id_user']);
			exit();
		}
	} else {
		header('Location: ../../../admin/users.php?err=1&id='.$_POST['id_user']);	
		exit();
	}
}


/* si on veut supprimer une image */
if (isset($_POST['deletePicturePost'])) {
	/* on verifie qu'une image à bien été selectionné*/
	if (!empty($_POST['deleteImage'])) {
		/* on verifie que l'image qu'on veut supprimer existe */
		if(file_exists ("../../../upload/profiles/".$_POST['id_user']."/".$_POST['selectImage'])) {

			/* on verifie si l'image supprimer soit celle de la photo de profil actuelle
			comme on la set avec rien , elle prendra la valeur de la photo de profile par defaut */
			$sql_image=$connexion->prepare("select * from users where iduser=:iduser;");
			$sql_image->execute(array(
			'iduser' => $_POST['id_user']
			));
			$sql_image=$sql_image->fetch();
			if ($_POST['deleteImage']==$sql_image['profilepicture']) {
				$sql_delete = $connexion->prepare("UPDATE users SET profilepicture=:pp where iduser=:iduser;");
				$sql_delete->execute(array(
					'pp' => '',
					'iduser' => $_POST['id_user']
				));
				unlink("../../../upload/profiles/".$_POST['id_user']."/".$_POST['deleteImage']);
				header('Location: ../../../admin/users.php?succ=14&id='.$_POST['id_user']); 
				exit();
			} else {
				unlink("../../../upload/profiles/".$_POST['id_user']."/".$_POST['deleteImage']);
				header('Location: ../../../admin/users.php?succ=14&id='.$_POST['id_user']);
				exit();
			}
		} else {
			header('Location: ../../../admin/users.php?err=72&id='.$_POST['id_user']);
			exit();
		}
	} else {
		header('Location: ../../../admin/users.php?err=1&id='.$_POST['id_user']);
		exit();
	}
}


?>