<?php 
//parametre pour upload image
include('../../../include/init.php');    
define('MAX_SIZE', 8000000);
$tabExt = array('jpg','gif','png','jpeg'); 
$infosImg = array();
$image =$_FILES['photo_article']['name'];

//si l'utilisateur n'est pas admin et/ou si il n'est pas connecter , on n'affiche pas la page 
if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

 /* On verifie qu'on n'as bien donner un nom à l'article et un type avant de le creer */ 
if (isset($_POST['ajouterArticle']) && !empty($_POST['nom_article']) && !empty($_POST['nom_type']))  {
	$sql_verifNomArticle = $connexion->prepare("SELECT count(*) FROM articles where nomarticle=:nomarticle;");
	$sql_verifNomArticle->execute(array(
		'nomarticle' => $_POST['nom_article']
	));
	$sql_verifNomArticle=$sql_verifNomArticle->fetch();

	/*Requete pour verifier si le type entrée existe*/
	$sql_verifType = $connexion->prepare("SELECT count(*) FROM typearticle where idtype=:idtype;");
	$sql_verifType->execute(array(
		'idtype' => $_POST['nom_type']
	));
	$sql_verifType=$sql_verifType->fetch();

	//si le nom n'existe pas déjà alors on vérifie d'autre condidtion
	if ($sql_verifNomArticle[0]==0) {

		//si le type existe bien alors on vérifie d'autre conditions
		if ($sql_verifType[0]>0) {

			//si une quantité est bien rentré et que c'est un nombre
			if (!empty($_POST['qte_article']) && is_numeric($_POST['qte_article'])) {
			
				//si un prix est bien rentré et que c'est un nombre
				if (!empty($_POST['prix_article']) && is_numeric($_POST['prix_article'])) {
					/*toute condition rempli : on peut creer ce nouvelle article*/
					$sql_addArticle = $connexion->prepare("INSERT into articles (nomarticle,qtedispo,prixvente,desarticle,reftypearticle,keywords) VALUES  (:nomarticle,:qtedispo,:prixvente,:desarticle,:reftypearticle,:keywords);");
					$sql_addArticle->execute(array(
						'nomarticle' => $_POST['nom_article'],
						'qtedispo' => $_POST['qte_article'],
						'prixvente' => $_POST['prix_article'],
						'desarticle' => $_POST['des_article'],
						'reftypearticle' => $_POST['nom_type'],
						'keywords' => $_POST['mots_clefs']
					));
					$lastID=$connexion->lastInsertId();
					/* recuperation de l'id de l'article qui viens d'être creer*/

					if  (!empty($_FILES['photo_article']['name'])) { 
					/* avons nous bien selectionner un fichier */
				
						/* On regarde si le dossier de photo de l'user existe déjà sinon on lui créait un dossier */

						if (!file_exists('../../../upload/articles/'.$lastID.'/')) {
							mkdir('../../../upload/articles/'.$lastID.'/', 0775, true); /*0755 droit*/
						}
						$extension  = pathinfo($_FILES['photo_article']['name'], PATHINFO_EXTENSION);
					
					
						/* verifie si l'extension est correcte (extension depuis le tableau au début du code)*/

						if(in_array(strtolower($extension),$tabExt)) { /*string to lower car si une maj ca compte comme différent*/
							$nomImage = sha1(uniqid()) .'.'. $extension;
					
							/* si l'upload est reussi on rentre le liens de l'image dans la bdd*/

							if(move_uploaded_file(htmlspecialchars($_FILES['photo_article']['tmp_name']), '../../../upload/articles/'.$lastID.'/'.$nomImage)) {
						
								/*on lie l'image à l'article*/
								$sql_image = $connexion->prepare("UPDATE articles set imageprincipale=:nom_image where idarticle=:idarticle;");
								$sql_image->execute(array(
								'nom_image' => $nomImage,
								'idarticle' => $lastID
								));	
								header('Location: ../../../admin/gestion_article.php?succ=46');
								exit();
							} else  {
								header('Location: ../../../admin/users.php?err=1');
								exit();
									}
						}  else { 
							header('Location: ../../../admin/ajouterArticle.php?err=1');
							exit();
								} 
					} else {
						header('Location: ../../../admin/ajouterArticle.php?err=1');
						exit();
							}
				} else {
					header('Location: ../../../admin/gestion_article.php?err=1');
					exit();
				}
			} else {
				header('Location: ../../../admin/ajouterArticle.php?err=1');
				exit();
			} 
		} else {
			header('Location: ../../../admin/ajouterArticle.php?err=37');
			exit();
		}
	} else {
		header('Location: ../../../admin/ajouterArticle.php?err=77');
		exit();
	}
} else {
	header('Location: ../../../admin/ajouterArticle.php?err=1');
    exit();
}



?>