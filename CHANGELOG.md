04.05.2021:
- Fini la page d'édition des utilisateurs (Delete a faire)
- Fini la page pour commander (On peu commander des articles depuis notre panier)
- Ajouter la fonction findProfilePic() afin de trouver les photo de profile en fonction d'un IDuser
- Editer la page membre.php (typo & utilisation de la fonction findProfilePic)
- Modification minimal de la boutique

05.05.2021
- Ability to delete an user & update his rank
- Register.php: redirection quand on est déjà connecté
- Possibilité de regiriger après le login
- Fonction de troncage
- Ajout des descriptions utilisateurs
- Ne pas pouvoir supprimer les admins/modos depuis l'interface web
- Few CSS fixes
- membre.php : debut du developpement de la gestion des commandes
- boutique: Possibilité de rechercher par mot clés et catégorie
- Fixed typos

06.05.2021
- Started work on the Avis on the article page
- Ability to update user description depuis la page admins
- préparée.
- Fixed bug in the email regex
- Fixed la selection des roles dans la partie admin
- Fixed some CSS

07.05.2021
- Added admin page to modify shop categories
- Ajouter une fonction pour les message d'erreur et de succès
- On peut ajouter des avis sur les articles qu'on a déjà commander!
- Bug fixes
- More de préparée !
- Debut du listing des avis sur les articles
- Header boutique (avec panier et autres infos)
- Quand un produit est en rupture, ne pas pouvoir l'ajouter au panier.

13.05.2021
- Boutique: Quand on commande, décrémenter le nombre de l'item acheter
- Boutique: Les avis sont maintenant plus joli
- Messages: Corriger une erreur si on ne mettais pas un nombre dans le GET err/succ
- Boutique: Recherche complète! (possible faille de sécurité)
- Utiliser la nouvelle fonction partout pour les messages d'erreur/success sur les pages membres
- Systeme de wishlist (Un panier public)
- Added date d'inscription
- Gestion des catégories Finis
- Bug fixes sur la gestion des utilisateurs
- Debut de la gestion des articles
- Dashboard sur la page admin (WIP)
- More css in admin page
- Copy JS function

14.05.2021
- Ajouter les pages de profil
    - Possibilité de mettre des commentaires
    - Montre les avis des articles que l'utilisateur a laissé
    - Montre son A Propos
- Les commentaires peuvent etre désactivé/activé
- Début du travail sur la gestion des articles (ajout, modif)
- Ajouter des fonction php pour les dates

17.05.2021
- CSS tweaks sur les avis et commentaires d'utilisateurs
- Ajouté l'abilité de voir les details d'une commande d'un utilisateur
    - Ajouté un prixtotal dans la commande, pour pouvoir y mettre une réduction si besoin
    - Possibilité d'annulé une commande si elle n'est pas expédier/validée
- Ajouter les photos prises dans la BDD pour les articles
- Corriger un bug sur les wishlists quand on est pas connecté
- Abilités d'ajouter un articles
- Debut du travail sur les Plays
- Abilité de modifier un article
- Ajout d'un Play

25.05.2021
- Gestion des articles améliorée
- Commencement de la gestion des commandes
- Uploader, modifier, supprimer les plays
- Pages de profil: ajout des plays de l'user
- ajouter de nouvelle fonctions (findNumPlayComment)
- Edited CSS
- Edited header in adminzone

26.05.2021
- Ajouter functions php
- $needConnect = true; si on veux que l'utilisateur soit connecté
- Bugfixes sur les plays
- Plays en homepage
- Ajouter de la pagination sur les plays et les articles
- Nouveau systeme de messages sur la page users admin
- Checkbox pour les commentaires
- Added regex sur l'email dans la zone admin
- Added lostpassword page (WIP)
- gestion des comandes début
- Lostpassword pages working ! (sauf pour les mails)

28.05.2021
- Gestion des catégories des Plays
- Descriptions sur les vidéos
- Suppression des commentaires possibles (utilisateurs et plays)
- Bugfixes sur la gestion des catégogies des articles
- CSS tweaks

31.05.2021
- Images secondaire sur les articles (ajout, deletion, affichage)
- CSS tweaks (y'en aura tout le temps du tweak CSS...)
- Gestion des Plays (edition et listing)
- Ajout d'image secondaires
- Cursor are crosshairs

03.06.2021
- Regex sur les inputs (carte bancaire)
- Max file size configurable dans init.php (videos et images)
- Gestion des avis d'articles
- Button pour 'gestion' des commentaires (videos et profiles)
- 1 Item de chaque dans les wishlists (pas de doublons)
- Panier (Utilisation de quantité plutot que de mettre des articles pleins de fois)
- Editer les commandes pour utiliser le nouveau systeme de panier

07.06.2021
- Gestion commentaire Play
- Fixed some price sum quand on commande
- Ajouter quelques fonctions
- Edition d'un play: Check si le dossier de la miniature existe
- CSS & HTML tweaks
- Deleted unused files
- Envoie de mail fonctionnel (récuperation de mot de passe)
- Supporte les retour chariots dans les commentaires de Play, Utilisateurs et les Avis
- Ajouter qte des articles dans gestion des commandes
- Ajouter des arondis sur les prix totaux des commandes/panier/wishlist
- Fix un header() dans l'edition de Plays

13.06.2021
- Amélioré la fonciton truncage
- RANDOM videos sur la page d'acceuil
- TITLE sur les plays (parce qu'on a le truncage)
- Modification du fomulaire ADMIN de modification de PLAYS
- Ajouter du gras (admin zone)
- Gestion des rangs utilisateurs

11.09.2021
- Fais en sorte que tout marche via MySQL
- Ajouter du hCaptcha
- Fais 2 branches, 1 Postgres (/postgresql) et 1 mysql (/master)
- Added SQL database files
- Edité le README
- Suppression du rang par default (id=1) devenue impossible (sinon on peut plus s'inscrire vu que les users par default on besoin d'un idrang=1 avec une clé secondaire)
- utilisation de mon CDN pour fontawesome
- Corrigé des typos dans les requetes SQL
