<?php
$adminPage = true; $titrePage = "Gestion des utilisateurs";
include('../include/init.php');
include('header.php');

  /* Si on supprime un utilisateur */ 
if (isset($_GET['del']) && is_numeric($_GET['del']) && !empty($_GET['del'])) {
    $sql_count = $connexion->prepare("SELECT count(*) from users where iduser=:iduser ;");
    $sql_count->execute(array(
        'iduser' => $_GET['del']
    ));
    $sql_count=$sql_count->fetch();
        
        /* on regarde si il existe */
        if ($sql_count[0]>0) {
            $sql_admin = $connexion->prepare("SELECT refrang from users where iduser=:iduser ;");
            $sql_admin->execute(array(
                'iduser' => $_GET['del']
            ));
            $sql_admin=$sql_admin->fetch();
            /* si son rang n'est pas admin */
            if ($sql_admin['refrang']<3) {
                $sql_delete = $connexion->prepare("delete from users where iduser=:iduser ;");
                $sql_delete->execute(array(
                    'iduser' => $_GET['del']
                ));
                header('Location: ./users.php?succ=66');
                exit();
            } else {
                header('Location: ./users.php?err=65');
                exit();
            }
        } else {
            header('Location: ./users.php?err=64');
            exit();
        }

   } elseif (isset($_GET['id']) && !empty($_GET['id'])) {
        $sql_id = $connexion->prepare("SELECT * from users INNER JOIN rangs ON refrang=idrang where iduser=:iduser;");
        $sql_id->execute(array(
            'iduser' => $_GET['id']
        ));
        $affich=$sql_id->fetch();
            
            
            
        if(isset($_GET['err']) OR isset($_GET['succ'])) {
            if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                $idMsg = $_GET['err'];
                echo getMessage($idMsg);
            }
            if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                $idMsg = $_GET['succ'];
                echo getMessage($idMsg);
            }
        }?>
        <h1>Changer le profil de <?php echo $affich['pseudo']; ?></h1>
        <hr style="margin-bottom: 30px;">
        <style>
             .image-profile  {
                width: 150px;
                height: 150px;
                float: left;
                border-radius: 50%;
                margin-right: 15px;
                display: inline-block;
            }
            h1.profile {
                display: inline-block;
                float: left;
                margin: 0 !important;
                vertical-align: top;

            }
        </style>
        <form method="post"  action="../include/forms/admin/setProfile.php" enctype="multipart/form-data">         
        <div class="flexBlock">
            <div class="flexContent border-first">
            <h2>Aperçu du profil<br>(<a style="color: var(--blue)" href="../profiles.php?id=<?php echo $affich['iduser']; ?>">Voir la page de profile</a>)</h2>
                <div class="block" style="display:block; height: 150px;">
                    <img src="../../upload/profiles/<?php echo findProfilePic($affich['iduser']); ?>" class="image-profile">
                    <h1 class="profile"><?php echo $affich['pseudo']; ?></h1><br><br>
                    <h1 class="profile" style="font-style: italic;font-size: 40px;font-weight: 300;"><?php echo $affich['nomrang']; ?></h1><br><br><br>
                    <p style="font-style: italic; text-align: left;">Inscrit le <?php echo dateInscri($affich['iduser']); ?></p><br>
                </div><br>
                    <h1 class="profile">À mon propos</h1>
                    <br>
                    <br>
                    <br>
                    <p style='text-align: justify'><?php echo formatTexte($affich['about']); ?></p>
            </div>
            <div class="flexContent">
                <h2>Changer le profil</h2>
                <p>Changer le pseudo </p>  
                <input type="text" name="pseudo" value="<?php echo $affich['pseudo']; ?>" placeholder="Votre pseudo">
                <input type="hidden" name="id" value="<?php echo $affich['iduser'];?>" >

                <p>À propos de <?php echo $affich['pseudo']; ?></p>  
                <textarea style="margin-bottom: 0;" name="about" onfocus="countChar()" onkeyup="countChar()" onkeydown="countChar()" id="textareaCount" maxlength="2000" placeholder="À propos de vous, ce texte est afficher sur votre page de profile"><?php echo $affich['about']; ?></textarea>
                <small id="count" style="display: block; text-align: right;height: 5px"></small>

                <p>Changer la photo de profil</p>  
                <input type="file" name="dlpicture">
                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                <a href="#" onclick="openModal('modalPictureOld')" style="display:block">Selectionner une ancienne image.</a>

                <div class="sendButton">
                    <button type="submit" name="changeProfile" class="btn">Changer le profile</button>
                </div>
            </div>
        </div>
        <br>
        <br>
        </form>

        <div id="modalPictureOld" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>Utiliser une ancienne photo de profil</h2>
            </div>
            <p>
            <style>
            .testborder:focus > img {
                border: 5px solid var(--blue); 
            }
            </style>
                <?php $listeImages = glob("../../upload/profiles/".$affich['iduser']."/*"); 
                $counter = 0;
                foreach($listeImages as $images) {
                    $arr = explode('/', $images);
                    $counter++;?>
                        <a class="testborder" href="#" tabindex=0>
                        <img onclick="selectPicture('<?php echo $arr['5']; ?>')" id="" src="<?php echo $images; ?>" class="modal-image">
                    </a>
                </label>
                <?php } 
                if($counter == 0) { ?>
                    <div class="infoMessage">
                        L'utilisateur n'a pas encore uploader de photos de profil
                    </div>
                <?php } ?>
                <div class="sendButton">
                    <form method="POST" action="../include/forms/admin/setOldPicAdmin.php">
                        <input type="hidden" name="selectImage" id="selectImageName">
                        <input type="hidden" name="id_user" value="<?php echo $affich['iduser'];?>" >
                        <button type="submit" name="selectPicturePost" class="btn">Selectionner cette image</button>
                    </form>
                </div>
                <div class="sendButton">
                    <form method="POST" action="../include/forms/admin/setOldPicAdmin.php">
                        <input type="hidden" name="id_user" value="<?php echo $affich['iduser'];?>" >
                        <input type="hidden" name="deleteImage" id="selectImageNameDelete">
                        <button type="submit" name="deletePicturePost" class="btn">Supprimer cette image</button>
                    </form>
                </div>
            </p>
        </div>
        </div>


        <h1>Changer les informations personelles</h1>
        <div class="flexBlock">
            <div class="flexContent border-first">
                <h2>Changer le compte</h2>
                <form method="post" action="../include/forms/admin/setAccount.php">         
                    <div id="form">
                        <p>L'adresse e-mail </p>  
                        <input type="text" name="mail" value="<?php echo $affich['mail']; ?>" placeholder="Adresse mail de l'utilisateur">
                        <select name="rang">
                            <?php $sql_rang="select idrang,nomrang from rangs order by idrang;";
                                        $rang=$connexion->query ($sql_rang);
                                        $result_rang=$rang->fetchall();
                                        foreach ($result_rang as $ligne) { ?>
                                        <option <?php if ($affich['idrang']==$ligne['idrang']) {echo 'selected=""';} ?> value="<?php echo $ligne['idrang'];?>"> <?php echo $ligne['nomrang'];?></option> <?php } ?>

                        </select>                       
                        <input type="hidden" name="id" value="<?php echo $affich['iduser'];?>" >
                        <input type="checkbox" name="allowComment" id="comment" value="1" <?php if($affich['allowcommentaire'] == 1) { echo "checked"; } ?>><label for="comment"> Autorisé les commentaires sur le profil public</label>
                        <div class="sendButton">
                            <button type="submit" name="changeAccount" class="btn">Changer le compte</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="flexContent">
                <h2>Changer de mot de passe</h2>
                <form method="post" name="setpassword" action="../include/forms/admin/setPassword.php">         
                    <div id="form">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <p>Le nouveau mot de passe </p> 
                        <input type="password" name="new_pass" placeholder="Votre nouveau mot de passe">
                        <p>Confirmer le nouveau mot de passe </p> 
                        <input type="password" name="new_pass_conf" placeholder="Confirmer votre nouveau mot de passe">

                        <div class="sendButton">
                            <button type="submit" name="setpassword" class="btn">Changer le mot de passe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    <?php } else {
        /* messages d'erreurs et de succès */
        if(isset($_GET['err']) OR isset($_GET['succ'])) {
            if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                $idMsg = $_GET['err'];
                echo getMessage($idMsg);
            }
            if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                $idMsg = $_GET['succ'];
                echo getMessage($idMsg);
            }
        } ?>

<h1>Gestion des utilisateurs</h1>
    <hr style="margin-bottom: 30px;">
    <table>
    	<tr>
    		<td>Nom d'utilisateur</td>
    		<td>Adresse e-mail</td>
    		<td>Rang</td>
    		<td style="width: 30%;">Action</td>
    	</tr>
    	
    	<?php 

    	$sql = $connexion->prepare('SELECT * from users inner join rangs on refrang=idrang order by nomrang;');
        $sql->execute();
        $sql=$sql->fetchall();
        foreach ($sql as $ligne) {
        	$pseudo=$ligne['pseudo'];
        	echo "<tr><td> ".$pseudo."</td>"; 
        	$mail=$ligne['mail'];
        	echo "<td>".$mail."</td>";
        	echo "<td>".$ligne['nomrang']."</td>";
        	echo"<td><a href='?id=".$ligne['iduser']."' name='supprimer'>Modifier</a> 
                    <a onclick='openModal(\"delUser".$ligne['iduser']."\")' href='#'>Supprimer</a>
                    <div id='delUser".$ligne['iduser']."' class='modal'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <span class='close'>&times;</span>
                            <h2>Êtes-vous sûr de vous ?</h2>
                        </div>
                            <h1>Êtes-vous sur de vouloir supprimer l'utilisateur : <br>".$ligne['pseudo']."</h1>
                            <div class='sendButton'>
                                <a href='?del=".$ligne['iduser']."' class='btn'>Supprimer cet utilisateur</a>
                            </div>
                        </p>
                    </div>
                    </div>
                    </td>";
        }
    	?>
    </table>
    <?php }

 include('footer.php'); ?>


