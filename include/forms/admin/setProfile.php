<?php
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

define('TARGET', '../../../upload/profiles/'.$_POST['id'].'/');    
define('MAX_SIZE', 8000000);
$tabExt = array('jpg','gif','png','jpeg'); 
$infosImg = array();
$image = secure($_FILES['dlpicture']['name']);
echo $image;
print_r($_POST);

/* quand on appuie sur le bouton changeprofile */

if( isset($_POST['changeProfile'])) { 

	/* Si un fichier est selectionné  */

	if  (!empty($_FILES['dlpicture']['name'])) {			
		/* On regarde si le dossier de photo de l'user existe déjà sinon on lui crée un dossier */
		if (!file_exists('../../../upload/profiles/'.$_POST['id'].'/')) {
			mkdir('../../../upload/profiles/'.$_POST['id'].'/', 0755, true);
		}
		$extension  = pathinfo($_FILES['dlpicture']['name'], PATHINFO_EXTENSION);
		/* verifie si l'extension est correcte */
		if(in_array(strtolower($extension),$tabExt)) {
			$nomImage = sha1(uniqid()) .'.'. $extension;
			/* si l'upload est reussi on rentre le liens d el'image dans la bdd*/

			if(move_uploaded_file(htmlspecialchars($_FILES['dlpicture']['tmp_name']), TARGET.$nomImage)) {
				$sql_image = $connexion->prepare("UPDATE users set profilepicture=:nom_image where iduser=:iduser;");
				$sql_image->execute(array(
					'nom_image' => $nomImage,
					'iduser' => $_POST['id']
				));	
				header('Location: ../../../admin/users.php?succ=70&id='.secure($_POST['id']));
				exit();
			} else {
				header('Location: ../../../admin/users.php?err=9&id='.secure($_POST['id']));
				exit();
			}
		} else {
			header('Location: ../../../admin/users.php?err=7&id='.secure($_POST['id']));
			exit();
		}
	}	

	/* si le pseudo n'est pas vide alors on update le profil */
	if (!empty($_POST['pseudo']) AND isset($_POST['about'])) {
		$sql_pseudo = $connexion->prepare("UPDATE users SET pseudo=:pseudo, about=:about WHERE iduser=:iduser;");
		$sql_pseudo->execute(array(
			'pseudo'=> $_POST['pseudo'],
			'about'=> $_POST['about'],
			'iduser'=> $_POST['id']
		));
		header('Location: ../../../admin/users.php?err=70&id='.secure($_POST['id']));
		exit();
	} else { 
		header('Location: ../../../admin/users.php?err=69&id='.secure($_POST['id']));
		exit();
	}
} else {
	header('Location: ../../../admin/users.php?err=1&id='.secure($_POST['id']));
    exit();
}
		
?>