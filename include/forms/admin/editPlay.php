<?php
include('../../init.php');
define('TARGETTHUMB', '../../../../upload/thumbs/'.$_POST['iduser'].'/');    
define('MAX_SIZE', 8000000);
$tabExt = array('jpg','png','gif','jpeg'); 

/* SI ON EST PAS CONNECTÉ ET SI NOTRE RANG N'EST PAS SUFFISANT */
if(!connect() OR !isAdmin()) {
    header('Location: ../../index.php');
    exit();
}

if(connect() and isAdmin()) {
    /* SI LE POST A ÉTÉ ENVOYÉ ET QUE TOUT LES CHAMPS REQUIS SONT REMPLIS */
    if(isset($_POST['sendPlay'])) {
        if(isset($_POST['catPlay']) && !empty($_POST['catPlay']) 
        && isset($_POST['descPlay']) && isset($_POST['titrePlay']) && !empty($_POST['titrePlay'])) {
            /* SI L'UTILISATEUR A ENVOYÉ UNE NOUVELLE MINIATURE */
            if(!empty($_FILES['playThumb']['name'])) {
                /* EXTENTION DES FICHERS UPLOADER */
                $extension  = pathinfo($_FILES['playThumb']['name'], PATHINFO_EXTENSION);
                /* SI L'EXTENTION EST AUTORISÉE */
                if(in_array(strtolower($extension),$tabExt)) {
                    $nomImage = sha1(uniqid()) .'.'. $extension;
                    /* SI L'UPLOAD A ÉTÉ FAIS AVEC SUCCÈS */
                    if(move_uploaded_file($_FILES['playThumb']['tmp_name'], TARGETTHUMB.$nomImage)) {
                        $updateVideoBDD = $connexion->prepare('UPDATE plays SET miniature=:miniature WHERE refuser=:iduser AND idvideo=:idplay');
                        $updateVideoBDD->execute(array(
                            'miniature' => $nomImage,
                            'iduser' => $_SESSION['id'],
                            'idplay' => $_POST['idvideo']
                        ));
                    } else {
                        header('Location: ../../../admin/gestion_plays.php?err=60&idplay='.$_POST['idvideo']);
                        exit();
                    }
                } else {
                    header('Location: ../../../admin/gestion_plays.php?err=7&idplay='.$_POST['idvideo']);
                    exit();
                }
            } 

            $updateVideoBDD = $connexion->prepare('UPDATE plays SET titrevideo=:titrevideo, reftypevideo=:reftypevideo, descriptionvideo=:descriptionvideo WHERE idvideo=:idvideo');
            $updateVideoBDD->execute(array(
                'titrevideo' => $_POST['titrePlay'],
                'reftypevideo' => $_POST['catPlay'],
                'descriptionvideo' => $_POST['descPlay'],
                'idvideo' => $_POST['idvideo']
            ));
            header('Location: ../../../admin/gestion_plays.php?succ=58&idplay='.$_POST['idvideo']);
            exit();
        } else {
            header('Location: ../../../admin/gestion_plays.php?err=1');
            exit();
        }
    }

    /* SI ON DELETE, ON CHECK SI LE POST N'EST PAS VIDE ET L'ID EST NUMERIC */
    if(isset($_POST['delPlay'])) {
        if(isset($_POST['idvideo']) && is_numeric($_POST['idvideo'])) {

                /* ON SELECTIONNE LES FICHIERS AFIN DE LES SUPPRIMER */
                $selectPlayUserDelete = $connexion->prepare('SELECT * FROM plays WHERE idvideo=:idplay');
                $selectPlayUserDelete->execute(array(
                    'idplay' => $_POST['idvideo']
                ));
                $unlinkData = $selectPlayUserDelete->fetch();
                /* ON DELETE LES FICHIERS */
                unlink('../../../../upload/thumbs/'.$unlinkData['refuser'].'/'.$unlinkData['miniature']);
                unlink('../../../../upload/plays/'.$unlinkData['refuser'].'/'.$unlinkData['nomfichiervideo']);

                /* ON DELETE DE LA BDD LA VIDEO ET LES COMMENTAIRES */
                $delPlay = $connexion->prepare('DELETE FROM plays WHERE idvideo=:idplay');
                $delPlay->execute(array(
                    'idplay' => $_POST['idvideo']
                ));
                /* LA SUPPRESSION DES COMMENTAIRE SE FAIS PAR 'ON DELETE CASCADE' DANS LA BDD*/

                header('Location: ../../../admin/gestion_plays.php?action=gerer&succ=62');
                exit();
        } else {
            header('Location: ../../../admin/gestion_plays.php?action=gerer&err=1');
            exit();
        }
    }
    
}
?>