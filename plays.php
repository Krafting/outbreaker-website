<?php
$pageTitle = "Plays";
$isPlay = true;
include('include/init.php');
include('include/header.php');

/* SI ON UPLOAD UN PLAY */
if(isset($_GET['action']) && $_GET['action'] == "add" && connect() && canUploadPlay()) { ?>
    <div class="content">
        <div class="page plays addPlay" style="margin-bottom: 100px;">
        <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            }
        ?>
            <h1>Vous pouvez uploader un nouveau Play !</h1>
            <hr style="margin-bottom: 30px;">
            <form action="include/forms/addPlay.php" method="POST" enctype="multipart/form-data">
            <div id="form" style="max-width: 60%">
                <p>Fichier vidéo de votre Play  (Taille max. <?php echo $maxVideoSize / 1000000 ;?>MB)</p>
                <input type="file" required name="play">
                <p>Miniature de votre Play  (Taille max. <?php echo $maxImageSize / 1000000 ;?>MB)</p>
                <input type="file" required name="playThumb">
                <p>Titre de votre Play</p>
                <input type="text" required name="titrePlay" placeholder="Titre de votre Play">
                <p>Description de votre Play</p>
                <textarea name="descPlay" placeholder="Description de votre Play"></textarea>
                <p>Catégorie de votre Play</p>
                <select name="catPlay" required>
                    <?php 
                    /* ON SELECTIONNE TOUTE LES CATÉGORIES POSSIBLE DES VIDÉOS */
                    $selectAllCategorie = $connexion->prepare('SELECT * FROM typevideo');
                    $selectAllCategorie->execute();
                    $categories = $selectAllCategorie->fetchAll();
                    foreach($categories as $categorie) {
                        echo "<option value='".$categorie['idtypevideo']."'>".$categorie['nomtypevideo']."</option>";
                    }
                    ?>
                </select>

                <button name="sendPlay" class="btn" type="submit">Envoyer mon Play !</button>
            </div>
            </form>
        </div>
    </div>

<?php /* SI ON EST DANS LA GESTION DES PLAYS */
} elseif(isset($_GET['action']) && $_GET['action'] == "gerer" && connect() && canUploadPlay() && !isset($_GET['edit']))  { 
    $selectAllPlays = $connexion->prepare('SELECT * FROM plays WHERE refuser=:iduser');
    $selectAllPlays->execute(array('iduser' => $_SESSION['id']));  
    $allPlay = $selectAllPlays->fetchAll();
    $count = count($allPlay);
    ?>
    <div class="content">
        <div class="page plays editPlay" style="margin-bottom: 100px;">
            <?php 
                if(isset($_GET['err']) OR isset($_GET['succ'])) {
                    if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                        $idMsg = $_GET['err'];
                        echo getMessage($idMsg);
                    }
                    if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                        $idMsg = $_GET['succ'];
                        echo getMessage($idMsg);
                    }
                }
            ?><br>
            <table>
                <tbody>
                    <tr>
                        <td>Titre du Play</td>
                        <td>Nombre de vues</td>
                        <td style="width: 10%;">Action</td>
                    </tr>  
                    <?php foreach($allPlay as $playList) { ?>
                    <tr>
                        <td><?php echo $playList['titrevideo']; ?> <a href="watch.php?uuid=<?php echo $playList['idvideo']; ?>" target="blank">Vers le play</a></td>
                        <td><?php echo findNumViews($playList['idvideo']); ?> vues</td>
                        <td><a href="?action=gerer&edit=<?php echo $playList['idvideo']; ?>">Éditer</a></td>
                    </tr>
                    <?php } if($count == 0) { echo '<tr><td colspan="3"><div class="infoMessage">Vous n\'avez uploader aucuns Play.</div></td></tr>'; } ?>
                </tbody>
            </table>
        </div>
    </div>

<?php 
/* SI ON EDIT UN PLAY */
} elseif(isset($_GET['action']) && $_GET['action'] == "gerer" && connect() && canUploadPlay() && 
isset($_GET['edit']) && is_numeric($_GET['edit']) && !empty($_GET['edit'])) { 
    
    
    /* ON CHECK SI LE PLAY EXISTE */
    $findPlay = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE idvideo=:idvideo');
    $findPlay->execute(array(
        'idvideo' => $_GET['edit']
    ));
    $check = $findPlay->fetch();

    if($check[0] > 0) {
        /* ON SELECTIONNE LE PLAYS DEmANDER */
        $selectPlay = $connexion->prepare('SELECT * FROM plays WHERE idvideo=:idvideo');
        $selectPlay->execute(array(
            'idvideo' => $_GET['edit']
        ));
        $playEdit = $selectPlay->fetch();
?>

<div class="content">
        <div class="page plays addPlay" style="margin-bottom: 100px;">
<?php if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1>Edition de votre Play</h1>
            <hr style="margin-bottom: 30px;">
            <form action="include/forms/editPlay.php" method="POST" enctype="multipart/form-data">
            <div id="form" style="max-width: 60%">
                <input type="hidden" value="<?php echo $_GET['edit']; ?>" name="idvideo">
                <img style="width: 100%" height="355px" src="upload/thumbs/<?php echo $_SESSION['id'].'/'.$playEdit['miniature']; ?>"></img>
                <p>Miniature de votre Play</p>
                <input type="file" name="playThumb">
                <p>Titre de votre Play</p>
                <input type="text" required name="titrePlay" placeholder="Titre de votre Play" value="<?php echo $playEdit['titrevideo']; ?>">
                <p>Description de votre Play</p>
                <textarea name="descPlay" placeholder="Description de votre Play"><?php echo $playEdit['descriptionvideo']; ?></textarea>
                <p>Catégorie de votre Play</p>
                <select name="catPlay">
                    <?php 
                    /* ON SELECTIONNE TOUTE LES CATÉGORIES POSSIBLE DES VIDÉOS */
                    $selectAllCategorie = $connexion->prepare('SELECT * FROM typevideo');
                    $selectAllCategorie->execute();
                    $categories = $selectAllCategorie->fetchAll();
                    foreach($categories as $categorie) {
                        echo "<option value='".$categorie['idtypevideo']."'"; 
                        if($categorie['idtypevideo'] == $playEdit['reftypevideo']) { echo "selected"; }
                        echo ">".$categorie['nomtypevideo']."</option>";
                    }
                    ?>
                </select>

                <button name="sendPlay" class="btn" type="submit">Envoyer mon Play !</button> 
                <a class='btn' style="margin-left: 25px;" onclick='openModal("delPlay")' href='#'>Supprimer ce Play</a>
            </div>
            </form>
            <!-- MODAL DE SUPPRESSION -->
            <div id="delPlay" class='modal'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <span class='close'>&times;</span>
                        <h2>Êtes-vous sûr de vous ?</h2>
                    </div>
                        <h1>Êtes vous sur de vouloir supprimer ce Play ?</h1>
                        
                        <form style="padding-bottom: 75px;" action="include/forms/editPlay.php" method="POST" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_GET['edit']; ?>" name="idvideo">
                            <button name="delPlay" class="btn" type="submit">Supprimer mon Play !</button>
                        </form>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php } else { header('Location: plays.php?action=gerer'); exit(); }
/* SINON, ON AFFICHE LA LISTE DES PLAYS */ 
} else {
?>
    <div class="content">
        <div class="page plays">
            <h1>Les Plays de la communauté OutBreaker</h1>
		    <h2>Les plays sont des vidéos courtes montrant un gameplay de jeu vidéo, souvent épic!</h2>
            <div class="flexPlay">

                <?php 
                $videosParPage = 12;
                /* ON COMPTE LE NOMBRE TOTAL DE PLAYS */
                $nbPlayTotal = $connexion->prepare('SELECT count(*) FROM plays');
                $nbPlayTotal->execute();
                $nbr_selectVideo = $nbPlayTotal->fetch();
                $nbrPageVid = ceil($nbr_selectVideo[0] / $videosParPage);
                
                /* ON SELECTIONNE LA PAGE, SI Y'A LE GET OU PAS */
                if(!empty($_GET['p']) and is_numeric($_GET['p'])) {
                    $page = secure($_GET['p']);
                    if($page > $nbrPageVid) {
                        $page = $nbrPageVid;
                    }
                } else {
                    $page = 1;
                }
                $firstVids = ($page-1)*$videosParPage;

                /* ON RECHERCHE TOUT LES PLAYS DE LA PAGE ACTUELLE */
                $sql = 'SELECT * FROM plays INNER JOIN users ON refuser=iduser ORDER BY idvideo DESC LIMIT '.$videosParPage.' OFFSET '.$firstVids.';';
                $result = $connexion->prepare($sql);
                $result->execute();
                $result2 = $result->fetchAll();
                
                foreach($result2 as $play) {
                ?>
                <div class="previewPlay">
                    <a href="watch.php?uuid=<?php echo $play['idvideo']; ?>">
                        <img src="upload/thumbs/<?php echo $play['refuser'].'/'.$play['miniature']; ?>"></img>
                    </a>
                    <div class="userBlock">
                        <a href="profiles.php?id=<?php echo $play['iduser']; ?>">
                            <img src="upload/profiles/<?php echo findProfilePic($play['iduser']); ?>"></img>
                        </a>
                        <p>
                            <a title="<?php echo $play['titrevideo']; ?>" href="watch.php?uuid=<?php echo $play['idvideo']; ?>"><b><?php echo truncage(0, $play['titrevideo'], 28); ?></b></a>
                            <br><a href="profiles.php?id=<?php echo $play['iduser']; ?>"><?php echo $play['pseudo']; ?></a> - <?php echo findNumViews($play['idvideo']); ?> vues
                        </p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="pagination">
                <span class="pageB infos">PAGES</span>
                <?php for($i = 1; $i <= $nbrPageVid; $i++) {
                        if($i == $page) {
                            echo '<span class="pageB active">'.$i.'</span>';
                        } else {
                            echo '<a href="?p='.$i.'" class="pageB">'.$i.'</a>';
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>

<?php }
include('include/footer.php');
?>
