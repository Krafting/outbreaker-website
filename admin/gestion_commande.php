<?php
$adminPage = true; $titrePage = "Gestion des commandes";
include('../include/init.php');
include('header.php');

if (isset($_GET['idcommande']) && !empty($_GET['idcommande']) && is_numeric($_GET['idcommande'])) {

	$sql_id_exist = $connexion->prepare("SELECT count(*) FROM commandes where idcommande=:idcommande ;");
	$sql_id_exist->execute(array(
		'idcommande'=> $_GET['idcommande']
	));
	$sql_id_exist=$sql_id_exist->fetch();


	if ($sql_id_exist[0]>0) {

       
      	if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }             
            }
	   

	   

	    ?><h1>Modifier la commande</h1>
		<hr style="margin-bottom: 30px;"><?php 

        $sql_infocommandeT = $connexion->prepare("SELECT * from commandes inner join users on refuser=iduser inner join etats on refetat=idetat where idcommande=:idcommande;");
    	$sql_infocommandeT->execute(array(
    		'idcommande'=> $_GET['idcommande']
    		 ));
        $sql_infocommande = $sql_infocommandeT->fetch();
        	$extra=$sql_infocommande['extras'];
        	$etat=$sql_infocommande['nometat'];
        	$idetat=$sql_infocommande['idetat'];
        	$ville=$sql_infocommande['villecommande'];
        	$codepostal=$sql_infocommande['codepostal'];
        	$departement=$sql_infocommande['depcommande'];
        	$adresse=$sql_infocommande['addresscommande'];
        	$nom=$sql_infocommande['nomcommande'];
        	$prenom=$sql_infocommande['prenomcommande'];
        	$tel=$sql_infocommande['telcommande'];
        	$idcommande=$sql_infocommande['idcommande'];
?>


			<div class="flexBlock">
            	<div class="flexContent border-first">
					<form method="post" action="../include/forms/admin/setCommande.php" enctype="multipart/form-data">	
						<input type="hidden" name="idcommande" value="<?php echo $idcommande; ?>">

						<p>Modification de l'extra</p>
						<input type="text" name="setextras" placeholder="Extras" value="<?php echo $extra; ?>">

						<p>Modification du nom du commanditaire</p>
						<input type="text" name="setnomcommande" placeholder="Nom" value="<?php echo $nom; ?>">

						<p>Modification du prenom du commanditaire</p>
						<input type="text" name="setprenomcommande" placeholder="Prenom" value="<?php echo $prenom; ?>">

						<p>Modification du téléphone du commanditaire</p>
						<input type="text" name="settelcommande" placeholder="N°Tel" value="<?php echo $tel; ?>">

						<p>Choisissez le nouvelle etat de la commande</p>
						<select name="etat"> 
                            <?php $sql_etat = $connexion->prepare("SELECT * from etats");
                                        $sql_etat->execute();
                                        $sql_etat=$sql_etat->fetchall();
                                            foreach ($sql_etat as $ligne) { ?>
                                            <option <?php if ($idetat==$ligne['idetat']) {echo 'selected=""';} ?> value="<?php echo $ligne['idetat'];?>"> <?php echo $ligne['nometat'];?></option> <?php } ?>

                        </select>               

				</div>
				<div class="flexContent border-first">
						<p>Modification de l'adresse de livraison</p>
						<input type="text" name="setaddresscommande" placeholder="Adresse" value="<?php echo $adresse; ?>">

						<p>Modification du departement du commanditaire</p>
						<input type="text" name="setdepcommande" placeholder="Département" value="<?php echo $departement; ?>">

						<p>Modification du code postal du commanditaire</p>
						<input type="text" name="setcodepostal" placeholder="Code postal" value="<?php echo $codepostal; ?>">

						<p>Modification de la ville du commanditaire</p>
						<input type="text" name="setvillecommande" placeholder="ville" value="<?php echo $ville; ?>">
						
						<div class="sendButton">
							<button class="btn" type="submit" name="setCommande">Modifier commande</button>
						</div>
				</div>
			</div>
			 
			</form>
				<br>
                <hr>
                <br>
        	<table>
        		<tr>
                    <td style="width: 5%;">Quantité</td>
            		<td>Article</td>
            		<td>Prix</td>
            		<td>Action</td>
            	</tr>
            			<?php
							$sql_article_commande = $connexion->prepare("SELECT * from commander inner join articles on refarticle=idarticle where refcommande=:idcommande;");
							$sql_article_commande->execute(array(
								'idcommande' => $_GET['idcommande'] 
							));
						    $sql_article_commande=$sql_article_commande->fetchall();
						    foreach ($sql_article_commande as $ligne) {
						    	$nomarticle=$ligne['nomarticle'];
						    	$idarticle=$ligne['refarticle'];
                                $qte=$ligne['qtecommande'];
						    	$prix=$ligne['prixvente'];
                                echo "<tr><td> ".$qte." x</td>";
						    	echo "<td> ".$nomarticle."</td>";
						    	echo "<td> ".$prix*$qte." € <small>(".$prix."€ l'unité) </small> </td>";
						    	echo"<td><a class=' ' name='article' href='./gestion_article.php?idarticle=".$idarticle."'>Voir l'article</a></td></tr>";
							}


?>
			</table>
            


<?php
 } else {  
 	header('Location: ./gestion_categorie.php?err=37');
	 exit();
 }
} else {  

   if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            }
 	
?>
 
<h1>Gestion des commandes</h1>
    <hr style="margin-bottom: 30px;">
<br>
<table>
    	<tr>
    		<td>#</td>
    		<td>Etat</td>
    		<td>Addresse de livraison</td>
    		<td>Prix</td>
    		<td>Action</td>
    	</tr>
<?php
		$sql_idcommande = $connexion->prepare("SELECT * from commandes inner join users on refuser=iduser inner join etats on refetat=idetat order by idcommande;");
    	$sql_idcommande->execute();
        $sql_idcommande=$sql_idcommande->fetchall();
        foreach ($sql_idcommande as $ligne) {
        	$idcommande=$ligne['idcommande'];
        	$etat=$ligne['nometat'];
        	$ville=$ligne['villecommande'];
        	$codepostal=$ligne['codepostal'];
        	$departement=$ligne['depcommande'];
        	$addresse=$ligne['addresscommande'];
        	$prix=$ligne['prixtotal'];
        	$user=$ligne['pseudo'];
        	$iduser=$ligne['iduser'];

        	echo "<tr><td> ".$idcommande."</td>";
        	echo "<td> ".$etat."</td>";
        	echo "<td style='text-align: left;'>".$ville." (".$codepostal.") <br>
                      ".$departement." <br>
                      ".$addresse." 
                    </td>";
        	echo "<td> ".$prix."€</td>";
        	echo"<td><a class=' ' href='?idcommande=".$ligne['idcommande']."' name='modifier'>Modifier</a>
        	<a class=' ' name='profiluser' href='./users.php?id=".$iduser."'>Voir l'utilisateur</a></td>";
        }

?>

</table>
<?php } ?>
