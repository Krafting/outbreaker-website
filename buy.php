<?php
$isBoutique = true;
$needConnect = true;
$pageTitle = "Votre commande";
include('include/init.php');
include('include/header.php');

?>

    <div class="content">
        <!-- CHANGER LA CLASSE DE CETTE DIV : -->
        <div class="page">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                }
                echo getMessage($idMsg);
            } ?>
            <h1>Récapitulatif de votre commande</h1>
            <table>
                <tr>
                    <td style="width: 5%">Quantité</td>
                    <td>Article(s)</td>
                    <td>Prix (En €)</td>
                </tr>
                <?php 
                /* ON AFFICHE LES ARTICLES DU PANIER DE L'UTILISATEUR */ 
                $selectPanier = $connexion->prepare('SELECT * FROM panier2 INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                $selectPanier->execute(array(
                    'id' => $_SESSION['id']
                ));
                $articles = $selectPanier->fetchAll();

                foreach($articles as $article) {
                    echo "  <tr>
                                <td>".$article['qte']." x</td>
                                <td>".$article['nomarticle']." <a href='article.php?id=".$article['idarticle']."' target='blank'>Lien</a></td>
                                <td>".$article['prixvente']*$article['qte'] ." € <small style='margin-left: 5px'> (".$article['prixvente']."€ l'unité)</small></td>
                            </tr>";
                }
                /* SI ON A PAS D'ARTICLES DANS LE PANIER */
                if(count($articles) == 0) {
                    echo "  <tr>
                                <td colspan='3'>
                                    Vous n'avez pas d'articles dans votre panier. Vous ne pouvez pas passer de commande.
                                </td>
                            </tr>";
                /* SINON : ON A DES ARTICLES ET ON AFFICHE LE MONTANT TOTAL */ 
                } else {
                    $result = $connexion->prepare('SELECT sum(prixvente*qte) FROM panier2 INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                    $result->execute(array(
                        'id' => $_SESSION['id']
                    ));
                    $result = $result->fetch();
                    echo "<tr>
                                <td colspan=2 style='text-align: right;'> 
                                    Prix d'achat total de vos articles.
                                </td>
                                <td>".round($result[0], 2)." €</td>
                            </tr>";
                }
                ?>
            </table>
            <br>
            <br>
            <br>
            <form method="post" action="include/forms/passerCommande.php">
                <h2>Informations de votre commande</h2>
                <small>Votre adresse de livraison sera la même que votre adresse de facturation</small><br>
                <small>les champs marqués par par une couleur <span style="color: red;">rouge</span> sont obligatoires ou mal formattés.</small>
                <br>
                <br>
                <br>
                <div class="flexBlock">
                    <div class="flexContent border-first">
                        <h2>Coordonnés bancaire</h2>
                        <p>Code de carte bancaire (*)</p>  
                        <input type="text" name="codebancaire" pattern="^[0-9]{5,}$" required="" placeholder="Code de carte bancaire">
                        <p>Date d'expiration (*)</p>  
                        <input type="text" maxlength="2" pattern="^[0-9]{2}$" style="width: 35%; display: inline-block;margin-bottom: 15px; margin-right: 19px;" name="moisCarte" required="" placeholder="Mois">
                        <small style="font-size: 25px;">/</small><input type="text" pattern="^[0-9]{2}$" maxlength="2" style="width: 35%;margin-bottom: 15px; display: inline-block; margin-left: 19px;" name="AnneeCarte" required="" placeholder="Année">
                        <p>Cryptogramme (*)</p>  
                        <input type="text" maxlength="3" pattern="^[0-9]{3}$" required="" name="cryptoCarte" placeholder="Le cryptogramme de votre carte">
                        
                    </div>
                    <div class="flexContent">
                        <h2>Informations de commande </h2>
                        <p>Nom (*)</p>  
                        <input type="text" name="nomCommande" required="" placeholder="Votre nom de famille">
                        <p>Prénom(s) (*)</p>  
                        <input type="text" name="prenomCommande" required="" placeholder="Votre ou vos prénom(s)">
                        <p>Numéro de téléphone </p>  
                        <input type="text" name="telCommande" placeholder="Numéro de téléphone">
                        <br>
                        <br>
                        <p>Adresse de Livraison (*)</p>  
                        <input type="text" name="addrLivraison" required="" placeholder="Adresse de livraison">
                        <p>Département (*)</p>  
                        <input type="text" name="depLivraison" required="" placeholder="Département de livraison">
                        <p>Ville (*)</p>  
                        <input type="text" name="villeLivraison" required="" placeholder="Ville de livraison">
                        <p>Code Postal (*)</p>  
                        <input type="text" name="codePosLivraison" pattern="^[0-9- ]+$" required="" placeholder="Code postal de livraison">

                    </div>
                </div>
                <br>
                <br>
                <div class="sendButton">
                    <button type="submit" name="passerCmd" class="btn">Passer commande !</button>
                </div>
            </form>
            <br>
            <br>
            <br>
        </div>
    </div>

<?php
include('include/footer.php');
?>
