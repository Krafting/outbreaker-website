<?php 
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}

/* Si on appuie sue le bouton : */
if (isset($_POST['setCommande'])) {
	/* On regarde si la variable nom existe bien et si elle n'est pas vide de même pour d'autre condition */
	if (isset($_POST['setnomcommande']) && !empty($_POST['setnomcommande']) 
		&& isset($_POST['setprenomcommande']) && !empty($_POST['setprenomcommande'])
		&& isset($_POST['setaddresscommande']) && !empty($_POST['setaddresscommande'])
		&& isset($_POST['setdepcommande']) && !empty($_POST['setdepcommande'])
		&& isset($_POST['setcodepostal']) && !empty($_POST['setcodepostal'])
		&& isset($_POST['setvillecommande']) && !empty($_POST['setvillecommande'])
		&& isset($_POST['etat']) && !empty($_POST['etat'])) {
		
				$sql_setcommande=$connexion->prepare("UPDATE commandes set 
					addresscommande=:adresse,
					extras=:extras,
					nomcommande=:nom,
					prenomcommande=:prenom,
					telcommande=:tel,
					depcommande=:departement,
					codepostal=:codepostal,
					villecommande=:ville,
					refetat=:etat
					where idcommande=:idcommande;");
				$sql_setcommande->execute(array(
						'adresse' => $_POST['setaddresscommande'],
						'extras' => $_POST['setextras'],
						'nom' => $_POST['setnomcommande'],
						'prenom' => $_POST['setprenomcommande'],
						'tel' => $_POST['settelcommande'],
						'departement' => $_POST['setdepcommande'],
						'codepostal' => $_POST['setcodepostal'],
						'ville' => $_POST['setvillecommande'],
						'etat' => $_POST['etat'],
						'idcommande' => $_POST['idcommande']
				));
				header('Location: ../../../admin/gestion_commande.php?succ=73&idcommande='.$_POST['idcommande'].'');
				exit();
			} else {
				header('Location: ../../../admin/gestion_commande.php?err=1&idcommande='.$_POST['idcommande'].'');
				exit(); 
			}
	} else {
		header('Location: ../../../admin/gestion_commande.php'); 
		exit();
	}
	