<?php
$adminPage = true; $titrePage = "Gestion des catégories";
include('../include/init.php');
include('header.php');
?>
<?php

/* Si on à l'id d'un article dans le liens (en method get), alors on affiche la page de ce type */
if (isset($_GET['idtype']) && !empty($_GET['idtype']) && is_numeric($_GET['idtype'])) {

	$sql_id_exist = $connexion->prepare("SELECT count(*) FROM typearticle where idtype=:idtype ;");
	$sql_id_exist->execute(array(
		'idtype'=> $_GET['idtype']
	));
	$sql_id_exist=$sql_id_exist->fetch();

	if ($sql_id_exist[0]>0) {

      	if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }             
            }

	    ?><h1>Changer la categorie <?php 

        $sql_nomcategorie = $connexion->prepare("SELECT * from typearticle where idtype=:idtype;");
        $sql_nomcategorie->execute(array(
        	'idtype'=> $_GET['idtype']
        ));

     	$sql_nomcategorie=$sql_nomcategorie->fetch();
     	$nomtype=$sql_nomcategorie['nomtype'];
     	$idtype=$sql_nomcategorie['idtype'];

        ?></h1>
        <hr style="margin-bottom: 30px;">

        <div class="flexBlock">
            <div class="flexContent border-first">
                <h2>Changer le nom de la catégorie</h2>
                <form method="post" action="../include/forms/admin/setCategorie.php">         
                    <div id="form">
                        <p>Entrée un nouveau nom pour la catégorie : </p>  
                        <input type="text" name="nom_categorie" value="<?php echo $nomtype; ?>" placeholder="Nouveau nom de la categorie">
                        <input type="hidden" name="idtype" value="<?php echo $idtype; ?>">
                        <div class="sendButton">
                            <button type="submit" name="changeNomCategorie" class="btn">Changer le nom</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="flexContent">
                <h2>Supprimer la catégorie</h2>
                <form method="post"  action="../include/forms/admin/setCategorie.php">         
                    <div id="form">          
                        <div class="sendButton">
                            <button type="submit" name="delete_categorie" class="btn">Supprimer</button>
                   			<input type="hidden" name="idtype_del" value="<?php echo $idtype; ?>">
                        </div>
                    </div>

<?php
 } else {  
 	header('Location: ./gestion_categorie.php?err=37');
     exit();
 }
} else {  

   if(isset($_GET['err']) OR isset($_GET['succ'])) {
        if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
            $idMsg = $_GET['err'];
            echo getMessage($idMsg);
        }
        if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
            $idMsg = $_GET['succ'];
            echo getMessage($idMsg);
        }
    }
 	
?>
<h1>Catégorie d'articles</h1>
<hr style="margin-bottom: 30px;">
    <table>
        <tr>
            <td colspan="2">
                <form method="post"  action="../include/forms/admin/setCategorie.php">
                    <div id="form">
                        <input type="text" placeholder="Nom de la catégorie" style="width: 50%;display: inline-block;margin: 25px;" name="new_categorie" >         
                            <div class="sendButton" style="vertical-align: top;">
                                <button type="submit" name="add_categorie" class="btn">ajouter une categorie</button>
                            </div>
                    </div>	
                </form>
            </td>
        </tr>
    	<tr style="font-weight: bold;">
    		<td>Nom de la catégorie</td>
    		<td style="width: 30%;">Action</td>
    	</tr>
<?php
		$sql_type = $connexion->prepare("SELECT * from typearticle order by idtype;");
    	$sql_type->execute();
        $sql_type=$sql_type->fetchall();
        foreach ($sql_type as $ligne) {
        	$nomtype=$ligne['nomtype'];
        	echo "<tr><td> ".$nomtype."</td>"; 
        	echo"<td><a class=' ' href='?idtype=".$ligne['idtype']."' name='modifier'>Modifier</a>"; 
        }
?>
</table>
<?php } ?>