<?php 
include("../init.php");


/* hCaptcha Code */
$data = array(
	'secret' => "",
	'response' => $_POST['h-captcha-response']
);$verify = curl_init();
curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
curl_setopt($verify, CURLOPT_POST, true);
curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($verify);
$responseData = json_decode($response);


/* SI LE FORMULAIRE EST ENVOYÉ ET N'EST PAS VIDE */
if( isset($_POST['register']) 
    AND !empty($_POST['pseudo'])
    AND !empty($_POST['mail'])
    AND !empty($_POST['pass1']) 
    AND !empty($_POST['pass2']) 
	AND $responseData->success) {

	/* VARIABLES UTILES */
	$pseudo=strtolower(secure($_POST['pseudo']));
	$mail=secure($_POST['mail']);
	$pass1=hash("sha256",$_POST['pass1']);
	$pass2=hash("sha256",$_POST['pass2']);

	/* SI LE PSEUDO EST DANS LE BON FORMAT */
	if(preg_match_all($regexpseudo, $pseudo)) {	
		$verifpseudo = $connexion->prepare('SELECT COUNT(*) FROM users WHERE pseudo=:pseudo');
		$verifpseudo->execute(array(
			'pseudo' => $pseudo
		));
		$verif = $verifpseudo->fetch();
		/* ON VERIFIE SI LE PSEUDO N'EXISTE PAS */
		if ($verif[0] == 0) {
			/* SI LEMAIL EST DANS LE BON FORMAT */
			if(preg_match_all($regexmail, $mail)) {
				$verifmail = $connexion->prepare('SELECT COUNT(*) FROM users WHERE mail=:mail');
				$verifmail->execute(array(
					'mail' => $mail
				));
				$verif = $verifmail->fetch();

				/* SI L'EMAIL N'EST PAS DEJA UTILISÉE */
				if ($verif[0] == 0) {
					/* SI LES DEUX MOTS DE PASSE CORRESPONDENT, ALORS ON CRÉE L'UTILISATEUR */
					if ($pass1 == $pass2) {
							$adduser = $connexion->prepare('INSERT INTO users (pseudo,pass,mail, dateinscri) VALUES (:pseudo,:pass,:mail, :dateinscri);');
							$adduser->execute(array(
								'pseudo' => $pseudo,
								'pass' => $pass1,
								'mail' => $mail,
								'dateinscri' => date('Y-m-d')
							));
							/* SI L'UTILISATEUR REVIENS DE LOINS ET UTILISE LA REDIRECTION VERS UNE AUTRE PAGE */
							if(!empty($_POST['redir'])) {
								header('Location: ../../login.php?succ=27&redir='.secure($_POST['redir']).'.php');
								exit();
							} else {
								header('Location: ../../login.php?succ=27');
								exit();
							}
					} else {
						header('Location: ../../register.php?err=5');
						exit();
					} 
				} else {
					header('Location: ../../register.php?err=22');	
					exit();
				} 
			}else {
				header('Location: ../../register.php?err=4');
				exit();
			} 
		} else {
			header('Location: ../../register.php?err=23');
			exit();
		}	 
   	} else {
     	header('Location: ../../register.php?err=24');
		exit();
    }
} else {
	header('Location: ../../register.php?err=1');
    exit();
}




?>