<?php
$pageTitle = "Mon Compte";
$needConnect = true;
include('include/init.php');
include('include/header.php');
?>
<style>


</style>
    <div class="content">
        <div class="page" style='height: 60px;'>
            <div class="accountButtons">
                <a  style="float: right;top: 0;" href="?logout" class='btn'>Se deconnecter</a>
                <?php if(isAdmin()) { echo "<a   style='float: right;top: 0; right: 20px;' class='btn' href='admin/'>Espace Admin</a>"; } ?>
                <a  style="float: right;top: 0;right: 40px" href="wishlist.php?id=<?php echo $_SESSION['id']; ?>" class='btn'>Wishlist</a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="page user" style="margin-top: 20px;">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?>
            <h1>Changer votre profil public</h1>

            <form method="post"  action="include/forms/changeProfile.php" enctype="multipart/form-data">         
            <div class="flexBlock">
                <div class="flexContent border-first">
                <h2>Aperçu de votre profil <br>(<a style="color: var(--blue)" href="profiles.php?id=<?php echo $_SESSION['id']; ?>">Voir ma page de profile</a>)</h2>
                    <div class="block" style="display:block; height: 150px;">
                        <img src="upload/profiles/<?php echo findProfilePic($_SESSION['id']); ?>" class="image-profile">
                        <h1 class="profile"><?php echo $_SESSION['pseudo']; ?></h1><br><br>
                        <h1 class="profile" style="font-style: italic;"><?php echo $_SESSION['nomrang']; ?></h1><br><br>
                        <p style="font-style: italic; text-align: left;">Inscrit le <?php echo dateInscri($_SESSION['id']); ?></p><br>
                    </div><br>
                    <h1 class="profile">À mon propos</h1>
                    <br>
                    <br>
                    <p style='text-align: justify'><?php echo formatTexte($_SESSION['about']); ?></p>
                </div>
                <div class="flexContent">
                    <div id="form">
                        <h2>Changer votre profil</h2>
                        <p>Votre indentifiant de connexion </p>  
                        <input disabled type="text" name="pseudo" value="<?php echo $_SESSION['pseudo']; ?>" placeholder="Votre pseudonyme">
                        <p>À propos de vous</p>  
                        <textarea style="margin-bottom: 0;" name="about" onfocus="countChar()" onkeyup="countChar()" onkeydown="countChar()" id="textareaCount" maxlength="2000" placeholder="À propos de vous, ce texte est afficher sur votre page de profile"><?php echo $_SESSION['about']; ?></textarea>
                        <small id="count" style="display: block; text-align: right;height: 5px"></small>
                        <p>Votre photo de profil (Taille max. <?php echo $maxImageSize / 1000000 ;?>MB)</p>  
                        <input type="file" name="newPicture">
                        <a href="#" onclick="openModal('modalPictureOld')">Selectionner une ancienne image.</a>

                        <div class="sendButton">
                            <button type="submit" name="changeProfile" class="btn">Changer le profil</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            </form>

            <div id="modalPictureOld" class="modal">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close">&times;</span>
                        <h2>Utiliser une ancienne photo de profil</h2>
                    </div>
                    <p style="max-height: 330px;overflow: auto;">
                    <style>
                    .testborder:focus > img {
                        border: 5px solid var(--blue); 
                    }
                    </style>

                        <?php $listeImages = glob("upload/profiles/".$_SESSION['id']."/*"); 
                        $counter = 0;
                        foreach($listeImages as $images) { 
                            $arr = explode('/', $images);
                            $counter++;?>
                                <a class="testborder" href="#" tabindex=0>
                                <img onclick="selectPicture('<?php echo $arr['4']; ?>')" id="" src="<?php echo $images; ?>" class="modal-image">
                            </a>
                        </label>
                        <?php } 
                        if($counter > 0) { } else { ?>
                            <div class="successMessage">
                                Vous n'avez pas encore uploader de photo de profil.
                            </div>
                        <?php } ?>
                        <div class="sendButton">
                            <form method="POST" action="include/forms/selectOldPic.php">
                                <input type="hidden" name="selectImage" id="selectImageName">
                                <button type="submit" name="selectPicturePost" class="btn">Selectionner cette image</button>
                            </form>
                        </div>
                        <div class="sendButton" style="height: 100px;">
                            <form method="POST" action="include/forms/selectOldPic.php">
                                <input type="hidden" name="deleteImage" id="selectImageNameDelete">
                                <button type="submit" name="deletePicturePost" class="btn">Supprimer cette image</button>
                            </form>
                        </div>
                    </p>
                </div>
            </div>


            <h1>Changer vos informations personelles</h1>
            <div class="flexBlock">
                <div class="flexContent border-first">
                    <h2>Changer votre compte</h2>
                    <form method="post" action="include/forms/changeAccount.php">         
                        <div id="form">
                            <p>Votre adresse e-mail </p>  
                            <input type="text" name="mail" value="<?php echo $_SESSION['mail']; ?>" placeholder="Votre addresse mail">
                            <input type="checkbox" name="allowComment" id="comment" value="1" <?php if($_SESSION['allowcommentaire'] == 1) { echo "checked"; } ?>><label for="comment"> Autorisé les commentaires sur mon profil public</label>
                            <div class="sendButton">
                                <button type="submit" name="changeAccount" class="btn">Changer le compte</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="flexContent">
                    <h2>Changer de mot de passe</h2>
                    <form method="post" action="include/forms/changePassword.php">         
                        <div id="form">
                            <p>Votre ancien mot de passe </p>  
                            <input type="password" name="old_pass" placeholder="Votre ancien mot de passe">
                            <p>Votre nouveau mot de passe </p> 
                            <input type="password" name="new_pass" placeholder="Votre nouveau mot de passe">
                            <p>Confirmer votre nouveau mot de passe </p> 
                            <input type="password" name="new_pass_conf" placeholder="Confirmer votre nouveau mot de passe">

                            <div class="sendButton">
                                <button type="submit" name="changePass" class="btn">Changer le mot de passe</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <h1>Liste de vos commandes sur la boutique</h1>
            <?php 
                /* ON SELECTIONNE LES COMMANDES DE L'UTILISATEUR CONNECTÉS */
                $selectCommandes = $connexion->prepare('SELECT * FROM COMMANDES INNER JOIN etats ON idetat=refetat WHERE refuser=:id;');
                $selectCommandes->execute(array(
                    'id' => $_SESSION['id']
                ));
                $result = $selectCommandes->fetchAll();
                $nbCommande = 0
            ?>
            <div>
                <table>
                    <tr>
                        <td style="width: 150px;">#ID Commande</td>
                        <td>État</td>
                        <td>Montant Total</td>
                        <td>Détails</td>
                    </tr>
                    <?php foreach($result as $commande) { 
                        $nbCommande =$nbCommande+ 1;?>                    <tr>
                        <td>#<?php echo $commande['idcommande']; ?></td>
                        <td><?php echo $commande['nometat']; ?></td>
                        <td><?php echo round($commande['prixtotal'], 2); ?> €</td>
                        <td><a href="commandes.php?id=<?php echo $commande['idcommande']; ?>">Details</a></td>    
                    </tr>
                <?php } if($nbCommande == 0) { ?>
                    <tr>
                        <td colspan="4"><div class="infoMessage">Vous n'avez passé aucune commande sur la boutique de Outbreaker.</div></td>
                        
                    </tr>
                <?php } ?>
                </table>
            </div>
            <br>
            <br>
            <br>

        </div>
    </div>



<?php
include('include/footer.php');
?>