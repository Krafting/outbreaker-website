<?php 
include('../init.php');
$url = $url."lostPassword.php?reset=";
/* SI ON DEMANDE UN NOUVEAU MOT DE PASSE ON ENVOIE LE LIENS */
if(isset($_POST['sendLink'])) {
    if(!empty($_POST['mail'])) {
        $codePassword = sha1(uniqid());

        /* ON SELECTIONNE L'UTILISATEUR  A QUI L'EMAIL APPARTIENT */
        $selectUser = $connexion->prepare('SELECT iduser, pseudo FROM users WHERE mail=:mail');
        $selectUser->execute(array(
            'mail' => $_POST['mail']
        ));
        $userSelect = $selectUser->fetch();

        /* SI L'UTILISATEUR EXISTE */
        if(isset($userSelect['pseudo'])) {

            /* ON LUI ATTRIBUE UN CODE POUR PERMETTRE LA MISE A JOUR DU MOT DE PASSE (ainsi qu'un timestamp) */
            $result = $connexion->prepare('UPDATE users set resetlink=:resetlink, resetdate=:resetdate WHERE iduser=:id;');
            $result->execute(array(
                'resetlink' => $codePassword,
                'resetdate' => time(),
                'id' => $userSelect['iduser']
            ));

            /* ON ENVOIE LE MAIL A L'UTILISATEUR */
            $from = "noreply@outbreaker.net"; // Envoyeur
            $to = $_POST['mail'];     // Destinataire
            $subject = "Réinitialisation de votre mot de passe"; // Sujet
    
            // message
            $message = '<html>
                <head>
                    <title>Réinitialisation de votre mot de passe</title>
                </head>
                <body>
                    <p>Pour réinitialiser votre mot de passe, suivez ce lien. !</p>
                    <a href="'.$url.$codePassword.'">'.$url.$codePassword.'</a><br>
                    <p>Ce lien expirera dans 2 heures.</p>
                </body>
                </html>';
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type: text/html; charset=UTF-8". "\r\n";
            $headers .= "To: ".$to. "\r\n";
            $headers .= "From: ".$from. "\r\n";
            
            // Envoi

            if (mail($to, "Reinitialisation de votre mot de passe", $message, $headers)) {
                header('Location: ../../login.php?succ=75');
                exit();
            } else {
                header('Location: ../../lostPassword.php?err=86');
                exit();
            }

        } else {
            header('Location: ../../lostPassword.php?err=88');
            exit();
        }
    } else {
        header('Location: ../../lostPassword.php?err=1');
        exit();
    }

}
    /* ON CHECK SI LE POST EN ENVOYÉ ET N'EST PAS VIDE  ET SI ON EST CONNECTE*/
if(isset($_POST['changeNewPass'])) {
    if(!empty($_POST['new_pass'])
    AND !empty($_POST['new_pass_conf'])) {
        /* VARIABLE DE MOT DE PASSES */
        /* ON HASH LE NOUVEAU PASSWORD */
        $new_pass = hash('sha256',$_POST['new_pass']);
        $new_pass_conf = hash('sha256',$_POST['new_pass_conf']);

        $selectUser = $connexion->prepare('SELECT iduser, resetlink, resetdate FROM users WHERE resetlink=:resetlink AND :secondtime<resetdate AND resetdate<:resetdate ');
        $selectUser->execute(array(
            'resetlink' => $_POST['reset'],
            'resetdate' => time(),
            'secondtime' => time()-7200
        ));
        $user = $selectUser->fetch();
        /* ON CHECK SI LE LIEN A ÉTÉ ENVOYÉ DANS LES DERNIÈRES 2 HEURES */
        if( time()-7200 < $user['resetdate'] AND $user['resetdate'] < time()) {
            /* ON CHECK SI LES DEUX MOT DE PASSE ENTRÉ CORRESPONDENT */
            if ($new_pass == $new_pass_conf) {
                $result = $connexion->prepare('UPDATE users set pass=:pass, resetlink=:resetlink, resetdate=:resetdate WHERE iduser=:id;');
                $result->execute(array(
                    'pass' => $new_pass,
                    'resetlink' => '',
                    'resetdate' => '0',
                    'id' => $user['iduser']
                ));
                header('Location: ../../login.php?succ=10');
                exit();
            } else {
                header('Location: ../../lostPassword.php?err=3&reset='.$_POST['reset']);
                exit();
            }
        } else {
            header('Location: ../../lostPassword.php?err=74');
            exit();
        }
    } else {
        header('Location: ../../lostPassword.php?err=1&reset='.$_POST['reset']);
        exit();
    }
}
   



?>