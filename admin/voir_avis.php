<?php
$adminPage = true; $titrePage = "Avis de l'article";
include('../include/init.php');
include('header.php');

if (isset($_GET['idarticle']) && !empty($_GET['idarticle']) && is_numeric($_GET['idarticle'])) {

	if (isset($_POST['del_avis'])) {

	/* On check si il existe un avis pour cette article avec cette utilisateur */
	$sql_avis_exist=$connexion->prepare("SELECT count(*) FROM avisarticle where idrefarticle=:idarticle;");
	$sql_avis_exist->execute(array(
		'idarticle' => $_GET['idarticle']
	));
	$sql_avis_exist=$sql_avis_exist->fetch();

		if ($sql_avis_exist[0]>0){

			$sql_del_avis=$connexion->prepare("DELETE FROM avisarticle where idrefuser=:iduser and idrefarticle=:idarticle;");
			$sql_del_avis->execute(array(
				'iduser' => $_POST['iduser'],
				'idarticle' => $_GET['idarticle']
			));	
			header('Location:./voir_avis.php?idarticle='.$_GET['idarticle']);
			exit();
		} 
	}
	
	/*Verification de l'idarticle*/
	$sql_id_exist = $connexion->prepare("SELECT count(*) FROM articles where idarticle=:idarticle ;");
	$sql_id_exist->execute(array(
		'idarticle'=> $_GET['idarticle']
	));
	$sql_id_exist=$sql_id_exist->fetch();

	/* Si il existe */
	if ($sql_id_exist[0]>0) {
			if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }             
            }

	
		?><h1>Avis à propos de l'article </h1>
<style>
.avisArticle .profileBlock a { 
    vertical-align: top;
    margin: 40px;
    display: inline-block
}
.avisArticle .profileBlock img {
    height: 100px !important; width: 100px;
}
.avisArticle .flexBlock.oneChild {    
    background: var(--light-grey);
    border-radius: 10px;
}
</style>


		<?php 

		$sql_avis=$connexion->prepare("SELECT * FROM avisarticle INNER JOIN users on idrefuser=iduser WHERE idrefarticle=:idarticle;");
		$sql_avis->execute(array(
			'idarticle' => $_GET['idarticle']
			));
		$sql_avis=$sql_avis->fetchall(); 
		

		foreach ($sql_avis as $ligne) {
			$avis=$ligne['textavis'];
			$iduser=$ligne['idrefuser'];
			$nom=$ligne['pseudo'];
			$note=$ligne['noteavis'];
		?>

		<div class="avisArticle" style="margin-top: 60px;">
            <div style="flex-flow:column; margin-bottom: 30px">
                <div class="flexBlock oneChild">
                    <div class="flexContent">
                        <div class="profileBlock" style="max-height: 100px;margin-top: 7px">
                                     <img style="border-radius: 10px" src="../../upload/profiles/<?php echo findProfilePic($iduser); ?>">
                        </div>
                    </div>
                	<div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                            <a style="margin: 0; text-decoration: none; color: var(--white); cursor: crosshair;" href="profiles.php?id=7"><h2 style="margin:0; font-weight: lighter; "><?php echo  $nom; ?></h2></a>
                            <p style="margin:0; padding-bottom: 15px; font-size: 18px;">Note : <b style="font-weight: bold"><?php echo $note; ?>/ 5</b> étoiles</p>
                            <?php echo $avis; ?>
                    </div>
                    
   	        			<form method="post">
   	        			<input type="hidden" name="iduser" value="<?php echo $iduser; ?>">	
   	        				<div class="flexContent">
	        					<div class="sendButton">
	                				<button type="submit" name="del_avis" class="btn">Supprimer l'avis</button>        
	           				 	</div>
	        				</div>
	   					</form>
                </div>
            </div>
        </div>
        

<?php	}	
	} else {
		header('Location: ./gestion_article.php?err=28');
		exit();
	}
} else {
	header('Location: ./gestion_article.php');
    exit();
}

?>
