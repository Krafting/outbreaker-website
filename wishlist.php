<?php
$pageTitle = "Wishlist";
include('include/init.php');
/* NOTE: wishlist is a fancy term for panier */
/* ON GERE LA WISHLIST QUE SI L'UTILISATEUR EST LE PROPRIÉTAIRE DE LA DITE WISHLIST */
if(connect() AND ($_SESSION['id'] == $_GET['id'])) {
    /* AJOUT D'ARTICLE DANS LA WISHLIST */
    if(isset($_GET['addArticle']) AND is_numeric($_GET['addArticle'])) {

        /* ON CHECK SI L'ARTICLE EXISTE */
        $findArticle = $connexion->prepare('SELECT COUNT(*) FROM articles WHERE idarticle=:idarticle');
        $findArticle->execute(array(
            'idarticle' => $_GET['addArticle']
        ));
        $check = $findArticle->fetch();

        /* SI L'ARTICLE EXISTE, ON L'AJOUTE AU PANIER DE L'UTILISATEUR */
        if($check[0] == 1) {
                $sentToBdd = $connexion->prepare('INSERT INTO wishlist (idrefuser, idrefarticle) VALUES (:refuser, :refarticle)');
                $sentToBdd->execute(array(
                    'refuser' => $_SESSION['id'],
                    'refarticle' => $_GET['addArticle']
                ));
                
                header('Location: ?succ=43&id='.$_SESSION['id']);
                exit();
        } else {
            header('Location: ?err=28&id='.$_SESSION['id']);
            exit();
        }
    }
    /* DELETION D'ARTICLE DANS LE PANIER */
    if(isset($_GET['remArticle']) AND is_numeric($_GET['remArticle'])) {

        /* ON CHECK SI L'ARTICLE EST DANS LA WISHLIST DE L'UTILISATEUR */
        $findArticleWish = $connexion->prepare('SELECT * FROM wishlist WHERE idrefuser=:iduser AND idrefarticle=:article');
        $findArticleWish->execute(array(
            'iduser' => $_SESSION['id'],
            'article' => $_GET['remArticle']
        ));
        $check = count($findArticleWish->fetchAll());

        /* SI L'ARTICLE EST DANS LA WISHLIST DE L'UTILISATEUR ALORS ON LE SUPPRIME */
        if($check == 1) {
            $deleteWishArt = $connexion->prepare('DELETE FROM wishlist WHERE idrefuser=:iduser AND idrefarticle=:article');
            $deleteWishArt->execute(array(
                'iduser' => $_SESSION['id'],
                'article' => $_GET['remArticle']
            ));
            header('Location: ?succ=44&id='.$_SESSION['id']);
        } else {
            header('Location: ?err=45&id='.$_SESSION['id']);
        }
    }
}

include('include/header.php');
if(isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) {

    /* ON CHECK SI L'UTILISATEUR EXISTE */
    $userWishlist = $connexion->prepare('SELECT count(*) FROM users WHERE iduser=:id');
    $userWishlist->execute(array(
        'id' => $_GET['id']
    ));
    $count = $userWishlist->fetch();
    if($count[0] > 0) {
?>

    <div class="content">
        <div class="page">
            <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } 
            /* ON AFFICHE UN MESSAGE DIFFERENT SI C'EST LA WISHLIST DE L'UTILISATEUR CONNECTÉ */
            if(connect() && isset($_SESSION['id']) && $_SESSION['id'] == $_GET['id']) {
                echo "<h1>Votre wishlist</h1>"; ?>
                <h2>Vous pouvez partager le lien de votre wishlist avec vos amis ! 
                    <a href='#' onclick="copy('<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>')" style='color: var(--blue)'>Copier le lien</a></h2>
            <?php  } else {
                // ON CHERCHE LE PSEUDO DE L'UTILISATEUR DE LA WISHLIST
                $userWishlist = $connexion->prepare('SELECT pseudo FROM users WHERE iduser=:id');
                $userWishlist->execute(array(
                    'id' => $_GET['id']
                ));
                $pseudo = $userWishlist->fetch();

                echo "<h1>Wishlist de ".$pseudo['pseudo']."</h1>"; 
            } ?>
		    <table>
                <tr>
                    <td>Article(s)</td>
                    <td>Prix (En €)</td>
                    <td style="width: 10%;">Action</td>
                </tr>
                <?php 
                /* ON AFFICHE LES ARTICLES DU PANIER DE L'UTILISATEUR */ 
                $selectWishList = $connexion->prepare('SELECT * FROM wishlist INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                $selectWishList->execute(array(
                    'id' => $_GET['id']
                ));
                $articles = $selectWishList->fetchAll();

                 
                foreach($articles as $article) {
                    echo "<tr>
                            <td>".$article['nomarticle'];
                            if($article['qtedispo'] > 0) { 
                                echo '<label class="stock yes">En stock</label></td>';
                                } else {
                                echo '<label class="stock no">Rupture</label></td>';
                                }
                            echo "<td>".$article['prixvente']." €</td>";
                                if(connect() && isset($_SESSION['id']) && $_SESSION['id'] == $_GET['id']) {
                                    echo "<td><a href='article.php?id=".$article['idarticle']."'>Lien</a><a href='?remArticle=".$article['idrefarticle']."&id=".$_GET['id']."'>Retirer</a></td>"; 
                                } else {
                                    echo "<td><a href='article.php?id=".$article['idarticle']."'>Lien</a></td>"; 
                                }
                    echo "</tr>";
                }
                /* SI ON A PAS D'ARTICLES DANS LE PANIER */
                if(count($articles) == 0) {
                    echo "<tr><td colspan='3'>";
                            if(connect() && isset($_SESSION['id']) && $_SESSION['id'] == $_GET['id']) {
                                echo "Vous n'avez pas d'article dans votre wishlist."; 
                            } else {
                                echo "Cet utilisateur n'a pas d'article dans sa wishlist.";
                            }
                     echo "</td></tr>";
                /* SINON : ON A DES ARTICLES ET ON AFFICHE LE MONTANT TOTAL */ 
                } else {
                    $result = $connexion->prepare('SELECT sum(prixvente) FROM wishlist INNER JOIN articles ON idrefarticle=idarticle WHERE idrefuser=:id');
                    $result->execute(array(
                        'id' => $_GET['id']
                    ));
                    $result = $result->fetch();
                    echo "<tr>
                            <td colspan=2 style='text-align: right;'> 
                                Prix d'achat total des articles souhaités.
                            </td>
                            <td>".round($result[0], 2)." €</td>
                        </tr>";
                }
                ?>
            </table>
            <br>
        </div>
    </div>

<?php } else {
    header('Location: shop.php');
    exit();
}  } else {
    header('Location: shop.php');
    exit();
}
include('include/footer.php');
?>
