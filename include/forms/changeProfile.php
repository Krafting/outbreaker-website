<?php
include('../init.php');
define('TARGET', '../../upload/profiles/'.$_SESSION['id'].'/');    
define('MAX_SIZE', 8000000);
$tabExt = array('jpg','gif','png','jpeg'); 
$infosImg = array();

/* SI LE POST EST ENVOYÉ ET SI ON EST CONNECTÉ */
if(connect()) {
	if(isset($_POST['changeProfile'])) {

		/* SI LE POST ABOUT EST ENVOYÉ */ 
		if(isset($_POST['about'])) {
			/* SI LE FORMULAIRE EST EN DESSOUS DE LA TAILLE MAX. */
			if(strlen($_POST['about']) < 2150) {
				$updateAbout = $connexion->prepare('UPDATE users SET about=:about WHERE iduser=:id;');
				$updateAbout->execute(array(
					'about' => $_POST['about'],
					'id' => $_SESSION['id']
				));
				header('Location: ../../membre.php?succ=12');
				exit();
			} else {
				header('Location: ../../membre.php?err=25');
				exit();
			}
		}
		
		/* SI L'UTILISATEUR A UPLOAD UNE PHOTO, ON UPDATE */
		if(!empty($_FILES['newPicture']['name'])) {
			/* SI L'IMAGE EST INFÉRIEUR A LA TAILLE MAXIMUM DES IMAGES AUTORISÉES */
			if (filesize($_FILES["newPicture"]["tmp_name"]) < $maxImageSize) {
				/* SI LE DOSSIER D'UTILISATEUR N'EXISTE PAS, ON LE CRÉE */
				if (!file_exists('../../upload/profiles/'.$_SESSION['id'].'/')) {
					mkdir('../../upload/profiles/'.$_SESSION['id'].'/', 0755, true);
				}
				$extension  = pathinfo($_FILES['newPicture']['name'], PATHINFO_EXTENSION);
				/* SI L'EXTENTION EST AUTORISÉE */
				if(in_array(strtolower($extension),$tabExt)) {
					$nomImage = sha1(uniqid()) .'.'. $extension;
					/* SI L'UPLOAD A ÉTÉ FAIS AVEC SUCCÈS */
					if(move_uploaded_file($_FILES['newPicture']['tmp_name'], TARGET.$nomImage)) {
						$updateAbout = $connexion->prepare('UPDATE users SET profilepicture=:profilepicture WHERE iduser=:id;');
						$updateAbout->execute(array(
							'profilepicture' => $nomImage,
							'id' => $_SESSION['id']
						));
						header('Location: ../../membre.php?succ=13');
						exit();
					} else {
						header('Location: ../../membre.php?err=9');
						exit();
					}
				} else {
					header('Location: ../../membre.php?err=7');
					exit();
				}
			} else {
				header('Location: ../../membre.php?err=8');
				exit();
			}
		}
	} else {
		header('Location: ../../membre.php?err=1');
		exit();
	}
} else {
	header('Location: ../../');
    exit();
}



?>