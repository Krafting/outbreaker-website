<?php
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
} 

/* Apres avoir appuyer sur le bouton , on verifie que l'entrée ne sois pas vide sinon redirection page d'erreur*/
if( isset($_POST['changeNomCategorie'])) {
	if(!empty($_POST['nom_categorie'])) {
		$sql_exist = $connexion->prepare(" SELECT count(*) from typearticle where idtype=:idtype ;");
		$sql_exist->execute(array(
			'idtype' => $_POST['idtype']
		));
		$sql_exist=$sql_exist->fetch();

		/*On dois modifier un type qui existe*/
		if ($sql_exist[0]>0) {
			$sql_setCategorie = $connexion->prepare("UPDATE typearticle set nomtype=:nomtype where idtype=:idtype ;");
			$sql_setCategorie->execute(array(
				'nomtype' => $_POST['nom_categorie'],
				'idtype' => $_POST['idtype']
			));
			header('Location: ../../../admin/gestion_categorie.php?succ=34&idtype='.secure($_POST['idtype']).''); 
			exit();
		} else {
			header('Location: ../../../admin/gestion_categorie.php?succ=37'); 
			exit();
		} 
	} else {
		header('Location: ../../../admin/gestion_categorie.php?succ=39&idtype='.secure($_POST['idtype']).''); 
		exit();
	} 
}

/* quand on veut delete la categorie , il faut verifier qu'aucun article n'appartiens à cette categorie */
if (isset($_POST['delete_categorie'])) {
	$sql_empty = $connexion->prepare(" SELECT count(idarticle) from articles where reftypearticle=:idtype ;");
	$sql_empty->execute(array(
		'idtype' => $_POST['idtype_del']
	));
	$sql_empty=$sql_empty->fetch();	

	if ($sql_empty[0]==0) {
		$sql_deleteCategorie = $connexion->prepare("DELETE from typearticle where idtype=:idtype ;");
		$sql_deleteCategorie->execute(array(
			'idtype' => $_POST['idtype_del']
		));
		header('Location: ../../../admin/gestion_categorie.php?succ=36'); 
		exit();
	} else {
		header('Location: ../../../admin/gestion_categorie.php?err=35&idtype='.secure($_POST['idtype_del']).''); 
		exit();
	}
}



/* On verifie qu'on n'as bien donner un nom à la catégorie avant de la creer */
if (isset($_POST['add_categorie'])) {
	if(!empty($_POST['new_categorie'])) {
		$sql_addCategorie = $connexion->prepare("INSERT into typearticle (nomtype) VALUES (:nouvelle_categorie);");
		$sql_addCategorie->execute(array(
			'nouvelle_categorie' => $_POST['new_categorie']
		));
		header('Location: ../../../admin/gestion_categorie.php?succ=38&idtype='.secure($_POST['idtype']).'');
		exit();
	} else {
		header('Location: ../../../admin/gestion_categorie.php?err=39');
		exit();
	}
}
?>