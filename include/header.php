<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;900&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Outbreaker e-Sport | <?php echo $pageTitle; ?></title>
    <link href="https://fonts.googleapis.com/css2?family=Alfa+Slab+One&display=swap" rel="stylesheet"> 
    <link href="include/css/css.css" rel="stylesheet">
    <link href="include/css/components.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.krafting.net/css/fontawesome/css/all.min.css">
</head>
<body>
    <header <?php if($pageTitle == "Accueil") { echo 'class="main"'; } ?>>
        <nav>
            <div class="leftNav">
                <a href="index.php" class="headLink">Accueil</a>
                <a href="shop.php" class="headLink">Boutique</a>
                <a href="plays.php" class="headLink">Plays</a>
            </div>
            <div class="rightNav">
                <?php if(!connect()) { ?>
                <a href="register.php" class="headLink">S'inscrire</a>
                <a href="login.php" class="headLink">Se connecter</a>
                <?php } else { ?>
                    <a href="membre.php" class="headLink">Espace Membre</a>
                <?php } ?>
            </div>
            <div class="headText">
                <div class="teamLogo <?php if($pageTitle == "Accueil") { echo 'main'; } ?>">
                <?php  if($pageTitle == "Accueil") {
                        echo 'OutBreaker'; 
                    } else {
                        echo $pageTitle; 
                    } 
                ?>
                </div>
            </div>
        </nav>
    </header>
<?php if(isset($isBoutique) AND $isBoutique == true AND connect()) { 

    /* ON COMPTE LE NOMBRE D'ARTICLE DANS LE PANIER DE L'UTILISATEUR ACTUELLEMENT CONNECTÉ */
    $nbPanier = $connexion->prepare('SELECT * FROM panier2 WHERE idrefuser=:iduser');
    $nbPanier->execute(array(
        'iduser' => $_SESSION['id']
    ));
    $nbPanier = $nbPanier->fetchAll();
    $nbPanierTot = 0;
    $nbPanierTotal = "0 article";

    foreach ( $nbPanier as $nb) {
        $nbPanierTot = $nbPanierTot + $nb['qte'];
        $nbPanierTotal = $nbPanierTot." articles";
    }

    ?>
    <div class="content">
        <div class="page" style='height: 60px;'>
            <p style="float: left;font-size: 20px;">Bonjour, <b><?php echo $_SESSION['pseudo']; ?></b>. Vous avez actuellement <?php echo $nbPanierTotal; ?>  dans votre panier </p>
            <a style="float: right;top: 0;" class="btn" href="panier.php">Voir le panier</a>
        </div>
    </div><?php } ?>


<?php if(isset($isPlay) AND $isPlay == true AND connect() AND canUploadPlay()) {
    $nbPanier = $connexion->prepare('SELECT COUNT(*) FROM plays WHERE refuser=:iduser');
    $nbPanier->execute(array(
        'iduser' => $_SESSION['id']
    ));
    $nbPanier = $nbPanier->fetch();
    if($nbPanier['count'] > 0) {
        $nbPanierTotal = $nbPanier['count']." articles";
    } else {
        $nbPanierTotal = "0 article";
    }
    ?>
    <div class="content">
        <div class="page" style='height: 60px;'>
            <p style="float: left;font-size: 20px;">Bonjour, <b><?php echo $_SESSION['pseudo']; ?></b> ! </p>
            <a style="float: right;top: 0; left: 20px;" class="btn" href="plays.php?action=add">Proposer un Play</a>
            <a style="float: right;top: 0;" class="btn" href="plays.php?action=gerer">Gerer mes Plays</a>
        </div>
    </div><?php } ?>