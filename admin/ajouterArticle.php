<?php
$adminPage = true; $titrePage = "Ajouter un article";
include('../include/init.php');
include('header.php');

if(isset($_GET['err']) OR isset($_GET['succ'])) {
    if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
        $idMsg = $_GET['err'];
        echo getMessage($idMsg);
    }
    if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
        $idMsg = $_GET['succ'];
        echo getMessage($idMsg);
    }             
}

?>

<h1>Ajouter un article</h1>
<hr style="margin-bottom: 30px;">

<div class="flexBlock">
    <div class="flexContent border-first">
        <form method="post" action="../include/forms/admin/addArticle.php" enctype="multipart/form-data">         
            <p>Entrer le nom de l'article  : </p>  
            <input type="text" name="nom_article" placeholder="Nom de l'article"> 

            <p>Entrer la quantité disponible</p>
            <input type="text" name="qte_article" placeholder="Quantité de l'article">


            <p>Entrer des mot-clefs</p>
            <input type="text" name="mot_clefs" placeholder="Mots-clefs">  


            <p>Entrer le prix de vente</p>
            <input type="text" name="prix_article" placeholder="Prix de vente">    
    </div>
    <div class="flexContent">
            <p>Entrer la description de l'article</p>
            <textarea name="des_article" placeholder="Description"></textarea> 

            <!--reftype menu déroulant--> 
            <p>Type de l'article</p>
            <select name="nom_type">
                <?php
                    $sql_type = $connexion->prepare("SELECT * from typearticle  order by idtype;");
                    $sql_type->execute();
                    $sql_type=$sql_type->fetchall();
                    foreach ($sql_type as $ligne) {
                    print( '<option value="'.$ligne["idtype"].'">'.$ligne["nomtype"].'</option>'); 
                    }
                ?> 
            </select>

            <p>Photo(s) de l'article(s)</p>  
            <input type="file" name="photo_article"> 

            <div class="sendButton">
                <button type="submit" name="ajouterArticle" class="btn">Ajouter l'article</button>        
            </div>
        </form>                                           
    </div>
</div>




