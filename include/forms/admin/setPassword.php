<?php 
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
}


/* quand on appuie sur le bouton pour changer le password on verifie que les input ne soit pas vide puis on recupere les entrées */
if(isset($_POST['setpassword']) AND !empty($_POST['new_pass']) AND !empty($_POST['new_pass_conf'])) {
	$new_pass=$_POST['new_pass'];
	$new_pass_conf=$_POST['new_pass_conf'];
	/*On vérifie que les deux mots de pass rentré sont identiques et on hash le mot de passe avant de le mettre dans la bdd afin de ne pas l'avoir en clair (question de sécurité) */
		
	if ($new_pass==$new_pass_conf){
		$new_pass_hash=hash('sha256',$new_pass);
		$sql_pass = $connexion->prepare("UPDATE users set pass=:pass where iduser=:iduser;");
		$sql_pass->execute(array(
			'pass' => $new_pass_hash,
			'iduser' => secure($_POST['id'])
		));
		header('Location: ../../../admin/users.php?succ=71&id='.secure($_POST['id']));
		exit();
	} else {
		header('Location: ../../../admin/users.php?err=3&id='.secure($_POST['id']));
		exit();
	}
}  else {
	header('Location: ../../../admin/users.php?err=1&id='.secure($_POST['id']));
    exit();
} ?>