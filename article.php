<?php
$isBoutique = true;
$pageTitle = "Article";
include('include/init.php');
if(!isset($_GET['id'])) {
    header('Location: shop.php');
    exit();
}
include('include/header.php');

/* SI L'ARTICLE EXISTE, ET SEULEMENT SI, ON AFFICHE LA PAGE */
$result = $connexion->prepare('SELECT * FROM articles WHERE idarticle=:idarticle');
$result->execute(array(
    'idarticle' => $_GET['id']
));
$result2 = $result->fetch();

if(isset($result2['idarticle'])) {
?>
<style>

</style>
    <div class="content">
        <div class="page articles">
        <?php 
            if(isset($_GET['err']) OR isset($_GET['succ'])) {
                if(!empty($_GET['err']) AND is_numeric($_GET['err'])) {
                    $idMsg = $_GET['err'];
                    echo getMessage($idMsg);
                }
                if(!empty($_GET['succ']) AND is_numeric($_GET['succ'])) {
                    $idMsg = $_GET['succ'];
                    echo getMessage($idMsg);
                }
            } ?><br><br>
            <div class="flexBlock">
                <div class="flexContent article">
                    <?php if(empty($result2['imageprincipale'])) { ?>
                        <img src="upload/defaultArticle.png"></img>
                    <?php } else { ?>
                        <a href="#" onclick="openModal('picArticlePrincipal')"><img src="upload/articles/<?php echo $result2['idarticle'].'/'.$result2['imageprincipale'];?>"></img></a>
                        <div id="picArticlePrincipal" class="modal">
                            <div class="modal-content">
                                <p style="max-height: 600px;overflow: auto;">
                                    <img style="width: auto; height: auto" src="upload/articles/<?php echo $result2['idarticle'].'/'.$result2['imageprincipale'];?>"></img>
                                </p>
                            </div>
                        </div>
                    <?php } ?>
                    <br>
                    <br>
                    <div class="selectOthersPic">
                    <?php 
                        $reqImage = $connexion->prepare('SELECT * FROM imagesarticle WHERE refarticle=:refarticle');
                        $reqImage->execute(array(
                            'refarticle' => $_GET['id']
                        ));
                        $images = $reqImage->fetchAll();
                        foreach($images as $image) {
                    ?>
                        <button class="nocss" onclick="openModal('picArticle<?php echo $image['nomfichier'];?>')"><img src="upload/articles/<?php echo $_GET['id'].'/'.$image['nomfichier'];?>"></img></button>
                        <div id="picArticle<?php echo $image['nomfichier'];?>" class="modal">
                            <div class="modal-content" style="max-width: 40%;">
                                <p style="max-height: 700px;overflow: auto;">
                                    <img style="width: auto; height: auto" src="upload/articles/<?php echo $_GET['id'].'/'.$image['nomfichier']; ?>"></img>
                                </p>
                            </div>
                        </div><?php } ?>

                    </div>  
                </div>  
                <div class="flexContent" style="text-align: justify;">
                    <h1 style="text-transform: uppercase;margin-top: 0;"><?php echo $result2['nomarticle']; ?></h1>
                    <p style="font-weight: bold">Informations</p>
                    <p><?php echo $result2['prixvente']; ?> € 
                    <?php if($result2['qtedispo'] > 0) { ?>
                    <label class="stock yes">En stock</label></p>
                    <div class="sendButton">
                        <?php if(connect()) { ?>
                            <div class="sendButton" style="margin-bottom: 10px;">
                                <a href="panier.php?addArticle=<?php echo $result2['idarticle']; ?>" class="btn" style="margin-bottom: 0">Ajouter au panier</a>
                            </div>
                            <?php 
                            /* ON CHECK SI L'UTILISATEUR A DEJA CET ARTICLE DANS SA WISHLIST */
                            $reqWishList = $connexion->prepare('SELECT count(*) FROM wishlist WHERE idrefuser=:iduser AND idrefarticle=:idarticle');
                            $reqWishList->execute(array(
                                'iduser' => $_SESSION['id'],
                                'idarticle' => $_GET['id']
                            ));
                            $count = $reqWishList->fetch();

                            /* SI OUI, ON AFFICHE LE BOUTTON POUR RETIRER L'ARTICLE */
                            if($count[0] > 0) { ?>
                                <div class="sendButton">
                                    <a href="wishlist.php?id=<?php echo $_SESSION['id']; ?>&remArticle=<?php echo $result2['idarticle']; ?>" class="btn" style="margin-bottom: 0">Retirer de ma wishlist</a>
                                </div>
                            <?php } else { ?>
                                <div class="sendButton">
                                    <a href="wishlist.php?id=<?php echo $_SESSION['id']; ?>&addArticle=<?php echo $result2['idarticle']; ?>" class="btn" style="margin-bottom: 0">Ajouter à ma wishlist</a>
                                </div>
                            <?php } ?>
                            <br><br><br>
                        <?php } else { ?>
                            <a href="login.php?redir=shop" class="btn" style="margin-bottom: 0">Se connecter pour acheter</a>
                        <?php } ?>
                    </div>
                    <?php } else { ?>
                    <label class="stock no">Rupture</label></p>
                        <?php if(connect()) { ?>
                        <a href="wishlist.php?id=<?php echo $_SESSION['id']; ?>&addArticle=<?php echo $result2['idarticle']; ?>" class="btn" style="margin-bottom: 0">Ajouter à ma wishlist</a>
                        <br><br><br>
                        <?php } ?>
                    <?php } ?>
                    <p style="font-weight: bold">Description</p>
                    <p><?php echo formatTexte($result2['desarticle']); ?></p>
                </div>  
            </div>
            <hr style="margin: 50px auto;">
            <div class="avisArticles">
                <h1>Les avis des utilisateurs sur cet article</h1>
                <?php 
                /* SI ON EST CONNECTEE */
                if(connect()) {
                /* ON REGARDE SI L'UTILISATEUR A COMMANDER CET ARTICLE, SI OUI IL PEUT LAISSER UN AVIS */
                $countArticles = $connexion->prepare('SELECT count(refarticle) FROM commander INNER JOIN commandes ON refcommande=idcommande WHERE refarticle=:refarticle AND refuser=:iduser');
                $countArticles->execute(array(
                    'refarticle' => $result2['idarticle'],
                    'iduser' => $_SESSION['id']
                ));
                $countArticles = $countArticles->fetch();


                if($countArticles[0] > 0) {

                    /* ON RECUPERE LE CONTENU DE l'AVIS */
                    $fetchAvis = $connexion->prepare('SELECT * FROM avisarticle WHERE idrefarticle=:idrefarticle AND idrefuser=:idrefuser');
                    $fetchAvis->execute(array(
                        'idrefarticle' => $result2['idarticle'],
                        'idrefuser' => $_SESSION['id']
                    ));
                    /* ON RECUPERE LE NOMBRE D'AVIS SUR CET ARTICLE AVEC L'ID DE L'USER CONNECTÉ */
                    $say = $fetchAvis->fetch();

                    $countAvis = $connexion->prepare('SELECT COUNT(*) FROM avisarticle WHERE idrefarticle=:idrefarticle AND idrefuser=:idrefuser');
                    $countAvis->execute(array(
                        'idrefarticle' => $result2['idarticle'],
                        'idrefuser' => $_SESSION['id']
                    ));
                    $countAvis = $countAvis->fetch();

                    if($countAvis[0] > 0) { $avis = $say['textavis']; $nbStars = $say['noteavis']; } else { $avis = ""; }
                    ?>
                    <div class="LaisserAvis">
                        <p style="margin-bottom: 35px;">Vous pouvez laisser un avis sur cette article si vous le désiré (max 2500 caractères) </p>
                        <form method="POST" action="include/forms/addAvis.php">
                        <div class="flexBlock">
                            <div class="flexContent border-first" style="flex: 3;">
                                <p style="margin-top:0">Description de votre avis</p>
                                <textarea placeholder='Écrivez votre avis ici...' name="textarticle" max-lenght="250" style="margin: 0 auto;"><?php echo $avis; ?></textarea>
                            </div>
                            <div class="flexContent">
                                <p style="margin-top:0">Note du produit</p>
                                <select name="stars">
                                    <option value="5" <?php if(isset($nbStars) AND $nbStars == 5) { echo "selected"; } ?>>5 étoiles</option>
                                    <option value="4" <?php if(isset($nbStars) AND $nbStars == 4) { echo "selected"; } ?>>4 étoiles</option>
                                    <option value="3" <?php if(isset($nbStars) AND $nbStars == 3) { echo "selected"; } ?>>3 étoiles</option>
                                    <option value="2" <?php if(isset($nbStars) AND $nbStars == 2) { echo "selected"; } ?>>2 étoiles</option>
                                    <option value="1" <?php if(isset($nbStars) AND $nbStars == 1) { echo "selected"; } ?>>1 étoile</option>
                                </select>
                                <input name="idarticle" value="<?php echo $result2['idarticle']; ?>" type="hidden">
                                <button name="modifAvis" class="btn" type="submit">Envoyer mon avis!</button>
                            </div>
                        </div>
                        </form>
                    </div>
                <?php } } ?>
                <div class="avisArticle" style="margin-top: 60px;">
                    <?php 
                    $selectAllAvis = $connexion->prepare('SELECT * from avisarticle INNER JOIN users ON idrefuser=iduser WHERE idrefarticle=:idarticle');
                    $selectAllAvis->execute(array(
                        'idarticle' => $_GET['id']
                    ));
                    $selectAllAvis = $selectAllAvis->fetchAll();
                    $count = count($selectAllAvis);
                    if($count > 0) {
                    foreach($selectAllAvis as $avis) {
                    ?>
                    <div style="flex-flow:column; margin-bottom: 30px">
                        <div class="flexBlock oneChild">
                            <div class="flexContent" style="flex: 1; padding: 10px;max-width: 100px;">
                                <div class="profileBlock" style="max-height: 100px;">
                                    <img style="border-radius: 10px" src="upload/profiles/<?php echo findProfilePic($avis['idrefuser']); ?>"></img>
                                </div>
                            </div>
                            <div class="flexContent" style="flex: 6; padding: 10px; text-align: left;">
                                <a style="margin: 0;" href="profiles.php?id=<?php echo $avis['iduser']; ?>"><h2 style="margin:0;"><?php echo $avis['pseudo']; ?></h2></a>
                                <p style="margin:0; padding-bottom: 15px;">Note : <b style="font-weight: bold"><?php echo $avis['noteavis']; ?> / 5</b> étoiles</p>

                                <?php echo formatTexte($avis['textavis']); ?>
                            </div>
                        </div>
                    </div>
                    <?php } } else { echo '<div class="infoMessage" style="margin-bottom: 150px;">Aucun utilisateur n\'a posté d\'avis sur ce produit</div>'; } ?>
                </div>
            </div>
        </div>
    </div>

<?php } else {
    header('Location: shop.php');
    exit();
}
    include('include/footer.php');
?>