<?php
include('../../init.php');

if(!connect() OR !isAdmin()) {
	header('Location: ../../../index.php');
    exit();
} 

/* Apres avoir appuyer sur le bouton , on verifie que l'entrée ne sois pas vide sinon redirection page d'erreur*/
if( isset($_POST['changeNomrang'])) {
	if(!empty($_POST['nom_rang'])) {
		$sql_exist = $connexion->prepare(" SELECT count(*) from rangs where idrang=:idrang ;");
		$sql_exist->execute(array(
			'idrang' => $_POST['idrang']
		));
		$sql_exist=$sql_exist->fetch();

		/*On dois modifier un type qui existe*/
		if ($sql_exist[0]>0) {
			$sql_setrang = $connexion->prepare("UPDATE rangs set nomrang=:nomrang where idrang=:idrang ;");
			$sql_setrang->execute(array(
				'nomrang' => $_POST['nom_rang'],
				'idrang' => $_POST['idrang']
			));
			header('Location: ../../../admin/gestion_rang.php?succ=92&idrang='.secure($_POST['idrang']).''); 
			exit();
		} else {
			header('Location: ../../../admin/gestion_rang.php?err=91'); 
			exit();
		} 
	} else {
		header('Location: ../../../admin/gestion_rang.php?succ=1&idrang='.secure($_POST['idrang']).''); 
		exit();
	} 
}

/* quand on veut delete la rang , il faut verifier qu'aucun users n'appartiens à cette rang */
if (isset($_POST['delete_rang'])) {
	$sql_empty = $connexion->prepare(" SELECT count(iduser) from users where refrang=:idrang ;");
	$sql_empty->execute(array(
		'idrang' => $_POST['idrang_del']
	));
	$sql_empty=$sql_empty->fetch();	

	if ($sql_empty[0]==0) {
		if($_POST['idrang_del'] > 1) {
			$sql_deleterang = $connexion->prepare("DELETE FROM rangs where idrang=:idrang ;");
			$sql_deleterang->execute(array(
				'idrang' => $_POST['idrang_del']
			));
			header('Location: ../../../admin/gestion_rang.php?succ=93'); 
			exit();
		} else {
			header('Location: ../../../admin/gestion_rang.php?err=96'); 
			exit();
		} 	
	} else {
		header('Location: ../../../admin/gestion_rang.php?err=94&idrang='.secure($_POST['idrang_del']).''); 
		exit();
	}
}



/* On verifie qu'on n'as bien donner un nom à la catégorie avant de la creer */
if (isset($_POST['add_rang'])) {
	if(!empty($_POST['new_rang'])) {
		$sql_addrang = $connexion->prepare("INSERT into rangs (nomrang) VALUES (:nomrang);");
		$sql_addrang->execute(array(
			'nomrang' => $_POST['new_rang']
		));
		header('Location: ../../../admin/gestion_rang.php?succ=95&idrang='.secure($_POST['idrang']).'');
		exit();
	} else {
		header('Location: ../../../admin/gestion_rang.php?err=1');
		exit();
	}
}
?>